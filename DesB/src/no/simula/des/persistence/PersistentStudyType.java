/*
 * Created on 04.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.des.StudyType;
import no.simula.des.statements.StudyTypeStatement;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentStudyType implements PersistentObject {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PersistentStudyType.class);

  /**
   * There's not possible to create new study types (e.g. using an administrator's interface), except directly in the database
   * This method is not implemented
   * 
   * @param instance 
   * @return null
   * 
   * @throws CreatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#create(Object)
   */
  public Integer create(Object instance) throws CreatePersistentObjectException {
		Integer studyTypeId = new Integer(0);

		StudyType studyType = (StudyType) instance;

		StudyTypeStatement statement = new StudyTypeStatement();

		/* Tells the statement what study object to save */
		statement.setDataBean(studyType);

		/* Save the study */
		try {
			studyTypeId = (Integer) statement.executeInsert();
		} catch (StatementException se) {
			log.error("Couldn't create study type");

			se.printStackTrace();

			throw new CreatePersistentObjectException();
		}

		return (studyTypeId);
  }

  /**
   * There's not possible to delete study types (e.g. using an administrator's interface), except directly in the database
   * This method is not implemented
   * 
   * @param id 
   * @return boolean
   * @throws DeletePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#delete(Integer)
   */
  public boolean delete(Integer id) throws DeletePersistentObjectException {
		boolean result = false;

		StudyTypeStatement statement = new StudyTypeStatement();

		/* Delete the admin module text */
		try {
			result = statement.executeDelete(id);
		} catch (StatementException se) {
			log.error("Couldn't delete study type");

			se.printStackTrace();

			throw new DeletePersistentObjectException();
		}

		return (result);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#findByKey(java.lang.Integer)
   */
  /**
   * Method findByKey
   * @param id Integer
   * @return Object
   * @see no.halogen.persistence.PersistentObject#findByKey(Integer)
   */
  public Object findByKey(Integer id) {
    Object result = new Object();
    StudyTypeStatement statement = new StudyTypeStatement();

    StudyType studyType = new StudyType();

    statement.setDataBean(studyType);

    try {
      result = statement.executeSelect(id);
    } catch (StatementException e) {
      log.error("findBykey() - Fetching of study type " + id + " failed!!", e);
      return (null);
    }

    if (result instanceof StudyType) {
      studyType = (StudyType) result;
    }

    return (studyType);
  }

  /* (non-Javadoc)
   * @see no.halogen.persistence.PersistentObject#findByKeys(java.util.List)
   */
  /**
   * Method findByKeys
   * @param entityIds List
   * @return List
   * @see no.halogen.persistence.PersistentObject#findByKeys(List)
   */
  public List findByKeys(List entityIds) {
    List results = new ArrayList();

    if (!entityIds.isEmpty()) {
      StudyTypeStatement statement = new StudyTypeStatement();

      StringBuffer stmtWhere = new StringBuffer();

      StudyType studyType = new StudyType();

      statement.setDataBean(studyType);

      if (entityIds != null && !entityIds.isEmpty()) {
        stmtWhere.append(" WHERE sty_id IN (");

        Iterator i = entityIds.iterator();
        boolean firstIteration = true;

        while (i.hasNext()) {
          if (!firstIteration) {
            stmtWhere.append(", ");
          }

          stmtWhere.append("?");

          statement.getConditions().add(i.next());

          firstIteration = false;
        }

        stmtWhere.append(")");
      }

      statement.setWhereClause(stmtWhere.toString());

      try {
        results = (ArrayList) statement.executeSelect();
      } catch (StatementException e) {
        log.error("findByKeys() - Fetching of study types failed!!", e);
        return (null);
      }
    }

    return (results);
  }

  /**
   * There's not possible to update study types (e.g. using an administrator's interface), except directly in the database
   * This method is not implemented
   * @param instance Object
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Object)
   */
  public boolean update(Object instance) throws UpdatePersistentObjectException {
		StudyType studyType = (StudyType) instance;

		return (update(studyType.getId(), instance));
  }

  /**
   * There's not possible to update study types (e.g. using an administrator's interface), except directly in the database
   * This method is not implemented
   * @param id Integer
   * @param instance Object
   * @return boolean
   * @throws UpdatePersistentObjectException
   * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
   */
  public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
		boolean result = false;

		StudyType studyType = (StudyType) instance;

		StudyTypeStatement statement = new StudyTypeStatement();

		/* Tells the statement what study object to update */
		statement.setDataBean(studyType);

		/* Update the study */
		try {
			result = statement.executeUpdate(id);
		} catch (StatementException se) {
			log.error("Couldn't update study type");

			se.printStackTrace();

			throw new UpdatePersistentObjectException();
		}

		return (result);
  }

  /**
   * Finding all Study types
   * @return - All study types in the database
   * 
   * @see no.halogen.persistence.PersistentObject#find()
   */
  public List find() {
    List results = new ArrayList();

    StudyTypeStatement statement = new StudyTypeStatement();

    StudyType studyType = new StudyType();

    statement.setDataBean(studyType);

    try {
      results = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("find() - Fetching of all study types failed!!", e);
      return (null);
    }

    return (results);
  }

}
