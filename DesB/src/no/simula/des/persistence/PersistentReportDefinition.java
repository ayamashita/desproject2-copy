package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.PersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.statements.StatementException;
import no.simula.Person;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.ReportDefinition;
import no.simula.des.StudyColumn;
import no.simula.des.statements.ReportDefinitionColumnsStatement;
import no.simula.des.statements.ReportDefinitionCriteriaStatement;
import no.simula.des.statements.ReportDefinitionStatement;
import no.simula.persistence.PersistentPerson;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PersistentReportDefinition implements PersistentObject {

  private static Log log = LogFactory.getLog(PersistentReportDefinition.class);
  
  public Integer create(Object instance) throws CreatePersistentObjectException {
    Integer reportDefinitionId = new Integer(0);

    ReportDefinition reportDefinition = (ReportDefinition) instance;

    ReportDefinitionStatement statement = new ReportDefinitionStatement();

    /* Tells the statement what study object to save */
    statement.setDataBean(reportDefinition);

    /* Save the study */
    try {
      reportDefinitionId = (Integer) statement.executeInsert();
      
      setColumns(reportDefinitionId, reportDefinition);
      setCriteria(reportDefinitionId, reportDefinition);
      
    } catch (StatementException se) {
      log.error("Couldn't create report definition");

      se.printStackTrace();

      throw new CreatePersistentObjectException();
    }

    return (reportDefinitionId);
  }

  public boolean delete(Integer id) throws DeletePersistentObjectException {
    boolean result = false;

    ReportDefinitionColumnsStatement reportColumnsStatement = new ReportDefinitionColumnsStatement(); 
    try {
      result = reportColumnsStatement.executeDelete(id);
    } catch (StatementException e) {
      log.error("update() - Could not delete report definition columns.");
    }

    ReportDefinitionCriteriaStatement reportCriteriaStatement = new ReportDefinitionCriteriaStatement(); 
    try {
      result = reportCriteriaStatement.executeDelete(id);
    } catch (StatementException e) {
      log.error("update() - Could not delete report definition criteria.");
    }

    ReportDefinitionStatement statement = new ReportDefinitionStatement();

    /* Delete the admin module text */
    try {
      result = statement.executeDelete(id);
    } catch (StatementException se) {
      log.error("Couldn't delete report definition");

      se.printStackTrace();

      throw new DeletePersistentObjectException();
    }

    return (result);
  }

  public Object findByKey(Integer id) {
    Object result = new Object();
    ReportDefinitionStatement statement = new ReportDefinitionStatement();

    ReportDefinition reportDefinition = new ReportDefinition();

    statement.setDataBean(reportDefinition);

    try {
      result = statement.executeSelect(id);
    } catch (StatementException e) {
      log.error("findBykey() - Fetching of report definition " + id + " failed!!", e);
      return (null);
    }

    if (result instanceof ReportDefinition) {
      reportDefinition = (ReportDefinition) result;
    }

    populateReportDefinition(reportDefinition);

    return (reportDefinition);
  }

  public List findByKeys(List entityIds) {
    List results = new ArrayList();

    if (!entityIds.isEmpty()) {
      ReportDefinitionStatement statement = new ReportDefinitionStatement();

      StringBuffer stmtWhere = new StringBuffer();

      ReportDefinition reportDefinition = new ReportDefinition();

      statement.setDataBean(reportDefinition);

      if (entityIds != null && !entityIds.isEmpty()) {
        stmtWhere.append(" WHERE id IN (");

        Iterator i = entityIds.iterator();
        boolean firstIteration = true;

        while (i.hasNext()) {
          if (!firstIteration) {
            stmtWhere.append(", ");
          }

          stmtWhere.append("?");

          statement.getConditions().add(i.next());

          firstIteration = false;
        }

        stmtWhere.append(")");
      }

      statement.setWhereClause(stmtWhere.toString());

      try {
        results = (ArrayList) statement.executeSelect();
      } catch (StatementException e) {
        log.error("findByKeys() - Fetching of report definitions failed!!", e);
        return (null);
      }
    }

    populateReportDefinitions(results);
    
    return (results);
  }

  public boolean update(Object instance) throws UpdatePersistentObjectException {
    ReportDefinition reportDefinition = (ReportDefinition) instance;

    return (update(reportDefinition.getId(), instance));
  }

  public boolean update(Integer id, Object instance) throws UpdatePersistentObjectException {
    boolean result = false;

    ReportDefinition reportDefinition = (ReportDefinition) instance;

    ReportDefinitionColumnsStatement reportColumnsStatement = new ReportDefinitionColumnsStatement(); 
    try {
      result = reportColumnsStatement.executeDelete(id);

      setColumns(id, reportDefinition);
    } catch (StatementException e) {
      log.error("update() - Could not update report definition columns.");
    }

    ReportDefinitionCriteriaStatement reportCriteriaStatement = new ReportDefinitionCriteriaStatement(); 
    try {
      result = reportCriteriaStatement.executeDelete(id);

      setCriteria(id, reportDefinition);
    } catch (StatementException e) {
      log.error("update() - Could not update report definition criteria.");
    }

    
    ReportDefinitionStatement statement = new ReportDefinitionStatement();

    /* Tells the statement what study object to update */
    statement.setDataBean(reportDefinition);

    /* Update the study */
    try {
      result = statement.executeUpdate(id);
    } catch (StatementException se) {
      log.error("Couldn't update report definition");

      se.printStackTrace();

      throw new UpdatePersistentObjectException();
    }

    return (result);
  }

  public List find() {
    List results = new ArrayList();

    ReportDefinitionStatement statement = new ReportDefinitionStatement();

    ReportDefinition reportDefinition = new ReportDefinition();

    statement.setDataBean(reportDefinition);

    try {
      results = (ArrayList) statement.executeSelect();
    } catch (StatementException e) {
      log.error("find() - Fetching of all report definitions failed!!", e);
      return (null);
    }

    populateReportDefinitions(results);
    
    return (results);
  }
  
  private void populateReportDefinitions(List reportDefinitions) {

    for (Iterator iterator = reportDefinitions.iterator(); iterator.hasNext();) {
      ReportDefinition reportDefinition = (ReportDefinition) iterator.next();
      populateReportDefinition(reportDefinition);
    }

  }

  private void populateReportDefinition(ReportDefinition reportDefinition) {

    Person owner = getPerson(reportDefinition.getOwner().getId());
    if (owner != null && owner instanceof Person) {
      reportDefinition.setOwner(owner);
    }
    reportDefinition.setColumns(getColumns(reportDefinition.getId()));
    reportDefinition.setResponsibles(getCriteria(reportDefinition.getId()));
    
  }

  private void setColumns(Integer reportDefinitionId, ReportDefinition reportDefinition) throws StatementException {
    ReportDefinitionColumnsStatement statement = new ReportDefinitionColumnsStatement(); 
    ArrayList columns = new ArrayList();
    List valuePair = new ArrayList();

    columns = (ArrayList) reportDefinition.getColumns();

    for (int i = 0; i < columns.size(); i++) {
      StudyColumn column = (StudyColumn) columns.get(i);
      
      valuePair.clear();
      valuePair.add(reportDefinitionId);
      valuePair.add(column.getColumn());
      valuePair.add(new Integer(i));

      statement.setDataBean(valuePair);

      statement.executeInsert();
    }
  }

  private void setCriteria(Integer reportDefinitionId, ReportDefinition reportDefinition) throws StatementException {
    ReportDefinitionCriteriaStatement statement = new ReportDefinitionCriteriaStatement(); 
    List criteria = new ArrayList();
    List valuePair = new ArrayList();

    criteria = reportDefinition.getResponsibles();

    for (Iterator iterator = criteria.iterator(); iterator.hasNext();) {
      Person criterium = (Person) iterator.next();
      
      valuePair.clear();
      valuePair.add(reportDefinitionId);
      valuePair.add(criterium.getId());

      statement.setDataBean(valuePair);

      statement.executeInsert();
    }
  }

  private List getColumns(Integer id) {
    List columns = new ArrayList();
    List columnNames = null;

    ReportDefinitionColumnsStatement statement = new ReportDefinitionColumnsStatement(); 
    statement.setWhereClause(" WHERE " + statement.getKey() + " = ? ORDER BY column_order");
    statement.getConditions().add(id);

    try {
      columnNames = (List) statement.executeSelect();
    } catch (StatementException e) {
      log.error("getColumns() - Fetching columns of report definition id=" + id + " failed!!", e);
      return (null);
    }

    for (Iterator iterator = columnNames.iterator(); iterator.hasNext();) {
      String columnName = (String) iterator.next();
      StudyColumn column = new StudyColumn(columnName);
      columns.add(column);
    }
    
    return columns;
  }

  private List getCriteria(Integer id) {
    List responsibles = new ArrayList();
    List responsibleIds;

    ReportDefinitionCriteriaStatement statement = new ReportDefinitionCriteriaStatement(); 
    statement.setWhereClause(" WHERE " + statement.getKey() + " = ?");
    statement.getConditions().add(id);

    try {
      responsibleIds = (List) statement.executeSelect();
    } catch (StatementException e) {
      log.error("getCriteria() - Fetching criteria of report definition id=" + id + " failed!!", e);
      return (null);
    }

    for (Iterator iterator = responsibleIds.iterator(); iterator.hasNext();) {
      String responsibleId = (String) iterator.next();
      Person responsible = getPerson(responsibleId);
      responsibles.add(responsible);
    }

    return responsibles;
  }

  /**
   * Method getPerson
   * @param id Integer
   * @return Person
   */
  private Person getPerson(String id) {
    PersistentPerson persistentPerson = new PersistentPerson();

    return (Person) (persistentPerson.findByKey(id));
  }


}
