/*
 * Created on 05.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.persistence;

import java.util.ArrayList;
import java.util.List;

import no.simula.des.StudyType;
import junit.framework.TestCase;

/**
 * @author frodel
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PersistentStudyTypeTest extends TestCase {
	/**
	 * Field persistentStudyType
	 */
	private PersistentStudyType persistentStudyType = null;
	/**
	 * Field controlledExperiment
	 */
	private StudyType controlledExperiment = null;
	/**
	 * Field caseStudy
	 */
	private StudyType caseStudy = null;
	/**
	 * Field survey
	 */
	private StudyType survey = null;
	/**
	 * Field oneElement
	 */
	private List oneElement = null;
	/**
	 * Field allElements
	 */
	private List allElements = null;
	/**
	 * Field emptyElements
	 */
	private List emptyElements = null;
	
  /*
   * @see TestCase#setUp()
   */
  /**
   * Method setUp
   * @throws Exception
   */
  protected void setUp() throws Exception {
    super.setUp();
    
    persistentStudyType = new PersistentStudyType();
    
    controlledExperiment = new StudyType(new Integer(1), "Controlled Experiment");
		caseStudy = new StudyType(new Integer(2), "Case Study");
		survey = new StudyType(new Integer(3), "Survey");
		
		oneElement = new ArrayList(1);
		oneElement.add(caseStudy.getId());
		
		allElements = new ArrayList(3);
		allElements.add(controlledExperiment.getId());
		allElements.add(caseStudy.getId());
		allElements.add(survey.getId());
		
		emptyElements = new ArrayList();
  }

  /**
   * Method testFindByKey
   */
  public void testFindByKey() {
  	StudyType result = (StudyType) persistentStudyType.findByKey(controlledExperiment.getId());
  	assertEquals(controlledExperiment.getId(), result.getId());
  	assertEquals(controlledExperiment.getName(), result.getName());
  	
  }

  /**
   * Method testFindByKeys
   */
  public void testFindByKeys() {
  	List studyTypes = new ArrayList(2);
  	studyTypes.add(controlledExperiment);
  	studyTypes.add(survey);
  	
  	List ids = new ArrayList();
  	ids.add(controlledExperiment.getId());
  	ids.add(survey.getId());
  	
  	assertEquals(studyTypes, persistentStudyType.findByKeys(ids));
  }

  /**
   * Method testFind
   */
  public void testFind() {
  	assertEquals(survey, persistentStudyType.find().get(3));
  	
  }

}
