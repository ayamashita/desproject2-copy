package no.simula.des;

import java.io.Serializable;
import no.halogen.persistence.Persistable;

/** This is a duration unit used to specify the duration of a study. A duration
 * consists of an integer and a duration unit.
 */
public class DurationUnit implements Persistable, Serializable {
  
  /** Creates an empty duration unit. */  
  public DurationUnit() {}

  /** Creates a duration unit with the specified id. */
  public DurationUnit(Integer id) {
    this.id = id;
  }
  
  /** Creates a duration unit with the specified id and name. */
  public DurationUnit(Integer id, String name) {
    this.id = id;
    this.name = name;
  }
  
  private Integer id;
  private String name;
  
  /** Getter for property id.
   * @return Value of property id.
   *
   */
  public Integer getId() {
    return(id);
  }
  
  /** Setter for property id.
   * @param id New value of property id.
   *
   */
  public void setId(Integer id) {
    this.id = id;
  }
  
  /** Returns the name of the duration unit.
   * @return the name of the duration unit
   */
  public String getName() {
    return(name);
  }
  
  /** Sets the name of the duration unit
   * @param name the name of the duration unit
   */
  public void setName(String name) {
    this.name = name;
  }

  /** Returns the name of the duration unit.
   * @return the name of the duration unit
   */  
  public String toString() {
    return(getName());
  }
  
  /** Returns whether this and another duration unit is equal. They are equal only if
   * their ids are equal.
   * @param o the other duration unit
   * @return <CODE>true</CODE> if <CODE>this.id.equals(o.id)</CODE>
   */  
  public boolean equals(Object o) {
    return(((DurationUnit)o).id.equals(this.id));
  }
}