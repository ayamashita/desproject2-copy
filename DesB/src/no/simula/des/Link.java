package no.simula.des;

/** This is a study material of the type Link. It represents a URL to an external
 * resource that may be attached to one or several studies.
 */
public class Link extends StudyMaterial {

  /** Creates an empty link */  
  public Link() {
    super();
  }
  
  /** Creates a link with the specified id, description and url.
   * @param id the id
   * @param description the description
   * @param url the url
   */  
  public Link(Integer id, String description, String url) {
    super(id, description);
    this.url = url;
  }
  
  public static final String TYPE = "link";

  private String url;
  
  public String getUrl() {
    return(url);
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  public String getType() {
    return(TYPE);
  }
}