package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import no.halogen.statements.ObjectStatementImpl;

public class ReportDefinitionColumnsStatement extends ObjectStatementImpl {

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "report_definition_id, column_name, column_order";

  /**
   * Field INSERT_VALUES
   */
  private final String INSERT_VALUES = "(?, ?, ?)";

  /**
   * Field KEY
   */
  private final String KEY = "report_definition_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "column_name, column_order";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "report_columns";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "";
    
  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return INSERT_COLUMNS;
  }

  /**
   * Returns name of the id column
   * 
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return KEY;
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return SELECT_COLUMNS;
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return TABLE_NAME;
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return UPDATE_VALUES;
  }

  /** 
   * Populates the data bean with the result from a query
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    return rs.getString("column_name");
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

  /**
   * Method generateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      List conditions = (List) getDataBean();

      pstmt.setInt(1, ((Integer) conditions.get(0)).intValue());
      pstmt.setString(2, (String) conditions.get(1));
      pstmt.setInt(3, ((Integer) conditions.get(2)).intValue());
    }
  }

}

