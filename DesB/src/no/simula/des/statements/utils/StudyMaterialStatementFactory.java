/*
 * Created on 03.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.statements.utils;

import no.halogen.statements.ObjectStatementImpl;
import no.simula.des.File;
import no.simula.des.Link;
import no.simula.des.statements.StudyMaterialFileStatement;
import no.simula.des.statements.StudyMaterialLinkStatement;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class StudyMaterialStatementFactory {
  /**
   * Method create
   * Creates a study material statement (file or link statement)
   * @param instance Object
   * @return ObjectStatementImpl
   */
  static public ObjectStatementImpl create(Object instance) {
    ObjectStatementImpl statement = null;
    
    if (instance instanceof File) {
      statement = new StudyMaterialFileStatement();
    } else if (instance instanceof Link) {
      statement =new StudyMaterialLinkStatement();
    }
    
    return(statement);
  }
}
