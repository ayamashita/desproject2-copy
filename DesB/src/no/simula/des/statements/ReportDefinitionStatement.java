package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import no.halogen.statements.ObjectStatementImpl;
import no.simula.Person;
import no.simula.des.ReportDefinition;

public class ReportDefinitionStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(ReportDefinitionStatement.class);

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "no.simula.des.ReportDefinition";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "owner_id, title, sort, operator";

  /**
   * Field INSERT_VALUES
   */
  private final String INSERT_VALUES = "(?, ?, ?, ?)";

  /**
   * Field KEY
   */
  private final String KEY = "id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "id, owner_id, title, sort, operator";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "report_definitions";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "owner_id = ?, title = ?, sort = ?, operator = ?";

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return INSERT_COLUMNS;
  }

  /**
   * Returns name of the id column
   * 
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return KEY;
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return SELECT_COLUMNS;
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return TABLE_NAME;
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return UPDATE_VALUES;
  }

  /** 
   * Populates the data bean with the result from a query
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    ReportDefinition reportDefinition = null;
    try {
      if (getDataBean() != null) {
        reportDefinition = (ReportDefinition) getDataBean().getClass().newInstance();
      } else {
        reportDefinition = new ReportDefinition();
      }
    } catch (InstantiationException e) {
      log.error("fetchListResults() - Could not create new report definition data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchListResults() - Could not create new report definition data object");
      e.printStackTrace();
    }

    reportDefinition.setId(new Integer(rs.getInt("id")));
    reportDefinition.setTitle(rs.getString("title"));
    reportDefinition.setSort(rs.getBoolean("sort"));
    reportDefinition.setOperator(rs.getString("operator"));

    /*
     * Checks to see if the query returns an id for a person that owns the study
     * If there's an id, a Person is instansiated, and added to the study.
     * To get the complete person information, execute a query in the People table in the Simulaweb database
     */
    Person owner = null;
    String ownerId = rs.getString("owner_id");

    if (ownerId != null) {
      owner = new Person(ownerId);

      reportDefinition.setOwner(owner);
    }

    return (reportDefinition);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

  /**
   * Method generateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      ReportDefinition reportDefinition = (ReportDefinition) getDataBean();

      pstmt.setString(1, reportDefinition.getOwner().getId());
      pstmt.setString(2, reportDefinition.getTitle());
      pstmt.setBoolean(3, reportDefinition.isSort());
      pstmt.setString(4, reportDefinition.getOperator());
    }
  }

}

