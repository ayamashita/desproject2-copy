package no.simula.des.presentation.web.forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import no.halogen.presentation.web.CheckNewActionForm;
import no.simula.Person;
import no.simula.utils.PersonFamilyNameComparator;

import org.apache.struts.action.ActionMapping;

/** Holds temporary data in the presentation layer when editing or creating a study.
 *
 * Dates are stored internally as day, month and year. Methods exist, however to
 * get and set these fields as <CODE>Date</CODE>s. This allows us to autimatically
 * copy dates between the presentation layer bean (<CODE>StudyForm</CODE>) and the
 * application layer bean (<CODE>Study</CODE>).
 */
public class ReportDefinitionForm extends CheckNewActionForm {

  private String title;
  private String operator;
  private boolean sort;
  private Person owner;

  private List availableColumns;
  private List columns;
  private String[] addColumn;
  private String[] removeColumn;
  
  private List persons;
  private List responsibles;
  private String[] addResponsible;
  private String[] removeResponsible;
  
  /** <CODE>addResponsible</CODE> and <CODE>removeResponsible</CODE> is reset because
   * Struts is not able to do so automatically.
   * @param mapping current action mapping
   * @param request current http request
   */  
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    addResponsible = new String[0];
    removeResponsible = new String[0];
    addColumn = new String[0];
    removeColumn = new String[0];
  }
  
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public boolean isSort() {
    return sort;
  }

  public void setSort(boolean sort) {
    this.sort = sort;
  }

  public List getAvailableColumns() {
    return availableColumns;
  }

  public void setAvailableColumns(List availableColumns) {
    this.availableColumns = availableColumns;
  }

  public List getColumns() {
    return columns;
  }

  public void setColumns(List columns) {
    if(columns == null) {
      this.columns = null;
    }
    else {
      this.columns = new ArrayList(columns);
    }
  }

  public String[] getAddColumn() {
    return addColumn;
  }

  public void setAddColumn(String[] addColumn) {
    this.addColumn = addColumn;
  }

  public String[] getRemoveColumn() {
    return removeColumn;
  }

  public void setRemoveColumn(String[] removeColumn) {
    this.removeColumn = removeColumn;
  }

  /** Returns a list responsible persons
   * @return responsible persons
   */  
  public List getResponsibles() {
    return(responsibles);
  }
  
  /** Getter for property addResponsible.
   * @return Value of property addResponsible.
   *
   */
  public String[] getAddResponsible() {
    return(addResponsible);
  }
  
  /** Setter for property addResponsible.
   * @param addResponsible New value of property addResponsible.
   *
   */
  public void setAddResponsible(String[] addResponsible) {
    this.addResponsible = addResponsible;
  }

  /** Getter for property removeResponsible.
   * @return Value of property removeResponsible.
   *
   */
  public String[] getRemoveResponsible() {
    return(removeResponsible);
  }
    
  /** Setter for property removeResponsible.
   * @param removeResponsible New value of property removeResponsible.
   *
   */
  public void setRemoveResponsible(String[] removeResponsible) {
    this.removeResponsible = removeResponsible;
  }
    
  /** Setter for property responsibles. This list is always sorted.
   * @param responsibles New value of property responsibles.
   *
   */
  public void setResponsibles(List responsibles) {
    if(responsibles == null) {
      this.responsibles = null;
    }
    else {
      this.responsibles = new ArrayList(responsibles);
      sortResponsibles();
    }
  }
  
  /** Getter for property persons.
   * @return Value of property persons.
   *
   */
  public List getPersons() {
    return(persons);
  }
  
  /** Setter for property persons. This list is always sorted.
   * @param persons New value of property persons.
   *
   */
  public void setPersons(List persons) {
    if(persons == null) {
      this.persons = null;
    }
    else {
      this.persons = new ArrayList(persons);
      sortPersons();
    }
  }
  
  /** Sorts the list of non-responsible persons by family name. */  
  public void sortPersons() {
    Collections.sort(this.persons, new PersonFamilyNameComparator());
  }
  
  /** Sort the list of responsible persons by family name. */  
  public void sortResponsibles() {
    Collections.sort(this.responsibles, new PersonFamilyNameComparator());
  }

  public void sortAvailableColumns() {
    Collections.sort(this.availableColumns);
  }
  
  public Person getOwner() {
    return owner;
  }

  public void setOwner(Person owner) {
    this.owner = owner;
  }  
}