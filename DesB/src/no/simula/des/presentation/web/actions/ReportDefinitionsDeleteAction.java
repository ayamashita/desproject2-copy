package no.simula.des.presentation.web.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.ReportDefinition;
import no.simula.des.Study;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReportDefinitionsDeleteAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {
    
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
    
    Simula simula = SimulaFactory.getSimula();
    
    // Get deletable studies from the session.
    List deletableDefinitions = (List)request.getSession().getAttribute(Constants.DELETE_REPORT_DEFINITIONS);
    
    // If the list is null at this point, we have an illegal request and return an error.
    if(deletableDefinitions == null) {
      return(mapping.findForward("error"));
    }

    // Lets delete a report definition as specified in the request and display either a new
    // confirmation if there are more definitions to be deleted or the new list of
    // report definitions.
    String deleteIdString = request.getParameter("id");
    if(!skipDefinition(request) && deleteIdString != null) {
      Integer deleteId = Integer.valueOf(deleteIdString);
      simula.deleteReportDefinition(deleteId);
    }

    if(deletableDefinitions.isEmpty()) {
      return(mapping.findForward("return"));
    }
    else {
      ReportDefinition reportDefinition = (ReportDefinition)deletableDefinitions.remove(0);
      request.getSession().setAttribute(Constants.DELETE_REPORT_DEFINITIONS, deletableDefinitions);
      request.setAttribute(Constants.DELETE_THIS_REPORT_DEFINITION, reportDefinition);
      return(mapping.findForward("confirm"));
    }
  }
  
  private boolean skipDefinition(HttpServletRequest request) {
    return(request.getParameter("skip") != null);
  }
}