package no.simula.des.presentation.web.actions;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Person;
import no.simula.Simula;
import no.simula.SimulaException;
import no.simula.SimulaFactory;
import no.simula.des.ReportDefinition;
import no.simula.des.StudyColumn;
import no.simula.des.presentation.web.forms.ReportDefinitionForm;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/** Handles creation of new studies, editing existing studies and
 * saving changes in both cases.
 *
 * The study will not be saved if
 *
 * <ul>
 *  <li>no name is specified or if the name is equal to another study's name.</li>
 *  <li>no description is provided</li>
 *  <li>no responsible is defined</li>
 *  <li>
 *    no end date is specified, start date is larger than end date or provided
 *    dates are invalid
 *  </li>
 *  <li>
 *    non-integer values are provided for duration noOfProfessionals or
 *    noOfStudents
 *  </li>
 * </ul>
 *
 * Returns <CODE>edit</CODE> when the study form should be displayed. Returns
 * <CODE>relations-publications</CODE> to attach/detach publications to the study
 * and <CODE>relations-studymaterial</CODE> to attach/detach study material.
 * Returns <CODE>return</CODE> when cancel is hit or the changes have been successfully
 * saved.
 */
public class ReportDefinitionsEditAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {

    ///////////////////////////////////////////////////////
    // Inputs that redirects the user to other
    // actions than this one.
    
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
    
    ReportDefinitionForm form = (ReportDefinitionForm)actionForm;
    ActionErrors errors = null;
    Simula simula = SimulaFactory.getSimula();

    if(saveChanges(request)) {

      errors = validate(simula, form);
      if(errors == null) {

        ReportDefinition reportDefinition;
      
        // Get existing report definition from cache to allow tables in session to be 
        // updated automatically (they point to the same object in memory)
        if(isNewReportDefinition(form)) {
          reportDefinition = new ReportDefinition();
        }
        else {
          reportDefinition = simula.getReportDefinition(form.getId());
        }

        BeanUtils.copyProperties(reportDefinition, form);
        
        if(isNewReportDefinition(form)) {
          Person admin = null;
          Principal user = request.getUserPrincipal();
          if(user != null) {
            String userId = user.getName();
            admin = simula.getPerson(userId);
          }
          form.setOwner(admin);
          reportDefinition.setOwner(admin);
          simula.addReportDefinition(reportDefinition);
        }
        else {
          simula.storeReportDefinition(reportDefinition);
        }
        
        return(mapping.findForward("return"));
      }
    }
    
    ///////////////////////////////////////////////////////
    // Mutually exclusive actions that directs the user
    // to the main view of this action
    
    if(isInitialRequest(request)) {
    
      if(isNew(request)) {
        form.setId(null);
        form.setTitle(null);
        form.setOperator("AND");
        form.setOwner(null);
        form.setColumns(new ArrayList(0));
        form.setResponsibles(new ArrayList(0));
      }
      else {
        Integer id = Integer.valueOf(request.getParameter("id"));
        BeanUtils.copyProperties(form, simula.getReportDefinition(id));
      }
      
      form.setPersons(new ArrayList(simula.getPersons()));
      form.getPersons().removeAll(form.getResponsibles());
      
      StudyColumn[] columns = {
          new StudyColumn("description"),
          new StudyColumn("duration"),
          new StudyColumn("end"),
          new StudyColumn("keywords"),
          new StudyColumn("lastEditedBy"),
          new StudyColumn("name"),
          new StudyColumn("noOfProfessionals"),
          new StudyColumn("noOfStudents"),
          new StudyColumn("notes"),
          new StudyColumn("ownedBy"),
          new StudyColumn("publications"),
          new StudyColumn("responsibles"),
          new StudyColumn("start"),
          new StudyColumn("studyMaterial"),
          new StudyColumn("type") };
      form.setAvailableColumns(new ArrayList(Arrays.asList(columns)));
      form.getAvailableColumns().removeAll(form.getColumns());
    }
    
    else if(addResponsibles(request)) {
      String[] add = form.getAddResponsible();
      for(int i = 0; i < add.length; i++) {
        Person person = simula.getPerson(add[i]);
        if(!form.getResponsibles().contains(person)) {
          form.getResponsibles().add(person);
        }
        form.getPersons().remove(person);
      }
      form.sortResponsibles();
    }

    else if(removeResponsibles(request)) {
      String[] remove = form.getRemoveResponsible();
      for(int i = 0; i < remove.length; i++) {
        Person person = simula.getPerson(remove[i]);
        if(!form.getPersons().contains(person)) {
          form.getPersons().add(person);
        }
        form.getResponsibles().remove(person);
      }
      form.sortPersons();
    }
    
    else if(addColumns(request)) {
      String[] add = form.getAddColumn();
      for(int i = 0; i < add.length; i++) {
        StudyColumn column = new StudyColumn(add[i]);
        if(!form.getColumns().contains(column)) {
          form.getColumns().add(column);
        }
        form.getAvailableColumns().remove(column);
      }
    }

    else if(removeColumns(request)) {
      String[] remove = form.getRemoveColumn();
      for(int i = 0; i < remove.length; i++) {
        StudyColumn column = new StudyColumn(remove[i]);
        if(!form.getAvailableColumns().contains(column)) {
          form.getAvailableColumns().add(column);
        }
        form.getColumns().remove(column);
      }
      form.sortAvailableColumns();
    }
    
    if(errors != null) {
      saveErrors(request, errors);
    }
    return(mapping.findForward("edit"));
  }
  
  ///////////////////////////////////////////////////////////
  // Helper methods to make main method cleaner and more
  // readable.

  private ActionErrors validate(Simula simula, ReportDefinitionForm form) throws SimulaException {
    ActionErrors errors = new ActionErrors();

    if(form.getTitle() == null || form.getTitle().length() == 0) {
      ActionError error = new ActionError("errors.required", "title");
      errors.add("title", error);
    }

    if(form.getOperator() == null || form.getOperator().length() == 0) {
      ActionError error = new ActionError("errors.required", "operator");
      errors.add("operator", error);
    }
    
    if(form.getColumns().size() == 0) {
      ActionError error = new ActionError("errors.minoccur", "1 column");
      errors.add("selectedColumns", error);
    }

    if(errors.isEmpty()) {
      return(null);
    }
    else {
      return(errors);
    }
  }

  private boolean empty(String string) {
    return(string == null || string.length() == 0);
  }
  
  private boolean isInitialRequest(HttpServletRequest request) {
    return(request.getParameter("new") != null || request.getParameter("id") != null);
  }
  
  private boolean isNew(HttpServletRequest request) {
    return(request.getParameter("new") != null);
  }

  private boolean addResponsibles(HttpServletRequest request) {
    return(request.getParameter("addResponsibles") != null);
  }

  private boolean removeResponsibles(HttpServletRequest request) {
    return(request.getParameter("removeResponsibles") != null);
  }
  
  private boolean addColumns(HttpServletRequest request) {
    return(request.getParameter("addColumns") != null);
  }

  private boolean removeColumns(HttpServletRequest request) {
    return(request.getParameter("removeColumns") != null);
  }
  
  private boolean saveChanges(HttpServletRequest request) {
    return(request.getParameter("save") != null);
  }

  private boolean isNewReportDefinition(ReportDefinitionForm form) {
    return(form.getId() == null);
  }  
}