package no.simula.des.presentation.web.actions;

import org.apache.struts.action.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.Study;
import no.simula.des.DurationUnit;
import no.simula.des.presentation.web.forms.SimpleForm;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionErrors;

/** Handles creation of new duration units, editing existing duration units and
 * saving changes in both cases.
 * <p>
 * The duration unit will not be saved if no name is specified or if the name is equal
 * to another duration unit's name.
 * <p>
 * Returns <CODE>edit</CODE> when the duration unit form should be displayed and
 * <CODE>return</CODE> when cancel is hit or the changes have been successfully
 * saved.
 */
public class DurationUnitsEditAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {

    ///////////////////////////////////////////////////////
    // Inputs that redirects the user to other
    // actions than this one.
    
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
        
    SimpleForm form = (SimpleForm)actionForm;
    ActionErrors errors = null;
    Simula simula = SimulaFactory.getSimula();
    
    if(saveChanges(request)) {

      errors = validate(simula, form);
      if(errors == null) {
        
        DurationUnit durationUnit;

        // Get existing study type from cache to allow tables in session to be 
        // updated automatically (they point to the same object in memory)
        if(isNewDurationUnit(form)) {
          durationUnit = new DurationUnit();
        }
        else {
          durationUnit = simula.getDurationUnit(form.getId());
        }
        BeanUtils.copyProperties(durationUnit, form);
        
        if(isNewDurationUnit(form)) {
          simula.addDurationUnit(durationUnit);
        }
        else {
          durationUnit.setId(form.getId());
          simula.storeDurationUnit(durationUnit);
        }

        return(mapping.findForward("return"));
      }
    }

    if(isInitialRequest(request)) {      
      if(isNew(request)) {
        form.setId(null);
        form.setName(null);
      }
      else {
        Integer id = Integer.valueOf(request.getParameter("id"));
        BeanUtils.copyProperties(form, simula.getDurationUnit(id));
      }
    }

    if(errors != null) {
      saveErrors(request, errors);
    }
    return(mapping.findForward("edit"));
  }

  ///////////////////////////////////////////////////////////
  // Helper methods to make main method cleaner and more
  // readable.

  private ActionErrors validate(Simula simula, SimpleForm form) throws Exception {
    ActionErrors errors = new ActionErrors();

    if(form.getName() == null || form.getName().length() == 0) {
      ActionError error = new ActionError("errors.required", "name");
      errors.add("name", error);
    }
    
    for(Iterator i = simula.getDurationUnits().iterator(); i.hasNext();) {
      DurationUnit durationUnit = (DurationUnit)i.next();
      if(
        (
          form.getId() == null ||
          (form.getId() != null && !form.getId().equals(durationUnit.getId()))
        ) &&
        durationUnit.getName().trim().equals(form.getName().trim())
      ) {
        ActionError error = new ActionError("errors.name.equal", "study type", form.getName());
        errors.add("name", error);
        break;
      }
    }
        
    if(errors.isEmpty()) {
      return(null);
    }
    else {
      return(errors);
    }
  }
  
  private boolean isInitialRequest(HttpServletRequest request) {
    return(request.getParameter("new") != null || request.getParameter("id") != null);
  }
  
  private boolean isNew(HttpServletRequest request) {
    return(request.getParameter("new") != null);
  }

  private boolean attachFile(HttpServletRequest request) {
    return(request.getParameter("attach") != null);
  }

  private boolean removeFile(HttpServletRequest request) {
    return(request.getParameter("remove") != null);
  }
  
  private boolean uploadFile(HttpServletRequest request) {
    return(request.getParameter("upload") != null);
  }
  
  private boolean attachIsCancelled(HttpServletRequest request) {
    return(request.getParameter("attach-cancel") != null);
  }

  private boolean saveChanges(HttpServletRequest request) {
    return(request.getParameter("save") != null);
  }

  private boolean isNewDurationUnit(SimpleForm form) {
    return(form.getId() == null);
  }
}