package no.simula.des;

import java.util.List;

import no.halogen.persistence.Persistable;
import no.simula.Person;

public class ReportDefinition implements Persistable {

	private Integer id;
	private String title;
	private Person owner;
	private boolean sort;
	private String operator;
	
	private List columns;
	private List responsibles;

	public ReportDefinition(Integer id) {
	  this.id = id;
  }

  public ReportDefinition() {
  }

  public Integer getId() {
		return (id);
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	public boolean isSort() {
		return sort;
	}

  public Boolean getSort() {
    return new Boolean(sort);
  }

	public void setSort(boolean sort) {
		this.sort = sort;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String toString() {
		return "ReportDefinition[title: " + title + "]";
	}

	public List getColumns() {
		return columns;
	}

	public void setColumns(List columns) {
		this.columns = columns;
	}

	public List getResponsibles() {
		return responsibles;
	}

	public void setResponsibles(List criteria) {
		this.responsibles = criteria;
	}

  public boolean equals(Object obj) {
    if (obj == null) { return false; }
    if (obj == this) { return true; }
    if (obj.getClass() != getClass()) {
      return false;
    }
    ReportDefinition that = (ReportDefinition) obj;
    Object thatId = that.getId();
    return(thatId.equals(this.id));
  }

}
