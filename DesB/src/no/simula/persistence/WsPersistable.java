package no.simula.persistence;

/**
 * Objects that shall be persisted must implement
 * this interface
 *
 * @author  Stian Eide
 */
public interface WsPersistable {
  
  /**
   * Returns the id in the persistent store.
   * @return the id
   */
  public String getId();
  /**
   * Sets the id in the persistent store
   * @param id the id
   */
  public void setId(String id);

}