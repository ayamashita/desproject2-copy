package no.simula.statements;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import no.halogen.statements.StatementException;
import no.simula.Publication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

public class PublicationStatement extends AbstractWebServiceStatement {

	private static Log log = LogFactory.getLog(PublicationStatement.class);
	
  private static String webServiceMethod;

	public List executeSelect() throws StatementException {
		HashMap hash = new HashMap();
		Object[] methodParams = new Object[]{ hash };

		XmlRpcClient client = setupXmlRpcClient();

		List result = new ArrayList();
		try {
			log.debug("start");
			Object test = client.execute(webServiceMethod, methodParams);
			log.debug("end");
			Object[] list = (Object[])test;
			for (int i = 0; i < list.length; i++) {
				HashMap element = (HashMap)list[i];
				
				Publication publication = fillPublication(element);
				
				result.add(publication);
			}
		} catch (XmlRpcException e) {
			log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
			throw new StatementException(e.toString());
		}
		
		return result;
	}

	public List executeSelectByTitle() throws StatementException {
		XmlRpcClient client = setupXmlRpcClient();

		List result = new ArrayList();
		try {
			for (Iterator iterator = getConditions().iterator(); iterator.hasNext();) {
				String title = (String) iterator.next();
				
				HashMap hash = new HashMap();
				hash.put("title", title);
				Object[] methodParams = new Object[]{ hash };
	
				log.debug("start");
				Object test = client.execute(webServiceMethod, methodParams);
				log.debug("end");
				Object[] list = (Object[])test;
				for (int i = 0; i < list.length; i++) {
					HashMap element = (HashMap)list[i];
					Publication publication = fillPublication(element);
					
					result.add(publication);
				}
			}
		} catch (XmlRpcException e) {
			log.error("executeSelectByTitle() - XmlRpcException while retrieving persons.", e);
			throw new StatementException(e.toString());
		}
		
		return result;
	}

	public List executeSelectByAuthor() throws StatementException {
		XmlRpcClient client = setupXmlRpcClient();

		List result = new ArrayList();
		try {
			for (Iterator iterator = getConditions().iterator(); iterator.hasNext();) {
				String author = (String) iterator.next();
				
				HashMap hash = new HashMap();
				hash.put("authors", author);
				Object[] methodParams = new Object[]{ hash };
	
				log.debug("start");
				Object test = client.execute(webServiceMethod, methodParams);
				log.debug("end");
				Object[] list = (Object[])test;
				for (int i = 0; i < list.length; i++) {
					HashMap element = (HashMap)list[i];
					Publication publication = fillPublication(element);
					
					result.add(publication);
				}
			}
		} catch (XmlRpcException e) {
			log.error("executeSelectByTitle() - XmlRpcException while retrieving persons.", e);
			throw new StatementException(e.toString());
		}
		
		return result;
	}

	public Object executeSelect(Object entityId) throws StatementException {
		XmlRpcClient client = setupXmlRpcClient();

		HashMap hash = new HashMap();
		hash.put("id", entityId);
		Object[] methodParams = new Object[]{ hash };

		Publication publication = new Publication();
		try {
			log.debug("start");
			Object test = client.execute(webServiceMethod, methodParams);
			log.debug("end");
			Object[] list = (Object[])test;
			HashMap element = (HashMap)list[0];
			publication = fillPublication(element);
				
		} catch (XmlRpcException e) {
			log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
			throw new StatementException(e.toString());
		}
		
		return publication;
	}

	public List executeSelect(List entityIds) throws StatementException {
		XmlRpcClient client = setupXmlRpcClient();

		List result = new ArrayList();
		try {
			for (Iterator iterator = entityIds.iterator(); iterator.hasNext();) {
				String id = (String) iterator.next();
				
				HashMap hash = new HashMap();
				hash.put("id", id);
				Object[] methodParams = new Object[]{ hash };
	
				log.debug("start");
				Object test = client.execute(webServiceMethod, methodParams);
				log.debug("end");
				Object[] list = (Object[])test;
				for (int i = 0; i < list.length; i++) {
					HashMap element = (HashMap)list[i];
					Publication publication = fillPublication(element);
					
					result.add(publication);
				}
			}
		} catch (XmlRpcException e) {
			log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
			throw new StatementException(e.toString());
		}
		
		return result;
	}

	private Publication fillPublication(HashMap element) {
		
		Publication publication = new Publication();
		
		publication.setId((String) element.get("id"));
		publication.setUrl((String)element.get("url"));
		try {
			publication.setTitle(URLDecoder.decode((String) element.get("title"), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// Until we use UTF-8, this exception will never arise
		}
		
		StringBuffer authors = new StringBuffer();
		Object[] authorsArray = (Object[]) element.get("authors");
	    boolean firstIteration = true;
		for (int j = 0; j < authorsArray.length; j++) {
			HashMap author = (HashMap) authorsArray[j];
			if (!firstIteration) {
				authors.append(", ");
			}

			// gets the author name
			authors.append(author.get("lastname")).append(" ").append(author.get("firstname"));

			firstIteration = false;
		}
		publication.setAuthors(authors.toString());
		
		return publication;
	}

  public static String getWebServiceMethod() {
    return webServiceMethod;
  }

  public static void setWebServiceMethod(String webServiceMethod) {
    PublicationStatement.webServiceMethod = webServiceMethod;
  }

}
