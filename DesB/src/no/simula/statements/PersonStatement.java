package no.simula.statements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import no.simula.Person;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

public class PersonStatement extends AbstractWebServiceStatement {

	private static Log log = LogFactory.getLog(PersonStatement.class);
	
  private static String webServiceMethod;

  public Object executeSelect(Object id) {
		XmlRpcClient client = setupXmlRpcClient();

		HashMap hash = new HashMap();
		hash.put("id", id);
		Object[] methodParams = new Object[]{ hash };

		Person person = new Person();
		try {
			Object test = client.execute(webServiceMethod, methodParams);
			Object[] list = (Object[])test;
			HashMap element = (HashMap)list[0];
			person = fillPerson(element);
				
		} catch (XmlRpcException e) {
			log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
		}
		
		return person;
	}

	public List executeSelect(List entityIds) {
		XmlRpcClient client = setupXmlRpcClient();

		List result = new ArrayList();
		try {
			for (Iterator iterator = entityIds.iterator(); iterator.hasNext();) {
				String id = (String) iterator.next();
				
				HashMap hash = new HashMap();
				hash.put("id", id);
				Object[] methodParams = new Object[]{ hash };
	
				//Object test = client.execute("getPeople", methodParams);
				Object test = client.execute(webServiceMethod, methodParams);
				Object[] list = (Object[])test;
				for (int i = 0; i < list.length; i++) {
					HashMap element = (HashMap)list[i];
					Person person = fillPerson(element);
					
					result.add(person);
				}
			}
		} catch (XmlRpcException e) {
			log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
		}
		
		return result;
	}

	public List executeSelect() {
		HashMap hash = new HashMap();
		Object[] methodParams = new Object[]{ hash };

		XmlRpcClient client = setupXmlRpcClient();

		List result = new ArrayList();
		try {
			Object test = client.execute(webServiceMethod, methodParams);
			Object[] list = (Object[])test;
			for (int i = 0; i < list.length; i++) {
				HashMap element = (HashMap)list[i];
				Person person = fillPerson(element);
				
				result.add(person);
			}
		} catch (XmlRpcException e) {
			log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
		}
		
		return result;
	}

	public List executeSelectByEmail(String email) {
		HashMap hash = new HashMap();
		hash.put("email", email);
		Object[] methodParams = new Object[]{ hash };

		XmlRpcClient client = setupXmlRpcClient();

		List result = new ArrayList();
		try {
			Object test = client.execute(webServiceMethod, methodParams);
			Object[] list = (Object[])test;
			for (int i = 0; i < list.length; i++) {
				HashMap element = (HashMap)list[i];
				Person person = fillPerson(element);
				
				result.add(person);
			}
		} catch (XmlRpcException e) {
			log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
		}
		
		return result;
	}

	private Person fillPerson(HashMap element) {
		Person person = new Person();
		person.setId((String) element.get("id"));
		person.setFirstName((String) element.get("firstname"));
		person.setFamilyName((String) element.get("lastname"));
		person.setEmail((String) element.get("email"));
		person.setUrl((String) element.get("url"));
		return person;
	}

  public static String getWebServiceMethod() {
    return webServiceMethod;
  }

  public static void setWebServiceMethod(String webServiceMethod) {
    PersonStatement.webServiceMethod = webServiceMethod;
  }

}
