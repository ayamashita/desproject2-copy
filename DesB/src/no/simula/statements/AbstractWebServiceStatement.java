package no.simula.statements;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import no.halogen.statements.ObjectStatement;
import no.halogen.statements.StatementException;

public abstract class AbstractWebServiceStatement implements ObjectStatement {

	private List conditions = new ArrayList();
	
	private static String webServiceUrl;

	public AbstractWebServiceStatement() {
		super();
	}

	public abstract List executeSelect(List entityIds)
			throws StatementException;

	public abstract Object executeSelect(Object entityId)
			throws StatementException;

	public abstract List executeSelect() throws StatementException;

	public boolean executeDelete() throws StatementException {
		return false;
	}

	public boolean executeDelete(Object entityId) throws StatementException {
		return false;
	}

	public Object executeInsert() throws StatementException {
		return null;
	}

	public boolean executeUpdate() throws StatementException {
		return false;
	}

	public boolean executeUpdate(Object entityId) throws StatementException {
		return false;
	}

	public List fetchListResults(ResultSet resultSet) throws SQLException {
		return null;
	}

	public Object fetchResults(ResultSet resultSet) throws SQLException {
		return null;
	}

	public List getConditions() {
		return conditions;
	}

	public void setConditions(List conditions) {
		this.conditions = conditions;
	}

	protected XmlRpcClient setupXmlRpcClient() {
		XmlRpcClient client;
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		client = new XmlRpcClient();
		try {
			//config.setServerURL(new URL("http://simula.no:9080/simula/xmlrpcdes"));
			config.setServerURL(new URL(webServiceUrl));
		} catch (MalformedURLException e) {
			// We know that url is right.
		}
		client.setConfig(config);
		return client;
	}

  public static String getWebServiceUrl() {
    return webServiceUrl;
  }

  public static void setWebServiceUrl(String webServiceUrl) {
    AbstractWebServiceStatement.webServiceUrl = webServiceUrl;
  }

}