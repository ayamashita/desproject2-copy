/*
 * Created on 01.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.search;

import no.halogen.search.CriteriumException;
import no.halogen.search.CriteriumImpl;
import no.halogen.statements.ObjectStatement;
import no.simula.statements.PublicationStatement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class SearchByPublicationAuthorImpl extends CriteriumImpl implements SearchByPublicationAuthor {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(SearchByPublicationAuthor.class);

  /* (non-Javadoc)
   * @see no.halogen.search.Criterium#generateWhereClause()
   */
  /**
   * Method generateWhereClause
   * @return String
   * @throws CriteriumException
   * @see no.halogen.search.Criterium#generateWhereClause()
   */
  public String generateWhereClause() throws CriteriumException {
    return null;
  }

  /* (non-Javadoc)
   * @see no.halogen.search.Criterium#getStatements()
   */
  /**
   * Method getStatement
   * @return ObjectStatement
   * @see no.halogen.search.Criterium#getStatement()
   */
  public ObjectStatement getStatement() {
    return (new PublicationStatement());
  }

}
