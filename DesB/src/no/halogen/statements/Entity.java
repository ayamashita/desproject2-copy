/**
 * @(#) Entity.java
 */

package no.halogen.statements;

/**
 * An antity represents a database table.
 * The entity class knows the table's columns and what database it is created in
 * 
 * @author Frode Langseth
 */
public interface Entity
{
	/**
	 * Method getInsertColumnNames
	 * Gets the names and order of the table columns when performing an insert (creation) of an entity
	 * 
	 * @return String the column names and order to use in an insert statement 
	 */
	String getInsertColumnNames( );
	
	/**
	 * Method getKey
	 * Name of the key column for the table the <code>Entity</code> represents
	 * As for thes version, the key can only be ont column
	 * 
	 * @return String the name of the Key column
	 */
	String getKey( );
	
	/**
	 * Method getSelectColumnNames
	 * Name and order of columns that will be fetched from a table when a select statement is executed
	 * @return String the column names
	 */
	String getSelectColumnNames( );
	
	/**
	 * Method getTableName
	 * The phisycal name of the table in the database
	 * @return String the table name
	 */
	String getTableName( );
	
	/**
	 * Method getUpdateValuesString
	 * Column names to use in an update statement.
	 * 
	 * To make it possible to use the statement in a <code>PreparedStatement</code>, each column name is succeeded by "= ?", where the '?' character represents the column value.
	 *  
	 * @return String column names and value representators for the update statement 
	 */
	String getUpdateValuesString( );
	
	/**
	 * Method getWhereClause
	 * Gets where clause to use in statements where it is applicable (select, delete & update)
	 * @return String the where clause
	 */
	String getWhereClause( );
	
	
}
