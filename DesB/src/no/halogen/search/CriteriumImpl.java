/*
 * Created on 31.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.halogen.search;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements common methods for all criteria
 * @author Frode Langseth
 */
public abstract class CriteriumImpl implements Criterium {
  /**
   * Field DEFAULT_OPERATOR
   */
  private final String DEFAULT_OPERATOR = new String(" = ");
  /**
   * Field DEFAULT_JOINCONDITION
   */
  private final String DEFAULT_JOINCONDITION = new String(" AND ");

  /**
   * Field operator
   */
  private String operator = DEFAULT_OPERATOR;
  /**
   * Field joinCondition
   */
  private String joinCondition = DEFAULT_JOINCONDITION;

  /**
   * Field attributes
   */
  private List attributes = new ArrayList();

  /**
  * Sets values for the criterium
  * 
  * @author Frode Langseth
  * @version 0.1
  * @param attributes - A List of criteria values
  * 
  * @see no.halogen.search.Criterium#addAttributes(List)
   */
  public void addAttributes(List attributes) {
    this.attributes = attributes;
  }

  /**
   * Add a value for the criterium
   * 
   * @author Frode Langseth
   * @version 0.1
   * @param attribute - The value to add the criterium
   * 
   * @see no.halogen.search.Criterium#addAttribute(Object)
   */
  public void addAttribute(Object attribute) {
    this.attributes.add(attribute);
  }

  /**
  	* return Attributes to populate where statement in a query
  	 * @see no.halogen.search.Criterium#getAttributes()
  	*/
  public List getAttributes() {
    return attributes;
  }

  /** 
   * @see no.halogen.search.Criterium#getOperator()
   */
  public String getOperator() {
    return (operator);
  }

  /**
   * @see no.halogen.search.Criterium#setOperator(java.lang.String)
   */
  public void setOperator(String operator) {
    this.operator = operator;
  }

  /**
   * @see no.halogen.search.Criterium#getJoinCondition()
   */
  public String getJoinCondition() {
    return (joinCondition);
  }

  /**
   * @see no.halogen.search.Criterium#setJoinCondition(java.lang.String)
   */
  public void setJoinCondition(String joinCondition) {
    this.joinCondition = joinCondition;
  }
}
