/*
 * Created on 01.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.halogen.search;

import java.util.List;


/**
 * When a search criterium is composed by more than one field (that is, the value is to be searched for in more than one field), the criterium must implement this interface
 * A composite criteium is composed by several other <code>Criterium</code> classes
 * @author Frode Langseth
 * @see no.halogen.search.Criterium
 */
public interface CompositeCriterium {
	/**
	 * Method getCriteria
   * Get the <code>Criterium</code>s that makes up the <code>ComposedCriterium</code>
	 * @return List the <code>Crituerium</code>s in the <code>ComposedCriterium</code>
	 */
	List getCriteria();
	/**
	 * Method getCriterium
   * Get one specific <code>Crituerium</code>
	 * @param index Integer the number of the <code>Criterium</code> to get
	 * @return Criterium the <code>Criterium</code>
	 */
	Criterium getCriterium(Integer index);
	/**
	 * Method getStatements
   * Gets the statements for the sub criteria in the <code>CompositeCriterium</code>
	 * @return List of statements, related to the sub criteria
	 */
	List getStatements();
}
