package no.halogen.utils.table;

/** A column that contains a link to an action or a url.
 *  This differs from
 *  <CODE>TableColumn</CODE> and <CODE>SortableTableColumn</CODE> in that it
 *  has the same display content in each row, as specifed by
 *  the <CODE>buttonKey</CODE>.
 *
 *  The target of the link in this column is set by specifying one of
 *  <CODE>forward</CODE>, <CODE>href</CODE>, <CODE>page</CODE> or
 *  <CODE>action</CODE>.
 *
 *  @author Stian Eide
 */
public class ActionTableColumn extends TableColumn {
  
  /** Creates a new <CODE>ActionTableColumn</CODE>.
   * @param forward logical forward name for which to look up the context-relative URI (if specified)
   *
   * @param href URL to be utilized unmodified (if specified)
   *
   * @param page module-relative page for which a URL should be created (if specified)
   *
   * @param buttonKey the key in the application resource that contains the label of the link in this
   * column
   * @param labelKey the key to the label heading in application resources
   * @param action logical action name for which to look up the context-relative URI (if specified)
   * @param paramName the name of the parameter, which is appended to links in this colunm
   * @param property specify the property of the content of this column, which value will be appended
   * to links in this column
   * @param maxSize the maximum length of values. Longer values are clipped
   */
  public ActionTableColumn(
    String labelKey, 
    String buttonKey,
    String forward,
    String href,
    String page,
    String action, 
    String paramName, 
    String property, 
    Integer maxSize
  ) {
    super(null, labelKey, forward, href, page, action, paramName, property, maxSize);
    this.buttonKey = buttonKey;
  }

  private String buttonKey;  
  
  /** Getter for property buttonKey.
   * @return the key in the application resource that contains the label of the link in this
   * column
   */
  public java.lang.String getButtonKey() {
    return buttonKey;
  }
  
  /** Setter for property buttonKey.
   * @param buttonKey the key in the application resource that contains the label of the link in this
   * column
   */
  public void setButtonKey(java.lang.String buttonKey) {
    this.buttonKey = buttonKey;
  }
  
  /** Returns the value for this column of the specified row. Always returns the value
   * of <CODE>buttonKey</CODE>
   * @param row the object that represents the row to extract value from
   * @return the value for this column in row
   */  
  public Object getRowValue(Object row) {
    return(buttonKey);
  }    
}