package no.halogen.utils.table;

import java.util.List;

/** <CODE>TableSource</CODE> is used as a data source for <CODE>Table</CODE>s.
 * Implementations must implement an update()-method that updates the source and a
 * getContent()-method that returns every row as an element in a <CODE>List</CODE>.
 * @author Stian Eide
 */
public interface TableSource {
  
  /** Implementations should make sure that the table source is up-to-date after
   * invoking this method.
   */  
  public void update();
  /** Returns the content of this source as a <CODE>List</CODE> where each element
   * represents a row in the <CODE>Table</CODE>.
   * @return a <CODE>List</CODE> of rows
   */  
  public List getContent();
  
}