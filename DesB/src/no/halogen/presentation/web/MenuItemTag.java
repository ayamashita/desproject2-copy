package no.halogen.presentation.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTag;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.IterationTag;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.util.RequestUtils;

/**
 *  Renders a link based on the provided action. If
 *  <code>newAction</code> is set, a ?new=true will be
 *  appended to the url.
 */
public class MenuItemTag extends TagSupport {
  
  private static Log log = LogFactory.getLog(MenuItemTag.class);
  
  /** property declaration for tag attribute: action.
   */
  private String action;
  
  /** property declaration for tag attribute: new.
   */
  private boolean newAction = false;
  
  /** Creates a new instance of <CODE>MenuItemTag</CODE>. */  
  public MenuItemTag() {
    super();
  }
  
  /**
   * Fill in this method to perform other operations from doStartTag().
   */
  public void otherDoStartTagOperations()  {
    
    try {
      JspWriter out = pageContext.getOut();
      
      String labelKeyExt = "";
      String urlExt = "";
      
      if(newAction) {
        labelKeyExt = ".new";
        urlExt = "?new=true";
      }
      
      String labelKey = "navigation.history." + action + labelKeyExt;
      String label = RequestUtils.message(pageContext, Globals.MESSAGES_KEY, Globals.LOCALE_KEY, labelKey);
      String url = RequestUtils.computeURL(pageContext, null, null, null, action, null, null, false);
      out.print("<a href=\"" + url + urlExt + "\">" + label + "</a>");
      
    }
    catch (Exception e) {
      log.error("Could not render menu item.", e);
    }
  }
  
  /** Fill in this method to determine if the tag body should be evaluated
   * Called from doStartTag().
   * @return <CODE>true</CODE>
   */
  public boolean theBodyShouldBeEvaluated()  {    
    return(true);
  }
    
  /**
   * Fill in this method to perform other operations from doEndTag().
   */
  public void otherDoEndTagOperations() {}
  
  /** Fill in this method to determine if the rest of the JSP page
   * should be generated after this tag is finished.
   * Called from doEndTag().
   * @return <CODE>true</CODE>
   */
  public boolean shouldEvaluateRestOfPageAfterEndTag()  {
    return(true);
  }
  
  /** This method is called when the JSP engine encounters the start tag,
   * after the attributes are processed.
   * Scripting variables (if any) have their values set here.
   * @return EVAL_BODY_INCLUDE if the JSP engine should evaluate the tag body, otherwise return SKIP_BODY.
   * This method is automatically generated. Do not modify this method.
   * Instead, modify the methods that this method calls.
   * @throws JspException if an <CODE>JspException</CODE> is thrown from <CODE>otherDoEndTagOperations()</CODE>
   * or <CODE>shouldEvaluateRestOfPageAfterEndTag()</CODE>
   */
  public int doStartTag() throws JspException {
    otherDoStartTagOperations();
    
    if(theBodyShouldBeEvaluated()) {
      return(EVAL_BODY_INCLUDE);
    }
    else {
      return(SKIP_BODY);
    }
  }
  
  /** .
   * This method is called after the JSP engine finished processing the tag.
   * @return EVAL_PAGE if the JSP engine should continue evaluating the JSP page, otherwise return SKIP_PAGE.
   * This method is automatically generated. Do not modify this method.
   * Instead, modify the methods that this method calls.
   * @throws JspException if a <CODE>JspException</CODE> is thrown from <CODE>otherDoEndTagOperations()</CODE>
   * or <CODE>shouldEvaluateRestOfPageAfterEndTag()</CODE>
   */
  public int doEndTag() throws JspException {
    otherDoEndTagOperations();
    
    if(shouldEvaluateRestOfPageAfterEndTag()) {
      return(EVAL_PAGE);
    }
    else {
      return(SKIP_PAGE);
    }
  }
  
  /** Returns the action of this menu item.
   * @return the path of the action
   */  
  public String getAction() {
    return(action);
  }
  
  /** Set the action of this menu item
   * @param value the action path of the menu item
   */  
  public void setAction(String value) {
    action = value;
  }
  
  /** Returns whether this menu item is a new-request
   * @return true - if this is a new-request
   */  
  public boolean getNew() {
    return(newAction);
  }
  
  /** Set whether this is a new-requst
   * @param value true if this is a new request
   */  
  public void setNew(boolean value) {
    newAction = value;
  }
}