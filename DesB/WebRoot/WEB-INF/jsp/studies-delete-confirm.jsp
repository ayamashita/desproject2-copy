<%@include file="include/include-top.jsp"%>

    <h1>Confirm delete studies</h1>

    <html:form action="/adm/studies/delete">
      <html:hidden name="<%= Constants.DELETE_THIS_STUDY %>" property="id"/>

      <table class="container">
        <tr>
          <td>
            Are you sure you want to delete the study
            <b><bean:write name="<%= Constants.DELETE_THIS_STUDY %>" property="name"/></b>?
          </td>
        </tr>
      </table>

      <html:submit property="confirm" value="delete study"/>
      <html:cancel property="skip" value="skip study"/>
      <html:cancel value="cancel"/>
    </html:form>

<%@include file="include/include-bottom.jsp"%>