<%@include file="include/include-top.jsp"%>

  <h1>
    <logic:present name="<%= Constants.FORM_REPORT_DEFINITION %>" property="id">Edit report definition</logic:present>
    <logic:notPresent name="<%= Constants.FORM_REPORT_DEFINITION %>" property="id">Register new report definition</logic:notPresent>
  </h1>

  <html:form action="/adm/reportdefinitions/edit" method="post">

  <logic:messagesPresent>
    <table class="error">
      <tr>
        <td>
          <ul>
            <html:messages id="error">
              <li><bean:write name="error"/></li>
            </html:messages>
          </ul>
        </td>
      </tr>
    </table>
  </logic:messagesPresent>

  <%-- The main table that divides the page into two columns --%>
        study information
        <table class="container">
          <tr>
            <td>title*</td>
            <td><html:text property="title" style="width: 100%"/></td>
          </tr>
          <tr>
            <td>sort by end date*</td>
            <td>
              <html:checkbox property="sort" />
            </td>
          </tr>
          <tr>
            <td>columns*</td>
            <td>
              <table class="container">
                <tr>
                  <th>available columns</th>
                  <th/>
                  <th>selected columns</th>
                </tr>
                <tr>
                  <td>
                    <logic:empty name="<%= Constants.FORM_REPORT_DEFINITION %>" property="availableColumns">
                      <html:select property="addColumn" multiple="true" size="5" disabled="true">
                        <html:option value="-1" key="lists.option.empty"/>
                      </html:select>
                    </logic:empty>
                    <logic:notEmpty name="<%= Constants.FORM_REPORT_DEFINITION %>" property="availableColumns">
                      <html:select property="addColumn" multiple="true" size="5">
                        <html:optionsCollection property="availableColumns" value="column" label="column"/>
                      </html:select>
                    </logic:notEmpty>
                  </td>
                  <td style="text-align: center">
                    <html:submit property="addColumns" value=">"/><br/>
                    <html:submit property="removeColumns" value="<"/>
                  </td>
                  <td>
                    <logic:empty name="<%= Constants.FORM_REPORT_DEFINITION %>" property="columns">
                      <html:select property="removeColumn" multiple="true" size="5" disabled="true">
                        <html:option value="-1" key="lists.option.empty"/>
                      </html:select>
                    </logic:empty>
                    <logic:notEmpty name="<%= Constants.FORM_REPORT_DEFINITION %>" property="columns">
                      <html:select property="removeColumn" multiple="true" size="5">
                        <html:optionsCollection property="columns" value="column" label="column"/>
                      </html:select>
                    </logic:notEmpty>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>condition operator*</td>
            <td>
              <html:radio property="operator" value="AND" />AND
              <html:radio property="operator" value="OR" />OR
            </td>
          </tr>
          <tr>
            <td>responsible*</td>
            <td>
              <table class="container">
                <tr>
                  <th>available persons</th>
                  <th/>
                  <th>responsibles</th>
                </tr>
                <tr>
                  <td>
                    <logic:empty name="<%= Constants.FORM_REPORT_DEFINITION %>" property="persons">
                      <html:select property="addResponsible" multiple="true" size="5" disabled="true">
                        <html:option value="-1" key="lists.option.empty"/>
                      </html:select>
                    </logic:empty>
                    <logic:notEmpty name="<%= Constants.FORM_REPORT_DEFINITION %>" property="persons">
                      <html:select property="addResponsible" multiple="true" size="5">
                        <html:optionsCollection property="persons" value="id" label="fullName"/>
                      </html:select>
                    </logic:notEmpty>
                  </td>
                  <td style="text-align: center">
                    <html:submit property="addResponsibles" value=">"/><br/>
                    <html:submit property="removeResponsibles" value="<"/>
                  </td>
                  <td>
                    <logic:empty name="<%= Constants.FORM_REPORT_DEFINITION %>" property="responsibles">
                      <html:select property="removeResponsible" multiple="true" size="5" disabled="true">
                        <html:option value="-1" key="lists.option.empty"/>
                      </html:select>
                    </logic:empty>
                    <logic:notEmpty name="<%= Constants.FORM_REPORT_DEFINITION %>" property="responsibles">
                      <html:select property="removeResponsible" multiple="true" size="5">
                        <html:optionsCollection property="responsibles" value="id" label="fullName"/>
                      </html:select>
                    </logic:notEmpty>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
  <%-- End of Main Table --%>

  <html:submit property="save" value="save changes"/>
  <html:cancel value="cancel"/>

 </html:form>

<%@include file="include/include-bottom.jsp"%>