<%@include file="include/include-top.jsp"%>

    <h1>Confirm delete report definitions</h1>

    <html:form action="/adm/reportdefinitions/delete">
      <html:hidden name="<%= Constants.DELETE_THIS_REPORT_DEFINITION %>" property="id"/>

      <table class="container">
        <tr>
          <td>
            Are you sure you want to delete the report definition
            <b><bean:write name="<%= Constants.DELETE_THIS_REPORT_DEFINITION %>" property="title"/></b>?
          </td>
        </tr>
      </table>

      <html:submit property="confirm" value="delete report definition"/>
      <html:cancel property="skip" value="skip report definition"/>
      <html:cancel value="cancel"/>
    </html:form>

<%@include file="include/include-bottom.jsp"%>