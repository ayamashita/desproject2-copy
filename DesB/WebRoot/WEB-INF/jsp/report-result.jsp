<%@include file="include/include-top.jsp"%>

  <h1>Report result</h1>

  <logic:messagesPresent>
    <table class="error">
      <tr>
        <td>
          <ul>
            <html:messages id="error">
              <li><bean:write name="error"/></li>
            </html:messages>
          </ul>
        </td>
      </tr>
    </table>
  </logic:messagesPresent>

  <logic:present name="<%= Constants.TABLE_REPORT_RESULT %>">
    <logic:notEmpty name="<%= Constants.TABLE_REPORT_RESULT %>" property="source.content">
      <halogen:table name="<%= Constants.TABLE_REPORT_RESULT %>" selectableRows="false" id="id"/>
      <html:link target="_blank" page="/pdf/studies_custom_report.pdf">printer friendly version</html:link>
    </logic:notEmpty>
    <logic:empty name="<%= Constants.TABLE_REPORT_RESULT %>" property="source.content">
      No matching studies found.
    </logic:empty>
  </logic:present>

<%@include file="include/include-bottom.jsp"%>
