<%@page import="no.machina.simula.*, java.util.*"%>
<%
    if ( true ) {   // Create a local scope
        String loginPage = getServletContext().getInitParameter("php.login.page");
        if ( loginPage == null || loginPage.length() == 0 ) {
            loginPage = "/login.php";
        }
        String loginHost = getServletContext().getInitParameter("php.login.host");
        if ( loginHost == null || loginHost.length() == 0 ) {
            loginHost = "localhost";
        }
        String loginProtocol = getServletContext().getInitParameter("php.login.protocol");
        if ( loginProtocol == null || loginProtocol.length() == 0 ) {
            loginProtocol = "https";
        }
        //String url = loginProtocol + "://" + loginHost + loginPage;
 		String url = loginProtocol + "://" + "localhost:8080/DesC/jsp/admin/main.jsp";
    // First get some stats about the user and the allowed actions
    HttpSession sess = request.getSession();
    String status = (String)sess.getAttribute("status");
    String userid = (String)sess.getAttribute("simula_userid");
    List actions = null;
    if ( status != null && status.equalsIgnoreCase("OK")) {
        actions = DB.getInstance().getUserActions(userid);
    } else {
        actions = new ArrayList();
    }
    sess.setAttribute("actionlist", actions);
%>

<table border="0" cellpadding="10">
<tr>
<td valign="top">
	
	<% if ( DB.getInstance().listContainsAction(actions, "VIEW_MAIN")) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/main.jsp">Admin main page</a></p>
	<% } %>
	
	<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/search.jsp">Search for studies</a></p>
	<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/statistics.jsp">Statistics</a></p>
	
	<% if ( DB.getInstance().listContainsAction(actions, "CREATE_STUDY")) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/study_edit.jsp">Create new study</a></p>
	<% } %>
	
	<% if ( DB.getInstance().listContainsAction(actions, "EDIT_USER")) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/edit_user.jsp">User privileges</a></p>
	<% } %>
	
	<% if ( DB.getInstance().listContainsAction(actions, "MAINTAIN_DATA")) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/dataAdmin_main.jsp">DB Admin</a></p>
	<% } %>

	<% if (true || status == null || status.length() == 0 || status.equalsIgnoreCase("NOTOK") || (status.equalsIgnoreCase("OK") && userid==null)) { %>
		<p class="bodytext-bold"><a href="<%=url%>">Login</a></p>
	<% } else if ( status.equalsIgnoreCase("OK") && userid != null && userid.length() > 0 ) { %>
		<p class="bodytext-bold"><a href="<%=request.getContextPath()%>/jsp/admin/logoff.jsp">Log off</a></p>
	<% } %>
</td>
</tr>
</table>

<% } %>