<%@page import="org.apache.commons.httpclient.*"%>
<%@page import="org.apache.commons.httpclient.methods.*"%>
<%@page import="org.apache.commons.httpclient.protocol.*"%>
<%@page import="org.apache.commons.httpclient.cookie.*"%>
<%@page import="org.apache.commons.httpclient.contrib.ssl.*"%>
<%@page import="org.apache.commons.httpclient.contrib.utils.*"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html"%>
<%
    // Logoff the simula site first
    if ( true ) {   // Create a local scope for this file
        String status = (String)session.getAttribute("status");
        String cookieName = getServletContext().getInitParameter("php.cookie.name");
        String logoffPage = getServletContext().getInitParameter("php.logoff.page");
        if ( logoffPage == null || logoffPage.length() == 0 ) {
            logoffPage = "/login.php?action=logoff";
        }
        String loginHost = getServletContext().getInitParameter("php.login.host");
        if ( loginHost == null || loginHost.length() == 0 ) {
            loginHost = "localhost";
        }
        String loginProtocol = getServletContext().getInitParameter("php.login.protocol");
        if ( loginProtocol == null || loginProtocol.length() == 0 ) {
            loginProtocol = "https";
        }
        // Get any cookies sent and find the php session id
        javax.servlet.http.Cookie[] cookies = request.getCookies();
        String domain = null;
        String value = null;
        String path = null;
        int maxAge = -1;
        StringBuffer sb = new StringBuffer();
            int size = cookies != null ? cookies.length : 0;
            for ( int i = 0;i < size;i++) {
            String name = cookies[i].getName();
            sb.append(name);
            sb.append(",");
            if ( name != null && name.equals(cookieName)) {
                domain = cookies[i].getDomain();
                value = cookies[i].getValue();
                path = cookies[i].getPath();
                maxAge = cookies[i].getMaxAge();
            }
        }

        // Send a request to the status page to discover the clients status
        GetMethod httpget = null;
        String res = null;
        String error = null;
        String stack = null;
        try {
            //if ( status == null || status.equalsIgnoreCase("NOTOK")) {
            HttpClient httpclient = new HttpClient();
            if ( loginProtocol.equals("https")) {
                Protocol myhttps = new Protocol(loginProtocol, new EasySSLProtocolSocketFactory(), 443);
                httpclient.getHostConfiguration().setHost(loginHost, 443, myhttps);
                httpclient.setConnectionTimeout(30000);
                httpget = new GetMethod(logoffPage);
            } else {
                httpget = new GetMethod(loginProtocol + "://" + loginHost  + logoffPage);
            }
            httpclient.getState().setCookiePolicy(CookiePolicy.COMPATIBILITY);
            org.apache.commons.httpclient.Cookie phpCookie = new org.apache.commons.httpclient.Cookie(loginHost, cookieName, value);
            phpCookie.setPath("/");
            httpclient.getState().addCookie(phpCookie);
            int result = httpclient.executeMethod(httpget);
        } catch(Exception ex) {
            error = ex.getClass().getName() + ": " + ex.getMessage();
        } finally {
            if ( httpget != null ) {
                httpget.releaseConnection();
            }
        }
    }


    // Then remove the local session values
    HttpSession sess = request.getSession();
    sess.setAttribute("status", "NOTOK");
    sess.removeAttribute("status");
    sess.removeAttribute("simula_username");
    sess.removeAttribute("simula_userright");
    sess.removeAttribute("simula_userid");
    sess.invalidate();
    response.sendRedirect("../search.jsp");
%>