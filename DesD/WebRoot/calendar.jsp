<%@ page import="com.tec.des.http.WebKeys" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>

<%
//Working on year 1000 to 9999?

Date now = new Date();
GregorianCalendar gc = new GregorianCalendar();

//Read input, if any
String sYear = (new SimpleDateFormat("yyyy")).format(now);
String sMonth = (new SimpleDateFormat("MM")).format(now);;
if (request.getParameter("year") != null) sYear = request.getParameter("year");
if (request.getParameter("month") != null) sMonth = request.getParameter("month");

//First day of the month. 
//NB: strange things happening with the DAY_OF_WEEK, getMinimalDaysInFirstWeek and getFirstDayOfWeek (local issues)?
//    getMinimalDaysInFirstWeek and getFirstDayOfWeek are the same for all months and years?
gc.setTime((new SimpleDateFormat("yyyyMMdd")).parse(sYear+sMonth+"01"));
int iFirstDayOfMonth = gc.get(GregorianCalendar.DAY_OF_WEEK);
iFirstDayOfMonth--; 
if (iFirstDayOfMonth == 0) iFirstDayOfMonth = 7;

//Number of days in the month.
int iNumberOfDaysInMonth = gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

//Number of lines to display. One "week" per line.
int iNumberOfDisplayingWeeks = (iFirstDayOfMonth + iNumberOfDaysInMonth - 1) / 7;
if (((iFirstDayOfMonth + iNumberOfDaysInMonth - 1) % 7) > 0) iNumberOfDisplayingWeeks++;

//Year and month when clicking the prevoius month button.
int iPreviousYear = Integer.parseInt(sYear);
int iPreviousMonth = Integer.parseInt(sMonth) - 1;
if (iPreviousMonth == 0) { iPreviousMonth = 12;  iPreviousYear--; }
String sPreviousMonth = (new DecimalFormat("00")).format(iPreviousMonth);

//Year and month when clicking the next month button.
int iNextYear = Integer.parseInt(sYear);
int iNextMonth = Integer.parseInt(sMonth) + 1;
if (iNextMonth == 13) { iNextMonth = 1;  iNextYear++; }
String sNextMonth = (new DecimalFormat("00")).format(iNextMonth);

//Set the name of the method that the calendar will execute when a date is choosen
String selectMethod = "addDate('" + request.getParameter(WebKeys.REQUEST_PARAM_DATE_FIELD) + "', ";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<HTML>
<HEAD>
<TITLE>Calendar</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK rel="stylesheet" type="text/css" href="simula.css">
<SCRIPT type="text/javascript" src="javascript.js"></SCRIPT>
</HEAD>

<BODY onload="this.focus()" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c">
<TABLE bgcolor="aeaeae" width="100%" border="0" cellspacing="0" cellpadding="0">
<FORM name="form" action="calendar.jsp" method="post">
  <TR>
    <TD valign="top">
      <TABLE width="100%" border="0" cellspacing="2" cellpadding="2">
        <TR> 
          <TD class="bodytext" align="center"> 
		<A href="JavaScript:document.form.submit()" onClick="document.form.year.value='<%=iPreviousYear%>';document.form.month.value='<%=sPreviousMonth%>';">Previous month</A>
	  </TD> 
	  <TD class="bodytext" align="center">
		<%=(new SimpleDateFormat("MMMM", Locale.ENGLISH)).format((new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH)).parse(sYear+sMonth+"01"))%>
          	<SELECT name="year" class="bodytext" onChange="document.form.submit();">
			<%for(int i = Integer.parseInt(sYear)-20; i <= Integer.parseInt(sYear)+20; i++) { %>
		              	<OPTION value="<%=i%>"<%if(i==Integer.parseInt(sYear)) {%> selected<% } %>><%=i%></OPTION>
			<% } %>
            	</SELECT>
          </TD> 
          <TD class="bodytext" align="center"> 
		<A href="JavaScript:document.form.submit()" onClick="document.form.year.value='<%=iNextYear%>';document.form.month.value='<%=sNextMonth%>';">Next month</A>
          </TD> 
        </TR> 
      </TABLE> 
      <TABLE width="100%" border="0" cellspacing="2" cellpadding="2" bgcolor="#f5f5f5">
        <TR align="center"> 
          <TD class="bodytext-bold">M</TD> 
          <TD class="bodytext-bold">T</TD> 
          <TD class="bodytext-bold">W</TD> 
          <TD class="bodytext-bold">T</TD> 
          <TD class="bodytext-bold">F</TD> 
          <TD class="bodytext-bold">S</TD> 
          <TD class="bodytext-bold">S</TD> 
        </TR> 
	<% for(int i = 0; i < iNumberOfDisplayingWeeks; i++) { %>
    	<TR align="center">
		<% for(int ii = 1; ii <= 7; ii++) { %>
			<TD class="bodytext<% if((new SimpleDateFormat("yyyyMMdd")).format(now).equals(sYear+sMonth+(new DecimalFormat("00")).format(i*7 + ii - iFirstDayOfMonth + 1))) { %> + currentdate<% } %>"> 
				<% if(((i*7 + ii) >= iFirstDayOfMonth) && ((i*7 + ii) < (iFirstDayOfMonth + iNumberOfDaysInMonth))) { %> <A href="JavaScript:<%=selectMethod%>'<%=sYear%>/<%=sMonth%>/<%=(new DecimalFormat("00")).format(i*7 + ii - iFirstDayOfMonth + 1)%>')"><%=i*7 + ii - iFirstDayOfMonth + 1%></A> <% } %>
			</TD> 
		<% } %>	
	    </TR> 
	<% } %>	
		<TR>
  		<TD colspan="7" class="bodytext">&nbsp;</TD> 
		</TR> 
		<TR>
  		<TD colspan="7" align="right" class="bodytext"><A href="Javascript:window.close();">Close</A>&nbsp;</TD> 
		</TR> 
    </TABLE> 
    </TD> 
  </TR> 
</TABLE> 
<INPUT type="hidden" name="month" value="<%=sMonth%>">
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_DATE_FIELD%>" value="<%=request.getParameter(WebKeys.REQUEST_PARAM_DATE_FIELD)%>">
</FORM>
</BODY>
</HTML>