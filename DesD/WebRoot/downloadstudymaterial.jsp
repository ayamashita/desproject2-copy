<%@ page import="java.io.*, javax.servlet.*, com.tec.shared.util.Parser, com.tec.des.http.WebKeys, com.tec.des.command.GetStudyMaterialCommand, com.tec.des.dto.StudyMaterialDTO, com.tec.des.util.Message" %>
<%
//Fetches a study material object and extracts the file from it
int iRead;
FileInputStream stream = null;
Message message = null;
try { 
    GetStudyMaterialCommand command = new GetStudyMaterialCommand();   
    command.setStudyMaterialId(Parser.parseInt(request.getParameter(WebKeys.REQUEST_PARAM_STUDY_MATERIAL_ID)));
    command.setFromTemp(Parser.parseBoolean(request.getParameter(WebKeys.REQUEST_PARAM_FROM_TEMP))); 
    command.execute();

    StudyMaterialDTO material = (StudyMaterialDTO)command.getResult().firstElement();
    File file = material.getFile();

    response.setContentLength((int)file.length());
    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition","attachment; filename=" + material.getFileName());

    stream = new FileInputStream(file);
    out.clear();
    while ((iRead = stream.read()) != -1) { 
        out.write(iRead); 
    } 
    out.flush();
    stream.close();
    file.delete();
    material.getFile().delete();
} catch (Exception ex) {
    message = new Message(ex.getMessage());
%>
<jsp:include page="top.jsp"/>
<TABLE cellspacing="6" cellpadding="6">
    <TR>
        <TD>
            <TABLE cellspacing="2" cellpadding="0"> 
            <% if (message != null) { %>
            <FONT class="errortext" ><%=message.getText()%></FONT><BR>
            <% } %>
            </TABLE>
        </TD>
    </TR> 
</TABLE> 
<jsp:include page="bottom.html"/>
<%
} finally {
    if (stream != null) {
        stream.close(); 
    }
}
%>
