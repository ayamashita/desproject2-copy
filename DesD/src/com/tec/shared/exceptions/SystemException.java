package com.tec.shared.exceptions;
/**
 * System exception, the main node of system exceptions in 
 * the system's exception hierarchy.
 *
 * @author :  Norunn Haug Christensen
 */
public class SystemException extends GeneralException  {
  private Throwable originalException;

  /**
   * Creates a SystemException with the specified message.
   */
  public SystemException(String message) {
    super(message);
  }

  /**
   * Creates a SystemException based on an original exception.
   */
  public SystemException(String message, Throwable originalException)
  {
    this(message);
    this.originalException = originalException;
  }
}