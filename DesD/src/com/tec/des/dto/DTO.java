package com.tec.des.dto;

/**
 * A Data Transfer Object encapsulates data, and is used for sending data 
 * between client and server. 
 * All attributes in the DTO should be private, and have public getter and
 * setter methods.
 * All Data Transfer Objects should extend the DTO interface. 
 *
 * @author :  Norunn Haug Christensen
 */
public interface DTO {
}