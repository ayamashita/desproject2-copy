package com.tec.des.util;
import com.tec.shared.util.Nuller;
/**
 * Message object is used to pass information to users.
 *
 * @author Norunn Haug Christensen
 */
public class Message {
  private String text = Nuller.getStringNull();

  public Message(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }

}


