package com.tec.des.dao;
import java.sql.*;
import java.util.*;
import com.tec.server.log4j.LoggerFactory;
import com.tec.des.dto.PublicationDTO;
import com.tec.shared.util.Nuller;
import com.tec.shared.exceptions.SystemException;
import com.tec.server.db.ResultSetHelper;
import com.tec.des.util.Validator;

/**
 * Class encapsulating Publication data access.
 *
 * @author :  Per Kristian Foss
 */
public class PublicationDAO extends DAO {

 /**
  * Constructs a PublicationDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
    public PublicationDAO() throws SystemException, SQLException {
    super();
  }

 /**
  * Returns a Vector containing selected Publications. 
  * @param String containing Id's of selected Publications
  * @return Vector
  * @throws SQLException
  */

  public Vector getSelectedPublications(String publicationIds) throws SQLException {
    
    Vector publications = new Vector();  
    StringBuffer query = new StringBuffer();
   
    query.append("select pu.publication_id, pu.publication_title, pu.publication_source ");
    query.append("from " + getSimulaDb() + ".publication pu where publication_id in (");
    query.append(publicationIds + ");");
        
    LoggerFactory.getLogger(PublicationDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);

    while (rs.next()) {
        PublicationDTO pub = new PublicationDTO();
        pub.setId(rsh.getInt("publication_id"));
        pub.setTitle(rsh.getString("publication_title"));
        pub.setSource(rsh.getString("publication_source"));
        publications.add(pub);
    }
    
    rs.close();
    stmt.close();
    
    return publications;
  }
  
 /**
  * Returns a Vector containing Publications, based on the given search criteria. 
  * @param String searchText containing the words typed in by user
  * @return Vector
  * @throws SQLException
  */

  public Vector findPublications(String searchText) throws SQLException { 

    Vector publications = new Vector();    
    StringBuffer selectlist = new StringBuffer();         
    StringBuffer query = new StringBuffer();    
    String[] searchCriteria = Validator.checkEscapes(searchText).split(" ");
    
    if (Nuller.isReallyNull(selectlist.append(searchOneCriteria(searchCriteria[0], null)).toString()))
        return publications;
    
    //Run if more than one word has been entered in search field
    if (searchCriteria.length > 1) {
        String newSelectlist;
        for (int i = 1; i < searchCriteria.length; i++) {            
            newSelectlist = searchOneCriteria(searchCriteria[i], selectlist.toString());            
            selectlist.delete(0, selectlist.length());  
            selectlist.append(newSelectlist);
        }    
    }
    
    //Run only if at least one publication has been found
    if (selectlist.length() > 0) {
        query.append("select pu.publication_id, pu.publication_title, pu.publication_source ");
        query.append("from " + getSimulaDb() + ".publication pu, " + getSimulaDb() + ".publication_author pa ");
        query.append("where pa.publication_id = pu.publication_id ");
        query.append("and pu.publication_id in (" + selectlist.toString() + ") ");
        query.append("group by pu.publication_id, pu.publication_title, pu.publication_source order by pu.publication_title;");
        
        LoggerFactory.getLogger(PublicationDAO.class).debug(query.toString()); 
        Statement stmt = getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query.toString());
        ResultSetHelper rsh = new ResultSetHelper(rs);
    
        while (rs.next()) {
            PublicationDTO pub = new PublicationDTO();
            pub.setId(rsh.getInt("publication_id"));
            pub.setTitle(rsh.getString("publication_title"));
            pub.setSource(rsh.getString("publication_source"));
            publications.add(pub);
        }
        
        rs.close();
        stmt.close();
    }
    
    return publications;
  }
  
 /**
  * Returns a String containing the Publications matching ONE search criteria. 
  * @param String with one search criteria, String with already found publications
  * @return String
  * @throws SQLException
  */
  
  public String searchOneCriteria(String criteria, String foundPubs) throws SQLException { 
    
    StringBuffer query = new StringBuffer();
    StringBuffer selectlist = new StringBuffer();
      
    query.append("select pu.publication_id from " + getSimulaDb() + ".publication pu, " + getSimulaDb() + ".publication_author pa ");    
    query.append("where pa.publication_id = pu.publication_id ");
    query.append("and (pa.external_name like '%" + criteria + "%' or ");
    query.append("pu.publication_title like '%" + criteria + "%' or ");
    query.append("pu.publication_source like '%" + criteria + "%' or ");
    query.append("pu.publication_abstract like '%" + criteria + "%')");
    if (foundPubs != null && foundPubs.length() > 0)
        query.append(" and pu.publication_id in (" + foundPubs + ")");
    query.append(";");
    
    LoggerFactory.getLogger(PublicationDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);

    while (rs.next()) {
        selectlist.append(String.valueOf(rsh.getInt("publication_id")));
        selectlist.append(",");
    }
    
    if (selectlist.length() > 0)
        selectlist.deleteCharAt(selectlist.length()-1);    
    
    rs.close();
    stmt.close();
    
    return selectlist.toString();
  }
  
}