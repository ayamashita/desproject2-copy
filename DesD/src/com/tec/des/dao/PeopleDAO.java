package com.tec.des.dao;
import java.sql.*;
import java.util.*;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.util.Nuller;
import com.tec.shared.exceptions.*;
import com.tec.des.dto.*;
import com.tec.server.db.ResultSetHelper;
import com.tec.des.util.*;

/**
 * Class encapsulating People data access.
 *
 * @author :  Per Kristian Foss
 */
public class PeopleDAO extends DAO {

 /**
  * Constructs a PeopleDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
    public PeopleDAO() throws SystemException, SQLException {
    super();
  }

 /**
  * Returns a Vector containing all People. 
  * @return Vector
  * @throws SQLException
  */

  public Vector getAllPeople() throws SQLException {

    Vector people = new Vector();
       
    StringBuffer query = new StringBuffer();    
    query.append("select people_id, people_family_name, people_first_name from " + getSimulaDb() + ".people order by people_family_name;");
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    StudyResponsibleDTO person;
    while (rs.next()) {
        person = new StudyResponsibleDTO();
        person.setId(String.valueOf(rsh.getInt("people_id")));
        person.setListName((rsh.getString("people_family_name") + ", " + rsh.getString("people_first_name")));
        people.add(person);
    }
    
    rs.close();
    stmt.close();
    
    return people;
  }
  
 /**
  * Returns a Vector containing selected Study Responsibles. 
  * @param String[] containg the ids of the study responsibles to fetch.
  * @return Vector
  * @throws SQLException
  */

  public Vector getSelectedPeople(String[] selectedPeople) throws SQLException {

    Vector people = new Vector();
    StringBuffer selectlist = new StringBuffer();
    
    for (int i = 0; i < selectedPeople.length; i++) {
        selectlist.append(selectedPeople[i]);
        selectlist.append(",");
    }
    selectlist.deleteCharAt(selectlist.length()-1);
       
    StringBuffer query = new StringBuffer();    
    query.append("select people_id, people_family_name, people_first_name from " + getSimulaDb() + ".people ");
    query.append("where people_id in (" + selectlist + ") order by people_family_name;");   
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    StudyResponsibleDTO person;
    while (rs.next()) {
        person = new StudyResponsibleDTO();
        person.setId(String.valueOf(rsh.getInt("people_id")));
        person.setListName((rsh.getString("people_family_name") + ", " + rsh.getString("people_first_name")));
        person.setSingleName((rsh.getString("people_first_name") + " " + rsh.getString("people_family_name")));
        people.add(person);
    }
    
    rs.close();
    stmt.close();
    
    return people;
  }
  
 /**
  * Fetches all people matching the given searchText. 
  * @param onlyRole If true only people with role matching the search criteria is fetched.
  * @param searchText The search criteria
  * @return Vector containing UserDTOs.
  * @throws SQLException
  */

  public Vector getPeopleRole(boolean onlyRole, String searchText) throws SQLException {

    Vector peopleRole = new Vector();
    StringBuffer query = new StringBuffer();    

    query.append("select p.people_id, p.people_family_name, p.people_first_name, r.role_id, r.role_name from " + getSimulaDb() + ".people p");
    if (onlyRole) 
        query.append(", people_role pr, role r where p.people_id = pr.people_id and pr.role_id = r.role_id");
    else {
        query.append(" left join people_role pr on p.people_id = pr.people_id ");
        query.append(" left join role r on pr.role_id = r.role_id");
    }
    
    if (searchText != null) {
        String[] searchCriteria = Validator.checkEscapes(searchText).split(" ");
        if (onlyRole)
            query.append(" and");
        else
            query.append(" where");
        for (int i = 0; i < searchCriteria.length; i++) {
            query.append(" (p.people_family_name like '%" + searchCriteria[i] + "%' or");
            query.append(" p.people_first_name like '%" + searchCriteria[i] + "%') and");
        }
        query.deleteCharAt(query.length()-1);
        query.deleteCharAt(query.length()-1);
        query.deleteCharAt(query.length()-1);
    }
    
    query.append(" order by p.people_family_name;");
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        UserDTO user = new UserDTO();
        user.setId(rsh.getInt("people_id"));           
        user.setListName(rsh.getString("people_family_name") + ", " + rsh.getString("people_first_name"));   
        user.setRoleId(rsh.getInt("role_id"));           
        user.setRoleName(rsh.getString("role_name"));        
        peopleRole.add(user);   
    }
    
    rs.close();
    stmt.close();
    
    return peopleRole;
  }
  
  /**
  * Returns a hashtable containing all Roles in the database 
  * @param Hashtable
  * @throws SQLException
  */

  public Hashtable getRoles() throws SQLException {

    Hashtable roles = new Hashtable();
    StringBuffer query = new StringBuffer();    
    
    query.append("select role_id, role_name from role;");
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        roles.put(String.valueOf(rsh.getInt("role_id")), rsh.getString("role_name"));
    }   
    
    rs.close();
    stmt.close();
    
    return roles;
  }
  
 /**
  * Updates the people_role tablewith the given hashtable.
  * @throws SQLException
  */

  public void setPeopleRole(Hashtable peopleRole) throws SQLException {

    StringBuffer query_find = new StringBuffer();
    StringBuffer query_update = new StringBuffer();
    String[] peopleId = (String[])peopleRole.keySet().toArray(new String[0]);
    String[] peopleRoleId = (String[])peopleRole.values().toArray(new String[0]);
    Vector peopleWithRole = new Vector();;
    
    // Fetch all people having a role
    query_find.append("select people_id from people_role;");    
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query_find.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query_find.toString());    
    ResultSetHelper rsh = new ResultSetHelper(rs);
    while (rs.next()) {
        peopleWithRole.add(String.valueOf(rsh.getInt("people_id")));
    }   
    
    // Checks previous role (if any) with new role and updates people_role table accordingly
    for (int i = 0; i < peopleId.length; i++) {       
        if (peopleWithRole.contains(peopleId[i])) {
            if (peopleRoleId[i].equals("0"))
                stmt.executeUpdate("delete from people_role where people_id = " + peopleId[i]);
            else {
                stmt.executeUpdate("update people_role set role_id = " + peopleRoleId[i] + " where people_id = " + peopleId[i] + ";");
            }
        } else {            
            if (!(peopleRoleId[i].equals("0")))
                stmt.executeUpdate("insert into people_role values (" +  peopleId[i] + "," + peopleRoleId[i] + ");");
        }
    }
    
    rs.close();
    stmt.close();
  }
  
 /**
  * Returns the user connected to the specified username and password
  * @param username (e-mail) of the user to fetch
  * @param password of the user to fetch
  * @return UserDTO
  * @throws SQLException
  */
  public UserDTO getUser(String username, String password) throws SQLException, UserException {

    String encryptedPassword = Password.crypt(password, "7$");  
    StringBuffer query = new StringBuffer();    
    query.append("select p.people_id, p.people_family_name, p.people_first_name, r.role_id, r.role_name from " + getSimulaDb() + ".people p ");
    query.append("left join people_role pr on p.people_id = pr.people_id ");
    query.append("left join role r on pr.role_id = r.role_id ");
    query.append("where p.people_email = '" + Validator.checkEscapes(username) + "' and p.people_password = '" + encryptedPassword + "';");
    
    LoggerFactory.getLogger(PeopleDAO.class).debug(query.toString()); 
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery(query.toString());
    ResultSetHelper rsh = new ResultSetHelper(rs);
    UserDTO user = new UserDTO();
    if (rs.next()) {
        user.setId(rsh.getInt("people_id"));           
        user.setSingleName(rsh.getString("people_first_name") + " " + rsh.getString("people_family_name")); 
        user.setRoleName(rsh.getString("role_name"));           
        user.setRoleId(rsh.getInt("role_id"));           
    } else
        throw new UserException(ExceptionMessages.INVALID_USER_NAME_OR_PASSWORD);
    
    rs.close();
    stmt.close();

    return user;
  }
      
}