package no.simula;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

public class RpcWs {

	public RpcWs() {
		System.out.println("RcpWs constructed.");
	}

	public Object[] getPeople(HashMap<String, String> params) {
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
		
		String sortBy = params.get("sort_on");
		params.remove("sort_on");
		params.remove("password"); // omit password check
		
		ArrayList<String> whereClause = new ArrayList<String>();
		for (Iterator<String> iterator = params.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();

			String clause;
			if (key.equals("id")) {
				clause = key + " = '" + params.get(key) + "'";
			}
			else {
				clause = key + " like '%" + params.get(key) + "%'";
			}
			whereClause.add(clause);
		}
		
		
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/xmlrpcserver";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			
			String sql = "SELECT * FROM people";
			
			String where = StringUtils.join(whereClause, " and ");
			if (where != null && where.length() > 0) {
				sql = sql + " where " + where;
			}
			
			
			if (sortBy != null && sortBy.length() > 0) {
				sql += " order by " + sortBy;
			}
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				HashMap<String, String> person = new HashMap<String, String>();
				person.put("id", rs.getString("id"));
				person.put("url", rs.getString("url"));
				person.put("Title", rs.getString("Title"));
				person.put("lastname", rs.getString("lastname"));
				person.put("firstname", rs.getString("firstname"));
				person.put("jobtitle", rs.getString("jobtitle"));
				person.put("email", rs.getString("email"));
				result.add(person);
			}
			
			rs.close();
			ps.close();
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) { /* ignore close errors */
				}
			}
		}


		return result.toArray();

	}

	public Object[] getPublications(HashMap<String, String> params) {
		ArrayList result = new ArrayList();
		
		String sortBy = params.get("sort_on");
		params.remove("sort_on");
		
		ArrayList<String> tmpAuthors = new ArrayList<String>();
		String tmpAuthor = params.remove("authors");
		while (tmpAuthor != null) {
			tmpAuthors.add("pax.lastname like '%" + tmpAuthor + "%' or pax.firstname like '%" + tmpAuthor + "%'");
			tmpAuthor = params.remove("authors");
		}
		
		ArrayList<String> whereClause = new ArrayList<String>();
		for (Iterator<String> iterator = params.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();

			String clause = "p." + key + " like '%" + params.get(key) + "%'";
			whereClause.add(clause);
		}
		
		
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/xmlrpcserver";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			
			String sql = "SELECT * FROM publications p left outer join publications_authors pa on p.id = pa.publication_id";
			
			String whereAuthors = StringUtils.join(tmpAuthors, " or ");
			if (whereAuthors != null && whereAuthors.length() > 0) {
				whereClause.add("(p.id in (select publication_id from publications_authors pax where " + whereAuthors + "))");
			}
			
			String where = StringUtils.join(whereClause, " and ");
			if (where != null && where.length() > 0) {
				sql = sql + " where " + where;
			}
			
			
			if (sortBy != null && sortBy.length() > 0) {
				sql += " order by " + sortBy;
			}
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			String oldId = "";
			HashMap curPublication = null;
			while (rs.next()) {
				String currId = rs.getString("id");
				if (!oldId.equals(currId)) {
					oldId = currId;
					HashMap publication = new HashMap();
					publication.put("id", getNonNull(rs.getString("id")));
					publication.put("url", getNonNull(rs.getString("url")));
					publication.put("title", getNonNull(rs.getString("title")));
					publication.put("type", getNonNull(rs.getString("type")));
					publication.put("year", getNonNull(rs.getString("year")));
					publication.put("creator", getNonNull(rs.getString("creator")));
					publication.put("source", getNonNull(rs.getString("source")));
					publication.put("hasPdf", getNonNull(rs.getString("hasPdf")));
					publication.put("abstract", getNonNull(rs.getString("abstract")));
					result.add(publication);
					curPublication = publication;
				}
				
				HashMap author = new HashMap();
				author.put("lastname", getNonNull(rs.getString("lastname")));
				author.put("firstname", getNonNull(rs.getString("firstname")));
				author.put("uid", getNonNull(rs.getString("uid")));
				author.put("homepage", getNonNull(rs.getString("homepage")));
				author.put("username", getNonNull(rs.getString("username")));
				
				Object[] authors = (Object[]) curPublication.get("authors");
				if (authors == null) {
					authors = new Object[0];
				}
				authors = ArrayUtils.add(authors, author);
				curPublication.put("authors", authors);
			}
			
			rs.close();
			ps.close();
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) { /* ignore close errors */
				}
			}
		}


		Object[] resultArr = result.toArray();
		return resultArr;

	}
	
	private String getNonNull(String value) {
		return value != null ? value : "null";
	}

}
