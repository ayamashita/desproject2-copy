<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>


<html>
<body>
  <head>
    <title>Grant Privileges</title>
    <link rel="stylesheet" href="http://www.simula.no/simula.css" media="screen">
    <meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Expires" content="0"> 
  </head>

  <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#F5F5F5" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c" onLoad="window.focus()">
    <table bgcolor="#F5F5F5" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr bgcolor="#ef1607">
			<td><img src="" width="0" height="70">	    	
    	<td colspan="5" class="h1"><img src="" width="10" height="0"><font color="#ADAEAD">Grant admin privileges</font></td>
    </tr>
    <tr>
		    	<td></td>
					<td colspan="5">&nbsp;</td>
					</tr>
			<tr>
    <tr>
    	<td></td>
			<td colspan="5" class="h2"><b>Grant Admin Priviliges</b></td>
			</tr>
			<tr>
				<td></td>
				<td colspan="5">
				<table width="100%">
					<tr>
						<td class="bodytext">Tick radiobuttons below, then click "Submit".
						<logic:messagesPresent message="true">
						                	<span style="color:green;">
						                  <br><b><bean:message key="no.simula.des.grantprivilges.updated"/></b>
						                  </span>
                </logic:messagesPresent>
						</td>
						<td align="right" class="bodytext-bold"><a href="JavaScript:document.grantPrivilegesForm.submit();">Submit</a>&nbsp;&nbsp;&nbsp;
						<a href="JavaScript:window.self.close();">Close</a><img src="" width="10" height="0">
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="5">
				<hr style="height: 1px; color: black; width: 100%;">
				</td>
			</tr>
    <tr class="bodytext-bold">
      <td><img src="" width="10" height="0"></td><td>DBA<img src="" width="2" height="0"></td><td>SA<img src="" width="2" height="0"></td><td>None<img src="" width="2" height="0"></td><td>Name</td><td>Position</td>
    </tr>
    <tr>
								<td></td>
								<td colspan="5">
								<hr style="height: 1px; color: black; width: 100%;">
								</td>
			</tr>
			<html:form action="/grantPrivileges">
			<html:hidden property="update" value="update" />
		<%boolean color= true;%>	
    <logic:iterate id="people" name="grantPrivilegesForm" property="peoples.peoples">
    <tr class="bodytext"  <%=(color==true?"bgcolor=\"white\"":"") %>>
    	<td></td>
      <td><input type="radio" name="<bean:write name="people" property="id" filter="true"/>" value="2" <logic:equal name="people" property="privilege" value="2">checked</logic:equal> ></td>
      <td><input type="radio" name="<bean:write name="people" property="id"filter="true"/>" value="1" <logic:equal name="people" property="privilege" value="1">checked</logic:equal> ></td>
      <td><input type="radio" name="<bean:write name="people" property="id" filter="true"/>" value="0" <logic:equal name="people" property="privilege" value="0">checked</logic:equal> ></td>
      <td><bean:write name="people" property="familyName" filter="true"/>,
        <bean:write name="people" property="firstName" filter="true"/></td>
      <td><bean:write name="people" property="position" filter="true"/></td>
    </tr>
    <%color=(color==true?false:true); %>
    </logic:iterate>
    <tr>
										<td></td>
										<td colspan="5">
										<hr style="height: 1px; color: black; width: 100%;">
										</td>
			</tr>
    <tr>
						<td></td>
						<td colspan="5">
						<table width="100%">
							<tr>
								<td class="bodytext"></td>
								<td align="right" class="bodytext-bold"><a href="JavaScript:document.grantPrivilegesForm.submit();">Submit</a>&nbsp;&nbsp;&nbsp;
								<a href="JavaScript:window.self.close()">Close</a><img src="" width="10" height="0">
								</td>
							</tr>
						</table>
						</td>
			</tr>
    <!--<html:hidden property="submit" value="submit" /> -->
    <!--<html:submit property="submit" value="Submit" /> -->
    </html:form>
    
    </table>
    
   

  <script>
    function submitForm(){
      document.forms[0].submit()
    }
  </script>
  </body>
</html>
