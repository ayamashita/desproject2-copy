<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html>
<body>
  <head>
    <title>Add study material</title>
    <link rel="stylesheet" href="http://www.simula.no/simula.css" media="screen">
    <meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Expires" content="0"> 
  </head>

  <body bgcolor="#F5F5F5" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c" onLoad="window.focus()">
    <html:form action="studyMaterial" method="POST" enctype="multipart/form-data">
    <table bgcolor="#F5F5F5" width="100%" cellspacing="0" cellpadding="0" border="0">
     <tr bgcolor="#ef1607">
     	<td><img src="" width="10" height="0" ><img src="" height="70" width="0" /></td>
     	<td colspan="3" class="h1"><font color="#ADAEAD">Add study material</font></td>
     </tr>
     <tr>
		 	<td></td>
		 	<td colspan="3"><h2><b>Add study material</b><h2></td>
		 </tr>
			<tr>
				<td></td>
				<td colspan="3" class="bodytext">Enter new information, then click "Submit"</td>
				</tr>
				<tr>
					<td><img src="" width="0" height="20" border="0"></td>
					<td colspan="3" align="right" class="bodytext-bold"><a href="JavaScript:document.studyMaterialForm.submit();">Submit</a>&nbsp;&nbsp;<a href="Javascript:window.opener.location.href=window.opener.location;window.self.close()">Close</a>&nbsp;</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="3" bgcolor="black"><img src="" width="0" height="1"></td>
				</tr>
     <tr>
     <td></td>
     <td colspan="3" class="bodytext-bold">
    		<span style="color:red;">
    		<html:errors/>
				</span>
                <logic:messagesPresent message="true">
                	<span style="color:green;">
                  <bean:message key="no.simula.des.studtmaterial.addMaterial"/>
                  </span>
                </logic:messagesPresent>
     </td>
    
    </tr>
    	<tr>
    		<td></td>
    		<td class="bodytext-bold">Description</td>
    		<td class="bodytext"><html:text property="description"/> </td>
    		<td></td>
    </tr>
    <tr>
    	<td></td>
    	<td colspan="3">
    		&nbsp;
    	</td>
    </tr>
    <tr>
    	<td></td>
    	<td class="bodytext-bold"><html:radio property="isUrl" value="false" />&nbsp; Upload file</td>
    	<td colspan="2" class="bodytext">
        <html:file property="file" />
      </td>
     </tr>
     <tr>
     			<td></td>
		     	<td colspan="3">
		     		&nbsp;
		     	</td>
    </tr>
     <tr>
     	<td></td>
     	<td class="bodytext-bold"><html:radio property="isUrl" value="true" />&nbsp; File reference (url)</td>
     	<td colspan="2" class="bodytext"><html:text property="url"/></td>
    </tr>
    <tr>
					<td><img src="" width="0" height="30"></td>
					<td colspan="3"></td>
					</tr>
   <!-- <tr>
			<td></td>
			<td colspan="3" bgcolor="black"><img src="" width="0" height="1"></td>
			</tr>
    <tr>
			<td><img src="" width="0" height="20" border="0"></td>
			<td colspan="3" align="right" class="bodytext-bold"><a href="JavaScript:document.studyMaterialForm.submit();">Submit</a>&nbsp;&nbsp;<a href="Javascript:window.opener.location.href=window.opener.location;window.self.close();">Close</a>&nbsp;</td>
			</tr> -->
			<tr>
						<td></td>
						<td>&nbsp;</td>
			</tr>
			<tr>
									<td></td>
									<td>&nbsp;</td>
			</tr>
			<tr>
									<td></td>
									<td>&nbsp;</td>
			</tr>
	 </table>
        <html:hidden property="upload" value="upload"/>
    </html:form>
  </body>
</html>
