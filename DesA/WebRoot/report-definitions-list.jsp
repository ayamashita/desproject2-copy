<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<%@ include file="head.jsp"%>

<div id="navActions"><logic:present name="user"><a href="frontPage.do">Front page</a> > </logic:present><a href="listReportDefinitions.do">Report definitions</a></div>
<html:form action="/listReportDefinitions">
  <logic:present name="report-definitions-list">
    <logic:notEmpty name="report-definitions-list">
      <bean:define id="tableHtml" name="report-definitions-list"/>
      <%= tableHtml %>
    </logic:notEmpty>
    <logic:empty name="report-definitions-list">
      No matching studies found.
    </logic:empty>
  </logic:present>
</html:form>

<html:link page="/editReportDefinition.do?new">Create Report definition</html:link>

<script type="text/javascript">
function onInvokeAction(id) {
	setExportToLimit(id, '');
	createHiddenInputFieldsForLimitAndSubmit(id);
}
</script>

<%@ include file="footer.jsp"%>