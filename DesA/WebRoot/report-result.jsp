<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<%@ include file="head.jsp"%>

<div id="navActions"><logic:present name="user"><a href="frontPage.do">Front page</a> > </logic:present><a href="listReportDefinitions.do">Report definitions</a> > Report result</div>

<html:form action="/runReport">
  <html:hidden property="id"/>
  <logic:present name="report-result">
    <logic:notEmpty name="report-result">
      <bean:define id="tableHtml" name="report-result"/>
      <%= tableHtml %>
    </logic:notEmpty>
    <logic:empty name="report-result">
      No matching studies found.
    </logic:empty>
  </logic:present>

  <script type="text/javascript">
    function onInvokeAction(id) {
	  setExportToLimit(id, '');
	  createHiddenInputFieldsForLimitAndSubmit(id);
    }
    function onInvokeExportAction(id, dummy) {
      var parameterString = createParameterStringForLimit(id);
      location.href = '<%=request.getContextPath()%>/runReport.do?id=<bean:write name="reportFormId" property="id"/>&' + parameterString;
    }
  </script>
</html:form>

<%@ include file="footer.jsp"%>
