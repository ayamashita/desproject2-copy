<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<%@ include file="head.jsp"%>

  <h1>
    <logic:present name="editReportDefinitionForm" property="id">Edit report definition</logic:present>
    <logic:notPresent name="editReportDefinitionForm" property="id">New report definition</logic:notPresent>
  </h1>

  <html:form action="/editReportDefinition" method="post">

  <span style="color:red;">
	<b><html:errors/></b>
  </span>

  <%-- The main table that divides the page into two columns --%>
        <table class="container">
          <tr>
            <td>title *</td>
            <td><html:text property="title" style="width: 100%"/></td>
          </tr>
          <tr>
            <td>sort by end date</td>
            <td>
              <html:checkbox property="sort" />
            </td>
          </tr>
          <tr>
            <td>columns *</td>
            <td>
              <table class="container">
                <tr>
                  <th>available columns</th>
                  <th/>
                  <th>selected columns</th>
                </tr>
                <tr>
                  <td>
                    <logic:empty name="editReportDefinitionForm" property="availableColumns">
                      <html:select property="addColumn" multiple="true" size="5" disabled="true">
                        <html:option value="-1">Empty</html:option>
                      </html:select>
                    </logic:empty>
                    <logic:notEmpty name="editReportDefinitionForm" property="availableColumns">
                      <html:select property="addColumn" multiple="true" size="5">
                        <html:optionsCollection property="availableColumns" value="column" label="column"/>
                      </html:select>
                    </logic:notEmpty>
                  </td>
                  <td style="text-align: center">
                    <html:submit property="addColumns" value=">"/><br/>
                    <html:submit property="removeColumns" value="<"/>
                  </td>
                  <td>
                    <logic:empty name="editReportDefinitionForm" property="columns">
                      <html:select property="removeColumn" multiple="true" size="5" disabled="true">
                        <html:option value="-1">Empty</html:option>
                      </html:select>
                    </logic:empty>
                    <logic:notEmpty name="editReportDefinitionForm" property="columns">
                      <html:select property="removeColumn" multiple="true" size="5">
                        <html:optionsCollection property="columns" value="column" label="column"/>
                      </html:select>
                    </logic:notEmpty>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>condition operator</td>
            <td>
              <html:radio property="operator" value="AND" />AND
              <html:radio property="operator" value="OR" />OR
            </td>
          </tr>
          <tr>
            <td>responsibles</td>
            <td><a href="Javascript:document.editReportDefinitionForm.submit();openPopupGeneral('addReportResponsibles.do?','addReportResponsibles','430','800','yes' );">(add responsibles)</a></td>
          </tr>
                	<%boolean color= true;%>	
                  <logic:iterate id="responsible"  name="editReportDefinitionForm" property="responsibles">
                    <logic:equal name="responsible" property="deleted" value="false">
                      <tr <%=(color==true?"bgcolor=\"white\"":"") %>>
                        <td align="left">
                          <a href='<bean:write name="responsible" property="url" filter="true"/>'><bean:write name="responsible" property="firstName" filter="true"/> <bean:write name="responsible" property="familyName" filter="true"/></a>
                        </td>
                        <td align="left">
                          <bean:write name="responsible" property="position" filter="true"/>
                        </td>
                        <td align="right">
                          <a href='removeReportResponsible.do?responsible=<bean:write name="responsible" property="id" filter="true"/>'>Delete</a>
                        </td>
                      </tr>
                    </logic:equal>
                    <%color=(color==true?false:true); %>
                    </logic:iterate>
        </table>
  <%-- End of Main Table --%>

  <html:submit property="save" value="save changes"/>
  <html:cancel value="cancel"/>

 </html:form>

<%@ include file="footer.jsp"%>
