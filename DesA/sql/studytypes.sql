-- MySQL dump 8.22
--
-- Host: localhost    Database: des
---------------------------------------------------------
-- Server version	3.23.57-nt

--
-- Table structure for table 'studytypes'
--

CREATE TABLE studytypes (
  type_id int(3) NOT NULL auto_increment,
  type_name varchar(100) NOT NULL default '',
  PRIMARY KEY  (type_id),
  UNIQUE KEY type_id (type_id)
) TYPE=ISAM PACK_KEYS=1;

--
-- Dumping data for table 'studytypes'
--


INSERT INTO studytypes VALUES (1,'Experiment');
INSERT INTO studytypes VALUES (2,'Case study');
INSERT INTO studytypes VALUES (3,'Multi-case');
INSERT INTO studytypes VALUES (4,'Interviews');
INSERT INTO studytypes VALUES (5,'Questionnaire');

