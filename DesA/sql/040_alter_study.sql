ALTER TABLE des_a.study
 CHANGE study_created_by study_created_by VARCHAR(50),
 CHANGE study_edited_by study_edited_by VARCHAR(50);
