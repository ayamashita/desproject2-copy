package no.simula.des.data;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PublicationBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

public class PublicationDatabase extends AbstractWebServiceDatabase {

    private static Log log = LogFactory.getLog(PublicationDatabase.class);

    protected static String webServiceUrl = Constants.WEB_SERVICE_URL;
    private static String webServiceMethod = Constants.WEB_SERVICE_PUBLICATION_METHOD;

    public List getPublicationList()  throws Exception {
        HashMap hash = new HashMap();
        Object[] methodParams = new Object[]{ hash };

        XmlRpcClient client = setupXmlRpcClient(webServiceUrl);

        List result = new ArrayList();
        try {
            log.debug("start");
            Object test = client.execute(webServiceMethod, methodParams);
            log.debug("end");
            Object[] list = (Object[])test;
            for (int i = 0; i < list.length; i++) {
                HashMap element = (HashMap)list[i];
                
                PublicationBean publication = fillPublication(element);
                
                result.add(publication);
            }
        } catch (XmlRpcException e) {
            log.error("executeSelect() - XmlRpcException while retrieving publications.", e);
            throw e;
        }
        
        return result;
    }

    public List executeSelect(List entityIds) throws Exception {
        XmlRpcClient client = setupXmlRpcClient(webServiceUrl);

        List result = new ArrayList();
        try {
            for (Iterator iterator = entityIds.iterator(); iterator.hasNext();) {
                String id = (String) iterator.next();
                
                HashMap hash = new HashMap();
                hash.put("id", id);
                Object[] methodParams = new Object[]{ hash };
    
                log.debug("start");
                Object test = client.execute(webServiceMethod, methodParams);
                log.debug("end");
                Object[] list = (Object[])test;
                for (int i = 0; i < list.length; i++) {
                    HashMap element = (HashMap)list[i];
                    PublicationBean publication = fillPublication(element);
                    
                    result.add(publication);
                }
            }
        } catch (XmlRpcException e) {
            log.error("executeSelect() - XmlRpcException while retrieving publications.", e);
            throw e;
        }
        
        return result;
    }

    public List getPublicationList(int studyId) throws Exception {
        List publications = new ArrayList();


        PublicationsStudiesDatabase pubStudiesDb = new PublicationsStudiesDatabase();
        List publicationsIds = pubStudiesDb.getPublicationList(studyId);

        publications = executeSelect(publicationsIds);

        return publications;
    }

    private PublicationBean fillPublication(HashMap element) throws Exception {
        
        PublicationBean publication = new PublicationBean();
        
        publication.setId((String) element.get("id"));
        publication.setUrl((String)element.get("url"));
        try {
            log.debug("URLDecoding title of study id " + element.get("id") + ": " + element.get("title"));
            publication.setTitle(URLDecoder.decode((String) element.get("title"), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            // Until we use UTF-8, this exception will never arise
        }

        String pubAbstract = (String) element.get("abstract");
        if (pubAbstract == null || "null".equals(pubAbstract)) {
            pubAbstract = "No publication abstract specified";
        }
        publication.setAbstact(pubAbstract);
        
        String creatorName = null;
        String creatorId = (String) element.get("creator");
        PeopleDatabase peopleDatabase = new PeopleDatabase();
        PeopleBean creator = peopleDatabase.executeSelect(creatorId);
        if (creator != null) {
            creatorName = creator.getFirstName() + " " + creator.getFamilyName();
        } else {
            creatorName = creatorId;
        }
        publication.setEditor(creatorName);
        
        String year = (String)element.get("year");
        int intYear = -1;
        try {
            intYear = Integer.parseInt(year);
        } catch (NumberFormatException e) {
        }
        if (intYear > -1) {
            publication.setYear(intYear);
        }
        
        return publication;
    }

    public List getPublicationsByTitle(String title) throws Exception {
        List result = new ArrayList();

        if (title == null || title.length() == 0) {
            return result;
        }
        
        XmlRpcClient client = setupXmlRpcClient(webServiceUrl);

        try {
            HashMap hash = new HashMap();
            hash.put("title", title);
            Object[] methodParams = new Object[] { hash };

            log.debug("start");
            Object test = client.execute(webServiceMethod, methodParams);
            log.debug("end");
            Object[] list = (Object[]) test;
            for (int i = 0; i < list.length; i++) {
                HashMap element = (HashMap) list[i];
                PublicationBean publication = fillPublication(element);

                result.add(publication);
            }
        } catch (XmlRpcException e) {
            log.error("executeSelect() - XmlRpcException while retrieving publications.", e);
            throw e;
        }
        
        return result;
    }

}
