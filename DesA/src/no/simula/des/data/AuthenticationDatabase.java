package no.simula.des.data;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.util.Constants;

import org.apache.xmlrpc.client.XmlRpcClient;


/**
 * Handles retrival of all people related data. The "people table" itself is not modified,
 * only relations between it and other tables.
 *
 */
public class AuthenticationDatabase extends AbstractWebServiceDatabase {

    private static String webServiceUrl = Constants.WEB_SERVICE_AUTHENTICATION_URL;
    private static String webServiceAuthMethod = Constants.WEB_SERVICE_AUTHENTICATION_METHOD;

    /**
     * Autheticates a user.
     * Returns null if the user is not authenticated
     *
     * @param String userId
     * @param String password - (in clear text)
     * @return PeopleBean, null if user not authenticated
     * @throws Exception if SQL error
     */
    public PeopleBean authentiateUser(String userId, String password)
        throws Exception {
        if (userId == null || userId.equals("") || password == null || password.equals("")) {
            throw new Exception(
                "The username and/or password can not be empty ");
        }

        XmlRpcClient client = setupXmlRpcClient(webServiceUrl);

        java.util.HashMap hash = new java.util.HashMap();
        hash.put("login", userId);
        hash.put("password", password);
        Object[] methodParams = new Object[] { hash };

        Boolean test = (Boolean) client.execute(webServiceAuthMethod,
                methodParams);

        if (test.booleanValue() == false) {
            return null;
        }
        
        PeopleDatabase peopleDatabase = new PeopleDatabase();
        PeopleBean person = peopleDatabase.executeSelect(userId);
        
        if (person == null) {
            return null;
        }
        
        AdminPrivilegesDatabase adminPrivilegeDb = new AdminPrivilegesDatabase();
        int priv = adminPrivilegeDb.getPeoplePrivilege(userId);
        person.setPrivilege(priv);
        
        return person;
        
    }

}
