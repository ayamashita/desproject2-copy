package no.simula.des.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import no.simula.des.data.beans.StudyMaterialBean;
import no.simula.des.data.beans.StudyMaterialFileBean;


/**
 * Handles all database access related to study material
 *
 */
public class StudyMaterialDatabase extends DataObject {
    private static final String INSERT_STUDY_MATERIAL_QUERY =
      "INSERT INTO studymaterial (study_id, study_material_description, study_material_isUrl, study_material_file,  study_material_filename,  study_material_filetype, study_material_url) VALUES (?,?,?, ?, ?, ?,? )";
    private static final String RETRIEVE_STUDY_MATERIAL_INFO_QUERY =
      "select * from studymaterial where study_id = ?;";
    private static final String RETRIEVE_STUDY_MATERIAL_FILE_QUERY =
      "select study_material_filename, study_material_filetype, study_material_file from studymaterial where study_material_id = ? AND study_id =?;";
    private static final String DELETE_STUDY_MATERIAL_QUERY =
      "delete from studymaterial where study_material_id=?;";

    /**
     * Adds a study material element to the database
     *
     * @param studyMaterial the study material object to be stored
     * @return true if the method completed successfully
     * @throws Exception
     */
    public boolean insertStudyMateriel(StudyMaterialBean studyMaterial)
        throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = this.getConnection();
            PreparedStatement stmt = conn.prepareStatement(StudyMaterialDatabase.INSERT_STUDY_MATERIAL_QUERY);
            stmt.setInt(1, studyMaterial.getStudy_id());
            stmt.setString(2, studyMaterial.getDescription());

            if (studyMaterial.getIsUrl()) {
                stmt.setString(3, "1");
                stmt.setString(4, "NULL");
                stmt.setString(5, "NULL");
                stmt.setString(6, "NULL");
                stmt.setString(7, studyMaterial.getUrl());
            } else {
                stmt.setString(3, "0");
                stmt.setBytes(4, studyMaterial.getFile());
                stmt.setString(5, studyMaterial.getFilename());
                stmt.setString(6, studyMaterial.getContenttype());
                stmt.setString(7, "NULL");
            }

            int count = stmt.executeUpdate();

            if (count == 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            super.closeConnection(rs);
        }
    }

    /**
     * Retrieves all study material related to a given study
     *
     * @param studyId identifies the study
     * @return a Collection of StudyMaterialBeans
     * @throws Exception
     */
    public ArrayList getStudyMaterialInfo(int studyId)
        throws Exception {
        ResultSet rs = null;
        ArrayList materials = new ArrayList();

        try {
            Connection conn = this.getConnection();
            PreparedStatement stmt = conn.prepareStatement(StudyMaterialDatabase.RETRIEVE_STUDY_MATERIAL_INFO_QUERY);
            stmt.setInt(1, studyId);
            rs = stmt.executeQuery();

            while (rs.next()) {
                StudyMaterialBean bean = new StudyMaterialBean();
                bean.setStudy_material_id(rs.getInt(1));
                bean.setStudy_id(rs.getInt(2));
                bean.setDescription(rs.getString(3));

                boolean isUrl = rs.getBoolean(4);
                bean.setIsUrl(isUrl);

                if (isUrl) {
                    bean.setUrl(rs.getString(8));
                } else {
                    bean.setFilename(rs.getString(6));
                    bean.setContenttype(rs.getString(7));
                }

                materials.add(bean);
            }

            return materials;
        } catch (Exception ex) {
            throw ex;
        } finally {
            this.closeConnection(rs);
        }
    }

    /**
     * In the case where the study material contains a file, this method is used to retrieve the file
     *
     * @param studyMaterialId identifies the study material
     * @param studyId identifies the study that the material is related to
     * @return a StudyMaterialFileBean containing the file itself, along with relevant meta data
     * @throws Exception
     */
    public StudyMaterialFileBean retriveStudyMaterialFile(int studyMaterialId,
        int studyId) throws Exception {
        ResultSet rs = null;

        try {
            StudyMaterialFileBean bean = new StudyMaterialFileBean();
            Connection conn = this.getConnection();
            PreparedStatement stmt = conn.prepareStatement(StudyMaterialDatabase.RETRIEVE_STUDY_MATERIAL_FILE_QUERY);
            stmt.setInt(1, studyMaterialId);
            stmt.setInt(2, studyId);
            rs = stmt.executeQuery();
            rs.next();
            bean.setFilename(rs.getString(1));
            bean.setContentType(rs.getString(2));
            bean.setFile(rs.getBytes(3));

            return bean;
        } catch (Exception ex) {
            throw ex;
        } finally {
            this.closeConnection(rs);
        }
    }

    /**
     * Deletes a single study material entry
     *
     * @param studyMaterialId identifies the material to be deleted
     * @return true if the operation was successful
     * @throws Exception
     */
    public boolean deleteStudyMaterial(int studyMaterialId)
        throws Exception {
        ResultSet rs = null;

        try {
            StudyMaterialFileBean bean = new StudyMaterialFileBean();
            Connection conn = this.getConnection();
            PreparedStatement stmt = conn.prepareStatement(StudyMaterialDatabase.DELETE_STUDY_MATERIAL_QUERY);
            stmt.setInt(1, studyMaterialId);

            int count = stmt.executeUpdate();

            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            this.closeConnection(rs);
        }
    }
}
