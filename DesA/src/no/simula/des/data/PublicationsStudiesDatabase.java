/*
 * Created on 13.okt.2003
 *
*/
package no.simula.des.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import no.simula.des.data.beans.PublicationBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 * This class manages database access for publications related queries. As publications
 * are used in DES only as "links to related publications", the class never actually
 * deletes publications from the database - only relationships to studies are removed.
 */
public class PublicationsStudiesDatabase extends DataObject {
    private final static String DELETE_PUBLICATION =
        "DELETE FROM publicationsstudies " + "WHERE  (publication_id=?) AND " +
        "       (study_id=?);";

    private final static String ADD_PUBLICATION =
        "INSERT IGNORE INTO publicationsstudies " + "VALUES (?, ?);";

    private static final String GET_STUDY_PUBLICATIONS_QUERY = "select publication_id from publicationsstudies r where r.study_id = ?;";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * The method adds and removes relationships between publications and studies at the
     * database level.
     *
     * @param studyId identifies which study we are changing the relationships for.
     * @param publications all publications related to the study, including any added or deleted
     *                     ones.
     * @throws Exception
     */
    public void updatePublicationStudyRelationship(int studyId,
        Collection publications) throws Exception {
        if( publications == null) return;
        Iterator iterator = publications.iterator();

        //Discover any changes
        while (iterator.hasNext()) {
            PublicationBean bean = (PublicationBean) iterator.next();

            if (bean.getDeleted()) {
                //Delete publication from study
                deletePublicationFromStudy(studyId, bean.getId());
            } else if (bean.getAdded()) {
                //Add publication to study
                addPublicationToStudy(studyId, bean.getId());
            }
        }
    }

    /**
     * This method relates a publication to a study
     *
     * @param studyId
     * @param publicationId
     */
    private void addPublicationToStudy(int studyId, String publicationId)
        throws Exception {
        //TODO Can we assume that relation is not already existing?
        //If the addPublication action handles this, no additional check (SELECT) is needed here
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(ADD_PUBLICATION);
            pstmt.setString(1, publicationId);
            pstmt.setInt(2, studyId);

            int addCount = pstmt.executeUpdate();

            if (log.isDebugEnabled()) {
                log.debug(addCount + " publications added to study " + studyId);
            }
            /*
            if (addCount != 1) {
                throw new Exception("Publication (id=" + publicationId +
                    ") was not added from study id " + studyId);
            }
            */
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * This method removes the relationship between a study and a publication in the DES database
     *
     * @param studyId
     * @param publicationId
     */
    private void deletePublicationFromStudy(int studyId, String publicationId)
        throws Exception {
        //Delete all matching entries in publicationsstudies...
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(DELETE_PUBLICATION);
            pstmt.setString(1, publicationId);
            pstmt.setInt(2, studyId);

            int delCount = pstmt.executeUpdate();

            if (log.isDebugEnabled()) {
                log.debug(delCount + " publications deleted from study " +
                    studyId);
            }
            /*
            if (delCount != 1) {
                throw new Exception("Publication (id=" + publicationId +
                    ") was not deleted from study id " + studyId);
            }
            */
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }
    
    /**
     * Retrives all person ids listed as responsibles for a given study
     *
     * @param studyId ID of the study
     * @return a Collection of PeopleBean objects. An empty Collection if no responsibles exist
     */
    public List getPublicationList(int studyId) throws Exception {
        ArrayList publicationsIds = new ArrayList();
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement stmt = conn.prepareStatement(PublicationsStudiesDatabase.GET_STUDY_PUBLICATIONS_QUERY);
            stmt.setInt(1, studyId);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String personId = rs.getString(1);
                publicationsIds.add(personId);
            }

        } catch (Exception ex) {
            log.error("Error retrieving study publications from study: " +
                studyId, ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }

        return publicationsIds;
    }

}
