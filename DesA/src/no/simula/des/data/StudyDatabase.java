/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.data;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import no.simula.des.beans.NameValueBean;
import no.simula.des.beans.StudySortBean;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PublicationBean;
import no.simula.des.data.beans.StudiesBean;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.data.beans.StudyMaterialBean;
import no.simula.des.util.Constants;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



/**
 * The class handles all database access related to study lists
 */
public class StudyDatabase extends DataObject {

    //Prepared statements
    private final static String CHECK_STUDY_NAME_QUERY = "select count(*) from study where study_name=? and study_id != ?;";


    private final static String GET_ALL_STUDY_IDS_QUEREY = "select study_id from study  ORDER BY study_end_date;";
    private final static String SINGLE_STUDY_QUERY =
        "SELECT study.*, " +
        "       type_name, " +
        "       durationunits.duration_name " +
        "FROM   study, studytypes, durationunits " +
        "WHERE  (study_type = studytypes.type_id) AND " +
        "       study.study_duration_unit = durationunits.duration_id AND" +
        "       (study_id = ?);";
    private final static MessageFormat STUDY_QUERY = new MessageFormat(
            "SELECT study.study_id as study_id, " +
            "       study.study_name as study_name, " +
            "       study.study_description as study_description, " +
            "       study.study_end_date as study_end_date, " +
            "       studytypes.type_name as type_name " +
            "FROM   study, studytypes " +
            "WHERE  (study_type = studytypes.type_id) " + "ORDER BY {0} {1};");


    private static MessageFormat STUDY_SEARCH_QUERY = new MessageFormat(
            "select distinct  s.study_id, s.study_name, s.study_description, s.study_end_date, st.type_name " +
            "from study s " +
            "  LEFT JOIN publicationsstudies ps ON  " +
            "    s.study_id = ps.study_id  " +
            "  LEFT JOIN studytypes st ON  " +
            "    s.study_type = st.type_id  " +
            "  LEFT JOIN responsiblesstudies rs ON  " +
            "    rs.study_id = s.study_id " +
            "WHERE " +
            "  (s.study_name REGEXP ?  " +
            "   or s.study_keywords REGEXP ?  " +
            "   or s.study_notes REGEXP ?  " +
            "   or  s.study_description REGEXP ? " +
 //           "   /* When no publications were found, either don't generate following clause at all, or generate anything so that we don't have SQL syntax error (e.g. 'or ps.publication_id IN (-1)') */ " +
            "   {0} " + //"/* or ps.publication_id IN (<list of publications ids>) */ " +
            "  ) " +
            "  AND (s.study_end_date > ? AND s.study_end_date < ?) " +
            "  AND st.type_name regexp ? " +
//            "  /* When user does not enter responsible criteria, don't generate following clause at all */ " +
//            "  /* When no user is found, generate eg. 'AND rs.responsible_id IN (NULL)' */ " +
            "  {1} " + //"/* AND rs.responsible_id IN (<list of people ids>) */ " +
            "ORDER BY {2} {3} ");

    private final static String STUDY_TYPES_QUERY = "SELECT * FROM studytypes;";

    private final static String DURATION_UNITS_QUERY = "SELECT * FROM durationunits;";

    private final static String STUDY_INSERT = "INSERT INTO study (" +
        "       study_name, " + "       study_type, " +
        "       study_description, " + "       study_keywords, " +
        "       study_notes, " + "       study_students, " +
        "       study_professionals, " + "       study_duration, " +
        "       study_duration_unit, " + "       study_start_date, " +
        "       study_end_date, " + "       study_created_by, " +
        "       study_created_date, " + "       study_edited_by, " +
        "       study_edited_date) " +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    private final static String STUDY_UPDATE = "UPDATE study SET " +
        "       study_name=?, " + "       study_type=?, " +
        "       study_description=?, " + "       study_keywords=?, " +
        "       study_notes=?, " + "       study_students=?, " +
        "       study_professionals=?, " + "       study_duration=?, " +
        "       study_duration_unit=?, " + "       study_start_date=?, " +
        "       study_end_date=?, " + "       study_edited_by=?, " +
        "       study_edited_date=? " + "WHERE 	study_id=?;";

    public static final String DELETE_STUDY_QUERY = "DELETE FROM study WHERE study.study_id = ?;";

    public static final String DELETE_STUDY_MATERIAL_QUERY = "DELETE FROM studymaterial WHERE studymaterial.study_id = ?;";

    public static final String DELETE_PUBLICATION_RELATION_QUERY = "DELETE FROM publicationsstudies WHERE publicationsstudies.study_id=?;";

    public static final String DELETE_RESPONSIBLE_RELATION_QUERY = "DELETE FROM responsiblesstudies WHERE responsiblesstudies.study_id=?;";

    private final static MessageFormat STUDIES_BY_RESPONSIBLES_QUERY = new MessageFormat(
            "SELECT study.*, " +
            "       studytypes.type_name as type_name, " +
            "       durationunits.duration_name as duration_name " +
            "FROM   study, studytypes, durationunits " +
            "WHERE  (study_type = studytypes.type_id AND study.study_duration_unit = durationunits.duration_id) {0} " +
            "ORDER BY studytypes.type_name {1} {2};");

    private final static MessageFormat DYNAMIC_QUERY_RESPONSIBLE_OR = new MessageFormat(
            "AND study.study_id IN (SELECT rs.study_id FROM responsiblesstudies rs WHERE rs.responsible_id IN ({0}))");

    private final static MessageFormat DYNAMIC_QUERY_RESPONSIBLE_AND = new MessageFormat(
            "AND study.study_id IN (SELECT rs1.study_id FROM {0} WHERE {1} )");

    private static final String STUDY_END_DATES_QUERY ="select distinct(study_end_date) from study;";

    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    /**
     * Retrieves a single study from the database
     *
     * @param id the id of the study to retrieve
     * @return the StudyBean object representing the study
     * @throws Exception
     */
    public StudyBean getStudy(int id, Connection conn)
        throws Exception {
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        try {
            //Connection conn = getConnection();
            pstmt = conn.prepareStatement(SINGLE_STUDY_QUERY);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();
            rs.first();

            StudyBean study = populateStudyBeanComplete(rs);
            
            pstmt = conn.prepareStatement(STUDY_TYPES_QUERY);
            rs = pstmt.executeQuery();

            study.setStudyTypeOptions(getStudyTypes());

            return study;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                rs = null;
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                pstmt = null;
            }
        }
    }

    /**
     * Retrieves study data for a single study
     *
     * @param id identifies the study
     * @return a StudyBean containing the study data
     * @throws Exception
     */
    public StudyBean getStudy(int id) throws Exception {
        try {
            Connection conn = getConnection();

            return getStudy(id, conn);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * Retrieves a number of studies from the study table
     *
     * @param start row number to start on
     * @param count number of studies to retrieve
     * @return a Collection of StudyBean objects
     */
    public StudiesBean getStudies(StudySortBean sortBean)
        throws Exception {
        int start = sortBean.getStart();
        int count = sortBean.getCount();
        String orderBy = sortBean.getOrderBy();
        String orderDirection = sortBean.getOrderDirection();

        StudiesBean studies = null;
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_QUERY.format(
                        new Object[] { orderBy, orderDirection }));
            rs = pstmt.executeQuery();

            //Iterate through resultset and instantiate StudyBean objects
            //Start at first hit and return all hits
            studies = getStudyBeans(rs, start, count);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            super.closeConnection(rs);
        }

        return studies;
    }

    /**
     * Performs an advanced search
     *
     * @param StudySortBean containg the sort and search information
     * @return a StudiesBean object containing all the studies that matched the query
     */
    public StudiesBean advancedSearchStudies(StudySortBean sortBean)
        throws Exception {
        int start = sortBean.getStart();
        int count = sortBean.getCount();
        String searchString = sortBean.getSearchString();
        String studyType = sortBean.getStudyType();
        String startYear = sortBean.getStartYear();
        String endYear = sortBean.getEndYear();
        String firstName = sortBean.getResponsible_firstname();
        String familyName = sortBean.getResponsible_familyname();

        if (!endYear.equals("") && !startYear.equals("")) {
            int start_dt = Integer.parseInt(startYear);
            int end_dt = Integer.parseInt(endYear);
            if (start_dt > end_dt) {
                endYear = String.valueOf(start_dt);
                startYear = String.valueOf(end_dt);
            }
        }

        if (endYear.equals("")) {
            endYear = "3000";
        }

        if (startYear.equals("")) {
            startYear = "0";
        }


        Calendar calStartDate = Calendar.getInstance();
        calStartDate.set( Integer.parseInt( startYear ), 0, 1);

        Calendar calEndDate = Calendar.getInstance();
        calEndDate.set( Integer.parseInt( endYear ) +1 , 0, 1);



        log.debug("searcgString:" + searchString);
        log.debug("studyType:" + studyType);
        log.debug("startYear:" + startYear);
        log.debug("endYear:" + endYear);
        log.debug("responsible:" + sortBean.getResponsible_firstname() +
          " " + sortBean.getResponsible_familyname() );

        //String responsible = sortBean.getResponsible();

        if (studyType == null || studyType.equals("")) {
            studyType = ".*";
        }

        //STUDY_SEARCH_QUERY.getFormats();
        ResultSet rs = null;
        StudiesBean studies = null;
        String orderByColumn ="";
        if( sortBean.getOrderBy().equals("study_type") ){
          orderByColumn = "s.study_type";
        }
        else{
          orderByColumn = sortBean.getOrderBy();
        }

        List people = null;
        String responsiblesstudies = "";
        if ((firstName != null && firstName.length() > 0) || (familyName != null && familyName.length() > 0)) {
            PeopleDatabase peopleDb = new PeopleDatabase();
            people = peopleDb.getPeopleByName(firstName, familyName);
    
            if (people.size() > 0) {
                responsiblesstudies = StringUtils.repeat("?, " , people.size());
                if (responsiblesstudies != null && responsiblesstudies.length() > 0) {
                    if (responsiblesstudies.endsWith(", ")) {
                        responsiblesstudies = responsiblesstudies.substring(0, responsiblesstudies.length() - 2);
                    }
                    responsiblesstudies = "AND rs.responsible_id IN (" + responsiblesstudies + ") ";
                }
            } else {
                responsiblesstudies = "AND rs.responsible_id IN (NULL)";
            }
        }
        
        PublicationDatabase pubDb = new PublicationDatabase();
        List publications = pubDb.getPublicationsByTitle(searchString);
        
        String publicationsstudies = StringUtils.repeat("?, " , publications.size());
        if (publicationsstudies != null && publicationsstudies.length() > 0) {
            if (publicationsstudies.endsWith(", ")) {
                publicationsstudies = publicationsstudies.substring(0, publicationsstudies.length() - 2);
            }
            publicationsstudies = "OR ps.publication_id IN (" + publicationsstudies + ") ";
        }
        
        try {
            Connection conn = getConnection();
            String sql = STUDY_SEARCH_QUERY.format(new Object[] {
                    publicationsstudies,
                    responsiblesstudies,
                    orderByColumn,
                    sortBean.getOrderDirection()
                    });
            LoggableStatement pstmt = new LoggableStatement(conn, sql);

            int i = 1;
            pstmt.setString(i++, ".*" + searchString + ".*"); //s.study_name
            pstmt.setString(i++, ".*" + searchString + ".*"); //s.study_keywords
            pstmt.setString(i++, ".*" + searchString + ".*"); //s.study_notes
            pstmt.setString(i++, ".*" + searchString + ".*"); //s.study_description

            for (Iterator iterator = publications.iterator(); iterator.hasNext();) {
                PublicationBean publication = (PublicationBean) iterator.next();
                pstmt.setString(i++, publication.getId());
            }
            
            pstmt.setDate(i++, new java.sql.Date( calStartDate.getTime().getTime() ) ); //s.study_end_date
            pstmt.setDate(i++, new java.sql.Date( calEndDate.getTime().getTime() )  ); //s.study_end_date

            pstmt.setString(i++, studyType ); //st.type_name

            if ((firstName != null && firstName.length() > 0) || (familyName != null && familyName.length() > 0)) {
                for (Iterator iterator = people.iterator(); iterator.hasNext();) {
                    PeopleBean person = (PeopleBean) iterator.next();
                    pstmt.setString(i++, person.getId());
                }
            }

            rs = pstmt.executeQuery();

            //Iterate through resultset and instantiate StudyBean objects
            //Start at first hit and return all hits
            studies = getStudyBeans(rs, start, count);
        } catch (Exception e) {
            log.error(e);
            throw new Exception(e.toString());
        } finally {
            super.closeConnection(rs);
        }

        return studies;
    }

    /**
     * Updates an existing study
     *
     * @param bean contains all data related to the study
     * @throws Exception
     */
    public void updateStudy(StudyBean bean) throws Exception {

        try {
            //Perform any updates in publications and responsibles
            Collection pubs = bean.getPublications();

            if ( pubs != null && pubs.size() > 0) {
                PublicationsStudiesDatabase pubStudDb = new PublicationsStudiesDatabase();
                pubStudDb.updatePublicationStudyRelationship(bean.getId(), pubs);
            }

            Collection responsibles = bean.getResponsibles();

            if (responsibles != null && responsibles.size() > 0) {
                ResponsibleStudiesDatabase respStudyDb = new ResponsibleStudiesDatabase();
                respStudyDb.updateResponsibleStudyRelationship(bean.getId(),
                    responsibles);
            }

            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_UPDATE);

            pstmt.setString(1, bean.getName());

            //System.out.println( "Type = " + bean.getType());
            pstmt.setInt(2, Integer.parseInt(bean.getType()));
            pstmt.setString(3, bean.getDescription());
            pstmt.setString(4, bean.getKeywords());
            pstmt.setString(5, bean.getNotes());
            pstmt.setInt(6, bean.getStudents());
            pstmt.setInt(7, bean.getProfessionals());
            pstmt.setInt(8, bean.getDuration());
            pstmt.setInt(9, bean.getDurationUnit());
            if( bean.getStartDate() != null){
              pstmt.setDate(10, new Date(bean.getStartDate().getTime()));
            }
            else{
              pstmt.setNull(10, java.sql.Types.DATE );
            }
            pstmt.setDate(11, new Date(bean.getEndDate().getTime()));
            pstmt.setString(12, bean.getEditorId());
            pstmt.setDate(13, new Date(new java.util.Date().getTime()));
            pstmt.setInt(14, bean.getId());

            int updates = pstmt.executeUpdate();

            if (updates != 1) {
                throw new Exception("The study was not updated (" +
                    bean.getId() + ", " + bean.getName() + ")");
            }

            ResponsibleStudiesDatabase respStudyDb = new ResponsibleStudiesDatabase();
            respStudyDb.updateResponsibleStudyRelationship(bean.getId(),
                bean.getResponsibles());

            PublicationsStudiesDatabase pubDao = new PublicationsStudiesDatabase();
            pubDao.updatePublicationStudyRelationship(bean.getId(),
                bean.getPublications());

            ArrayList material = bean.getStudyMaterial();
            StudyMaterialDatabase materialDb = new StudyMaterialDatabase();
            Iterator iter = material.iterator();

            while (iter.hasNext()) {
                StudyMaterialBean item = (StudyMaterialBean) iter.next();

                if (item.getAdded()) {
                    item.setStudy_id(bean.getId() );
                    materialDb.insertStudyMateriel(item);
                }
                else if( item.getDeleted() && item.getStudy_material_id() != -1  ){
                  materialDb.deleteStudyMaterial( item.getStudy_material_id() );
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.toString());
        } finally {
            closeConnection(null);
        }
    }

    /**
     * Creates a new study entry
     *
     * @param bean contains all data related to the study
     * @return the ID of the inserted study
     */
    public synchronized int insertStudy(StudyBean bean)
        throws Exception {
        ResultSet rs = null;
        int id = 0;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_INSERT);
            pstmt.setString(1, bean.getName());

            //System.out.println( "Type = " + bean.getType());
            pstmt.setInt(2, Integer.parseInt(bean.getType()));
            pstmt.setString(3, bean.getDescription());
            pstmt.setString(4, bean.getKeywords());
            pstmt.setString(5, bean.getNotes());
            pstmt.setInt(6, bean.getStudents());
            pstmt.setInt(7, bean.getProfessionals());
            pstmt.setInt(8, bean.getDuration());
            pstmt.setInt(9, bean.getDurationUnit());
            if( bean.getStartDate() != null){
              pstmt.setDate(10, new Date(bean.getStartDate().getTime()));
            }
            else{
              pstmt.setNull(10, java.sql.Types.DATE );
            }
            pstmt.setDate(11, new Date(bean.getEndDate().getTime()));
            pstmt.setString(12, bean.getEditorId());
            pstmt.setDate(13, new Date(new java.util.Date().getTime()));
            pstmt.setString(14, bean.getEditorId());
            pstmt.setDate(15, new Date(new java.util.Date().getTime()));

            int updates = pstmt.executeUpdate();

            rs = pstmt.executeQuery("SELECT LAST_INSERT_ID();");

            while (rs.next()) {
                id = rs.getInt(1);
                log.debug("ID of the new study:" + id);
            }

            if (updates == 0) {
                throw new Exception("A new study was not added (" +
                    bean.getName() + ")");
            }

            ResponsibleStudiesDatabase respStudyDb = new ResponsibleStudiesDatabase();
            respStudyDb.updateResponsibleStudyRelationship(id,
                bean.getResponsibles());

            PublicationsStudiesDatabase pubDao = new PublicationsStudiesDatabase();
            pubDao.updatePublicationStudyRelationship(id, bean.getPublications());

            ArrayList material = bean.getStudyMaterial();
            StudyMaterialDatabase materialDb = new StudyMaterialDatabase();
            Iterator iter = material.iterator();

            while (iter.hasNext()) {
                StudyMaterialBean item = (StudyMaterialBean) iter.next();

                if (item.getAdded()) {
                    item.setStudy_id(id);
                    materialDb.insertStudyMateriel(item);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.toString());
        } finally {
            closeConnection(rs);
        }

        return id;
    }

    /**
     * The method iterates through a result set and adds instantiated StudyBeans to a collection.
     *
     * @param rs the SQL resultset
     * @param start which row to start on
     * @param count how many studies to retrieve
     * @return a collection of StudyBeans
     * @throws Exception
     */
    private StudiesBean getStudyBeans(ResultSet rs, int start, int count)
    throws Exception {
        return getStudyBeans(rs, start, count, false);
    }
    
    private StudiesBean getStudyBeans(ResultSet rs, int start, int count, boolean complete)
        throws Exception {
        StudiesBean studies = new StudiesBean();
        ArrayList beans = new ArrayList();

        rs.last();

        int total = rs.getRow();
        studies.setTotalStudies(total);

        //Start should always be above zero
        if (start > 1) {
            rs.absolute(start - 1);
            studies.setStartIndex(rs.getRow());
        } else {
            rs.beforeFirst();
            studies.setStartIndex(1);
        }

        int endRow = 0;

        for (int i = 0; rs.next() && (i < count); i++) {
            StudyBean study = null;
            if (complete) {
                study = populateStudyBeanComplete(rs);
            } else {
                study = populateStudyBeanBasic(rs);
            }
            beans.add(study);
            endRow = rs.getRow();
        }

        studies.setEndIndex(endRow);
        studies.setStudies(beans);

        return studies;
    }

    /**
     * The method instantiates a single StudyBean object from a row in a SQL result set.
     * Only attributes needed for the study list are included
     *
     * @param rs the result set from an SQL query
     * @return a single StudyBean object
     * @throws Exception 
     */
    private StudyBean populateStudyBeanBasic(ResultSet rs)
        throws Exception {
        StudyBean study = new StudyBean();
        study.setId(rs.getInt(1));
        study.setName(rs.getString(2));
        study.setDescription(rs.getString(3));
        study.setEndDate(rs.getDate(4));
        study.setType(rs.getString(5));

        fetchPeopleAndPublications(study);
        
        return study;
    }

    private void fetchPeopleAndPublications(StudyBean study) throws Exception {
        ResponsibleStudiesDatabase responsibleStudiesDatabase = new ResponsibleStudiesDatabase();
        List responsibleIds = responsibleStudiesDatabase.getResponsibleList(study.getId());
        
        PeopleDatabase peopleDatabase = new PeopleDatabase();
        List responsibles = peopleDatabase.executeSelect(responsibleIds);
        
        study.setResponsibles(responsibles);
        
        
        PublicationsStudiesDatabase publicationsStudiesDatabase = new PublicationsStudiesDatabase();
        List publicationsIds = publicationsStudiesDatabase.getPublicationList(study.getId());
        
        PublicationDatabase publicationDatabase = new PublicationDatabase();
        List studies = publicationDatabase.executeSelect(publicationsIds);
        
        study.setPublications(studies);
    }

    /**
     * The method instantiates a single StudyBean object from a row in a SQL result set
     *
     * @param rs the result set from an SQL query
     * @return a single StudyBean object
     * @throws SQLException
     */
    private StudyBean populateStudyBeanComplete(ResultSet rs)
        throws Exception {
        StudyBean study = new StudyBean();
        study.setId(rs.getInt("study_id"));

        //Mandatory attributes
        study.setName(rs.getString("study_name"));
        study.setDescription(rs.getString("study_description"));
        study.setEndDate(rs.getDate("study_end_date"));
        study.setType(rs.getString("type_name"));

        //Optional attributes
        study.setKeywords(rs.getString("study_keywords"));
        study.setNotes(rs.getString("study_notes"));
        study.setStudents(rs.getInt("study_students"));
        study.setProfessionals(rs.getInt("study_professionals"));

        //TODO Make changes to duration unit?
        study.setDuration(rs.getInt("study_duration"));
        study.setDurationUnit(rs.getInt("study_duration_unit"));
        study.setDurationUnitName(rs.getString("duration_name"));

        study.setStartDate(rs.getDate("study_start_date"));
        study.setEndDate(rs.getDate("study_end_date"));

        
        study.setCreatedBy(rs.getString("study_created_by"));
        study.setCreatedDate(rs.getDate("study_created_date"));

        study.setEditedBy(rs.getString("study_edited_by"));
        study.setEditedDate(rs.getDate("study_edited_date"));
        
        
        PeopleDatabase peopleDatabase = new PeopleDatabase();
        
        PeopleBean createdBy = peopleDatabase.executeSelect(study.getCreatedBy());
        if (createdBy != null) {
            study.setCreatedBy(createdBy.getFirstName() + " " + createdBy.getFamilyName());
        }
        
        PeopleBean editedBy = peopleDatabase.executeSelect(study.getEditedBy());
        if (editedBy != null) {
            study.setEditedBy(editedBy.getFirstName() + " " + editedBy.getFamilyName());
        }
        
        StudyMaterialDatabase materialDb = new StudyMaterialDatabase();
        study.setStudyMaterial(materialDb.getStudyMaterialInfo(study.getId()));


        fetchPeopleAndPublications(study);


        return study;
    }

    /**
     * Retrieves study type ids and names from the database
     *
     * @param rs query results
     * @return an ArrayList containing the found study types
     * @throws SQLException
     */
    public ArrayList getStudyTypes() throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(STUDY_TYPES_QUERY);
            rs = pstmt.executeQuery();

            ArrayList types = new ArrayList();

            while (rs.next()) {
                NameValueBean bean = new NameValueBean();
                bean.setName(rs.getString("type_name"));
                bean.setValue(rs.getString("type_id"));
                types.add(bean);
            }

            return types;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * Retrieves duration unit ids and names from the database
     *
     * @param rs query results
     * @return an ArrayList containing the found duration units
     * @throws SQLException
     */
    public ArrayList getDurationUnits() throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(DURATION_UNITS_QUERY);
            rs = pstmt.executeQuery();

            ArrayList types = new ArrayList();

            while (rs.next()) {
                NameValueBean bean = new NameValueBean();
                bean.setName(rs.getString("duration_name"));
                bean.setValue(rs.getString("duration_id"));
                types.add(bean);
            }

            return types;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        } finally {
            closeConnection(rs);
        }
    }

    /**
     * Deletes a single study
     *
     * @param studyId the study to delete
     * @throws Exception
     */
    public void deleteStudy(int studyId) throws Exception {
        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(StudyDatabase.DELETE_STUDY_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = conn.prepareStatement(StudyDatabase.DELETE_PUBLICATION_RELATION_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = conn.prepareStatement(StudyDatabase.DELETE_RESPONSIBLE_RELATION_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = conn.prepareStatement(StudyDatabase.DELETE_STUDY_MATERIAL_QUERY);
            pstmt.setInt(1, studyId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            log.error("Error deleting study " + studyId, ex);
            throw ex;
        } finally {
            this.closeConnection(null);
        }
    }

    /**
     * Retrieves all registered studies
     *
     * @return a Collection containing StudyBean objects
     * @throws Exception
     *
     * @see no.simula.des.data.beans.StudyBean
     */
    public Collection getAllStudies() throws Exception {
        ArrayList studies = new ArrayList();
        ArrayList studyIds = new ArrayList();
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            PreparedStatement pstmt = conn.prepareStatement(StudyDatabase.GET_ALL_STUDY_IDS_QUEREY);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                int studyId = rs.getInt(1);
                studyIds.add(new Integer(studyId));
            }

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                rs = null;
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException sqlEx) {
                }

                // ignore
                pstmt = null;
            }

            Iterator ids = studyIds.iterator();

            while (ids.hasNext()) {
                int studyId = ((Integer) ids.next()).intValue();
                StudyBean study = this.getStudy(studyId, conn);
                studies.add(study);
            }

            return studies;
        } catch (Exception ex) {
            log.error("Error fetching data:", ex);
            throw ex;
        } finally {
            this.closeConnection(rs);
        }
    }

    public boolean isStudyNameValid(String studyName, int id) throws Exception{
      ResultSet rs = null;
      boolean ret = true;
      try {
         Connection conn = getConnection();
         PreparedStatement pstmt = conn.prepareStatement(StudyDatabase.CHECK_STUDY_NAME_QUERY);
         pstmt.setString(1, studyName);
         pstmt.setInt(2, id);
         rs = pstmt.executeQuery();
         if(rs.next()){
            int count = rs.getInt(1);
            if(count > 0 ) ret = false;
         }
         return ret;
      }
      catch (Exception ex) {
        log.error("Error checking validy of studyname",ex);
        throw ex;
      }
      finally{
        closeConnection(rs);
      }
    }

   public ArrayList getStudyEndDates() throws Exception{
    ResultSet rs = null;
    ArrayList dates = new ArrayList();
    try {
      Connection conn = getConnection();
      PreparedStatement pstmt = conn.prepareStatement(STUDY_END_DATES_QUERY);
      rs = pstmt.executeQuery();
      while(  rs.next() ){
        java.util.Date date = rs.getDate(1);
        dates.add(date);
      }
      return dates;
    }
    catch (Exception ex) {
      log.error("Error retrieveing study end dates", ex);
      throw ex;
    }
    finally {
      closeConnection(rs);
    }


   }

    public StudiesBean getStudiesByResponsiblesOR(List responsibles, StudySortBean sortBean) throws Exception {
        StringBuffer inClause = new StringBuffer();
        String dynamicQueryResp = "";

        if (responsibles != null && responsibles.size() > 0) {
            boolean firstIteration = true;
            for (int i = 0; i < responsibles.size(); i++) {
                if (!firstIteration) {
                    inClause.append(", ");
                }

                inClause.append("?");

                firstIteration = false;
            }
            dynamicQueryResp = DYNAMIC_QUERY_RESPONSIBLE_OR.format(new Object[] { inClause });
        }
        
        return getStudiesByResponsibles(responsibles, sortBean, dynamicQueryResp);
    }

    public StudiesBean getStudiesByResponsiblesAND(List responsibles, StudySortBean sortBean) throws Exception {
        StringBuffer fromClause = new StringBuffer();
        StringBuffer whereClause = new StringBuffer();
        String dynamicQueryResp = "";

        if (responsibles != null && responsibles.size() > 0) {
            for (int i = 1; i <= responsibles.size(); i++) {
                fromClause.append("responsiblesstudies rs" + i);
                if (i < responsibles.size()) {
                    fromClause.append(", ");
                } else {
                    fromClause.append(" ");
                }
            }
            
            for (int i = 2; i <= responsibles.size(); i++) {
                if (i > 2) {
                    whereClause.append("and ");
                }
                whereClause.append("rs" + (i-1) + ".study_id = rs" + i + ".study_id ");
            }
            
            for (int i = 1; i <= responsibles.size(); i++) {
                if (i > 1 || whereClause.length() > 0) {
                    whereClause.append("and ");
                }
                whereClause.append("rs" + i + ".responsible_id = ? ");
            }
            
            dynamicQueryResp = DYNAMIC_QUERY_RESPONSIBLE_AND.format(new Object[] { fromClause, whereClause });
        }
        
        return getStudiesByResponsibles(responsibles, sortBean, dynamicQueryResp);
    }

    private StudiesBean getStudiesByResponsibles(List responsibles,
            StudySortBean sortBean, String dynamicQueryResp) throws Exception {

        String orderBy = sortBean.getOrderBy();
        if (orderBy != null && orderBy.length() > 0) {
            orderBy = ", " + orderBy;
        }
        String orderDirection = sortBean.getOrderDirection();
        
        StudiesBean studies = null;
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            LoggableStatement pstmt = new LoggableStatement(conn, STUDIES_BY_RESPONSIBLES_QUERY.format(
                    new Object[] { dynamicQueryResp, orderBy, orderDirection }));
            
            int i = 1;
            for (Iterator iterator = responsibles.iterator(); iterator.hasNext();) {
                PeopleBean responsible = (PeopleBean) iterator.next();
                String respId = responsible.getId();
                pstmt.setString(i++, respId);
            }
            
            rs = pstmt.executeQuery();

            //Iterate through resultset and instantiate StudyBean objects
            //Start at first hit and return all hits
            studies = getStudyBeans(rs, 1, Integer.MAX_VALUE, true);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception(e.toString());
        } finally {
            super.closeConnection(rs);
        }

        return studies;
    }
}
