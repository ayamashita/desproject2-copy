package no.simula.des.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PeoplesBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

public class PeopleDatabase extends AbstractWebServiceDatabase {

    private static Log log = LogFactory.getLog(PeopleDatabase.class);

    protected static String webServiceUrl = Constants.WEB_SERVICE_URL;
    private static String webServiceMethod = Constants.WEB_SERVICE_PEOPLE_METHOD;

    public PeopleBean executeSelect(String id) throws Exception {
        XmlRpcClient client = setupXmlRpcClient(webServiceUrl);

        HashMap hash = new HashMap();
        hash.put("id", id);
        Object[] methodParams = new Object[]{ hash };

        PeopleBean person = new PeopleBean();
        try {
            Object test = client.execute(webServiceMethod, methodParams);
            Object[] list = (Object[])test;
            if (list.length == 0) {
                return null;
            }
            HashMap element = (HashMap)list[0];
            person = fillPerson(element);
                
        } catch (XmlRpcException e) {
            log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
            throw e;
        }
        
        return person;
    }

    public List executeSelect(List entityIds) throws Exception {
        XmlRpcClient client = setupXmlRpcClient(webServiceUrl);

        List result = new ArrayList();
        try {
            for (Iterator iterator = entityIds.iterator(); iterator.hasNext();) {
                String id = (String) iterator.next();

                HashMap hash = new HashMap();
                hash.put("id", id);
                Object[] methodParams = new Object[] { hash };

                // Object test = client.execute("getPeople", methodParams);
                Object test = client.execute(webServiceMethod, methodParams);
                Object[] list = (Object[]) test;
                for (int i = 0; i < list.length; i++) {
                    HashMap element = (HashMap) list[i];
                    PeopleBean person = fillPerson(element);

                    result.add(person);
                }
            }
        } catch (XmlRpcException e) {
            log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
            throw e;
       }

        return result;
    }

    public PeoplesBean getPeoples() throws Exception {
        HashMap hash = new HashMap();
        hash.put("sort_on", "lastname");
        Object[] methodParams = new Object[] { hash };

        XmlRpcClient client = setupXmlRpcClient(webServiceUrl);

        ArrayList result = new ArrayList();
        try {
            Object test = client.execute(webServiceMethod, methodParams);
            Object[] list = (Object[]) test;
            for (int i = 0; i < list.length; i++) {
                HashMap element = (HashMap) list[i];
                PeopleBean person = fillPerson(element);

                result.add(person);
            }
        } catch (XmlRpcException e) {
            log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
            throw e;
       }

        PeoplesBean peoplesBean = new PeoplesBean();
        peoplesBean.setPeoples(result);

        return peoplesBean;
    }

    public List getResponsibleList(int studyId) throws Exception {
        List responsibles = new ArrayList();

        ResponsibleStudiesDatabase respStudiesDb = new ResponsibleStudiesDatabase();
        List responsibleIds = respStudiesDb.getResponsibleList(studyId);

        responsibles = executeSelect(responsibleIds);

        return responsibles;
    }

    private PeopleBean fillPerson(HashMap element) {
        PeopleBean person = new PeopleBean();
        person.setId((String) element.get("id"));
        person.setFirstName((String) element.get("firstname"));
        person.setFamilyName((String) element.get("lastname"));
        person.setUrl((String) element.get("url"));
        person.setPosition((String) element.get("jobtitle"));
        return person;
    }

    public List getPeopleByName(String firstName, String lastName) throws Exception {
        
        List result = new ArrayList();

        if ((firstName == null || firstName.length() == 0) && (lastName == null || lastName.length() == 0)) {
            return result;
        }
        
        XmlRpcClient client = setupXmlRpcClient(webServiceUrl);

        HashMap hash = new HashMap();
        
        if (firstName != null && firstName.length() > 0) {
            hash.put("firstname", firstName);
        }
        if (lastName != null && lastName.length() > 0) {
            hash.put("lastname", lastName);
        }
        Object[] methodParams = new Object[]{ hash };

        try {
            Object test = client.execute(webServiceMethod, methodParams);
            Object[] list = (Object[])test;
            for (int i = 0; i < list.length; i++) {
                HashMap element = (HashMap) list[i];
                PeopleBean person = fillPerson(element);

                result.add(person);
            }
                
        } catch (XmlRpcException e) {
            log.error("executeSelect() - XmlRpcException while retrieving persons.", e);
            throw e;
        }
        
        return result;
    }

}
