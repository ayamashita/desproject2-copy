package no.simula.des.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.simula.des.beans.StudyColumn;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.ReportDefinition;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReportDefinitionDatabase extends DataObject {

    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    private static final String GET_REPORT_DEFINITION_QUERY = "select * from report_definitions where id = ?;";
    private static final String GET_REPORT_DEFINITIONS_BY_USER_QUERY = "select * from report_definitions where owner_id = ? order by title;";
    private static final String GET_REPORT_DEFINITION_COUNT_BY_TITLE = "select count(*) from report_definitions where title = ? and id <> ? and owner_id = ?;";
    private static final String GET_RESPONSIBLES_QUERY = "select responsible from report_criteria where report_definition_id = ?;";
    private static final String GET_COLUMNS_QUERY = "select column_name from report_columns where report_definition_id = ? order by column_order;";
    private static final String REPORT_DEFINITION_INSERT = "INSERT INTO report_definitions (owner_id, title, sort, operator) VALUES (?, ?, ?, ?);";
    private static final String REPORT_DEFINITION_UPDATE = "UPDATE report_definitions SET title = ?, sort = ?, operator = ? WHERE id=?;";
    private static final String REPORT_DEFINITION_DELETE = "DELETE FROM report_definitions WHERE id = ?;";
    private static final String REPORT_COLUMNS_INSERT = "INSERT INTO report_columns (report_definition_id, column_name, column_order) VALUES (?, ?, ?);";
    private static final String REPORT_CRITERIA_INSERT = "INSERT INTO report_criteria (report_definition_id, responsible) VALUES (?, ?);";
    private static final String REPORT_COLUMNS_DELETE = "DELETE FROM report_columns WHERE report_definition_id = ?;";
    private static final String REPORT_CRITERIA_DELETE = "DELETE FROM report_criteria WHERE report_definition_id = ?;";


    public ReportDefinition getReportDefinition(int id) throws Exception {
        ResultSet rs = null;
        ReportDefinition reportDefinition;

        try {
            Connection conn = getConnection();
            LoggableStatement pstmt = new LoggableStatement(conn, GET_REPORT_DEFINITION_QUERY);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                reportDefinition = new ReportDefinition();
                reportDefinition.setId(new Integer(id));
                reportDefinition.setTitle(rs.getString("title"));
                reportDefinition.setOperator(rs.getString("operator"));
                reportDefinition.setSort(rs.getBoolean("sort"));

                LoggableStatement pstmtResp = new LoggableStatement(conn, GET_RESPONSIBLES_QUERY);
                pstmtResp.setInt(1, id);
                
                ResultSet rsResp = pstmtResp.executeQuery();
                
                List responsibles = new ArrayList();
                PeopleDatabase peopleDatabase = new PeopleDatabase();
                while (rsResp.next()) {
                    String respId = rsResp.getString("responsible");
                    PeopleBean responsible = peopleDatabase.executeSelect(respId);
                    responsibles.add(responsible);
                }
                reportDefinition.setResponsibles(responsibles);
                

                LoggableStatement pstmtCols = new LoggableStatement(conn, GET_COLUMNS_QUERY);
                pstmtCols.setInt(1, id);
                
                ResultSet rsCols = pstmtCols.executeQuery();
                
                List columns = new ArrayList();
                while (rsCols.next()) {
                    StudyColumn studyColumn = new StudyColumn(rsCols.getString("column_name"));
                    columns.add(studyColumn);
                }
                reportDefinition.setColumns(columns);
                

                return reportDefinition;
            } else {
                log.error("No report definition found with id: " + id);
                throw new Exception("No report definition found with id: " + id);
            }
        } catch (Exception ex) {
            log.error("Error getting report definition with id: " + id, ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }

    public List getReportDefinitionsByOwner(String ownerId) throws Exception {
        List results = new ArrayList();
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            LoggableStatement pstmt = new LoggableStatement(conn, GET_REPORT_DEFINITIONS_BY_USER_QUERY);
            pstmt.setString(1, ownerId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                ReportDefinition reportDefinition = new ReportDefinition();
                reportDefinition.setId(new Integer(rs.getInt("id")));
                reportDefinition.setTitle(rs.getString("title"));
                reportDefinition.setOperator(rs.getString("operator"));
                reportDefinition.setSort(rs.getBoolean("sort"));

                results.add(reportDefinition);
            }
        } catch (Exception ex) {
            log.error("Error getting report definition with ownerId: " + ownerId, ex);
            throw ex;
        } finally {
            closeConnection(rs);
        }
        
        return results;
    }

    public int insertReportDefinition(ReportDefinition reportDefinition) throws Exception {
        int id = 0;
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            LoggableStatement pstmt = new LoggableStatement(conn, REPORT_DEFINITION_INSERT);
            pstmt.setString(1, reportDefinition.getOwner().getId());
            pstmt.setString(2, reportDefinition.getTitle());
            pstmt.setBoolean(3, reportDefinition.getSort().booleanValue());
            pstmt.setString(4, reportDefinition.getOperator());

            int updates = pstmt.executeUpdate();

            if (updates == 0) {
                throw new Exception("A new report definition was not added (" + reportDefinition.getTitle() + ")");
            }

            /* Fetches the generated keys for the entity inserted
             * There should only be one row with one key
             */
            rs = pstmt.getGeneratedKeys();

            if (rs != null && rs.next()) {
              // if there's more keys, they're ignored, as there should be only one
              id = rs.getInt(1);
            }

            insertColumns(reportDefinition, id, conn);
            
            insertCriteria(reportDefinition, id, conn);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            closeConnection(rs);
        }

        return id;
    }

    private void insertCriteria(ReportDefinition reportDefinition, int id,
            Connection conn) throws SQLException {
        LoggableStatement pstmtCriteria = new LoggableStatement(conn, REPORT_CRITERIA_INSERT);
        List criteria = reportDefinition.getResponsibles();
        for (Iterator iterator = criteria.iterator(); iterator.hasNext();) {
            PeopleBean responsible = (PeopleBean) iterator.next();
            if (responsible.getDeleted()) {
                continue;
            }
            pstmtCriteria.setInt(1, id);
            pstmtCriteria.setString(2, responsible.getId());

            pstmtCriteria.executeUpdate();
        }
    }

    private void insertColumns(ReportDefinition reportDefinition, int id,
            Connection conn) throws SQLException {
        LoggableStatement pstmtColumns = new LoggableStatement(conn, REPORT_COLUMNS_INSERT);
        List columns = reportDefinition.getColumns();
        int i = 1;
        for (Iterator iterator = columns.iterator(); iterator.hasNext();) {
            StudyColumn studyColumn = (StudyColumn) iterator.next();
            pstmtColumns.setInt(1, id);
            pstmtColumns.setString(2, studyColumn.getColumn());
            pstmtColumns.setInt(3, i++);
            
            pstmtColumns.executeUpdate();
        }
    }

    private void deleteCriteria(int id, Connection conn) throws SQLException {
        LoggableStatement pstmtCriteria = new LoggableStatement(conn, REPORT_CRITERIA_DELETE);
        pstmtCriteria.setInt(1, id);

        pstmtCriteria.executeUpdate();
    }

    private void deleteColumns(int id, Connection conn) throws SQLException {
        LoggableStatement pstmtColumns = new LoggableStatement(conn, REPORT_COLUMNS_DELETE);
        pstmtColumns.setInt(1, id);

        pstmtColumns.executeUpdate();
    }

    public int updateReportDefinition(ReportDefinition reportDefinition) throws Exception {
        int id = reportDefinition.getId().intValue();
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            LoggableStatement pstmt = new LoggableStatement(conn, REPORT_DEFINITION_UPDATE);
            pstmt.setString(1, reportDefinition.getTitle());
            pstmt.setBoolean(2, reportDefinition.getSort().booleanValue());
            pstmt.setString(3, reportDefinition.getOperator());
            pstmt.setInt(4, id);

            int updates = pstmt.executeUpdate();

            if (updates == 0) {
                throw new Exception("A report definition was not updated (" + reportDefinition.getTitle() + ")");
            }

            deleteColumns(id, conn);
            insertColumns(reportDefinition, id, conn);
            
            deleteCriteria(id, conn);
            insertCriteria(reportDefinition, id, conn);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            closeConnection(rs);
        }

        return id;
    }
    
    public void delete(int id) throws Exception {
        ResultSet rs = null;
        
        try {
            Connection conn = getConnection();
            LoggableStatement pstmt = new LoggableStatement(conn, REPORT_DEFINITION_DELETE);
            pstmt.setInt(1, id);

            int updates = pstmt.executeUpdate();

            if (updates == 0) {
                throw new Exception("A report definition was not deleted.");
            }

            deleteColumns(id, conn);
            
            deleteCriteria(id, conn);
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            closeConnection(rs);
        }
    }

    public int getReportDefinitionsCountByTitle(String title, Integer repDefId, String userId) throws Exception {
        ResultSet rs = null;

        try {
            Connection conn = getConnection();
            LoggableStatement pstmt = new LoggableStatement(conn, GET_REPORT_DEFINITION_COUNT_BY_TITLE);
            pstmt.setString(1, title);
            pstmt.setInt(2, repDefId.intValue());
            pstmt.setString(3, userId);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                int count = rs.getInt(1);
                return count;
            } else {
                log.error("Could not find count of report definitions with title " + title + " for user id " + userId);
                throw new Exception("Could not find count of report definitions with title " + title + " for user id " + userId);
            }
        } catch (Exception ex) {
            log.error("Could not find count of report definitions with title " + title + " for user id " + userId);
            throw ex;
        } finally {
            closeConnection(rs);
        }
    }

}
