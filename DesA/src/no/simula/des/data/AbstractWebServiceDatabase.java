package no.simula.des.data;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public abstract class AbstractWebServiceDatabase {

	protected XmlRpcClient setupXmlRpcClient(String webServiceUrl) {
		XmlRpcClient client;
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		client = new XmlRpcClient();
		try {
			config.setServerURL(new URL(webServiceUrl));
		} catch (MalformedURLException e) {
			// We know that url is right.
		}
		client.setConfig(config);
		return client;
	}

}