package no.simula.des.data.beans;



public class PrivilegeBean {

  public static int DBA_PRIVILEGE = 2;
  public static int SA_PRIVILEGE = 1;
  public static int GUEST_PRIVILEGE = 0;

  private int people_id;
  private int privilege;

  public PrivilegeBean() {
  }

  public void setPrivilege(int people_id, int privilege ){
    this.people_id = people_id;
    this.privilege = privilege;
  }
  public int getPeople_id() {
    return people_id;
  }
  public int getPrivilege() {
    return privilege;
  }



}