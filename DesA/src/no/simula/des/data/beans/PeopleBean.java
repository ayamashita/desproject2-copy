/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.data.beans;


/**
 * Represents a single person
 */
public class PeopleBean {
    private String id;
    private String firstName;
    private String familyName;
    private String position;
    private String url;
    private int privilege = 0;

    //Utility attributes
    private boolean isAdded;

    //Utility attributes
    private boolean isDeleted;

    public String getFamilyName() {
        return familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getId() {
        return id;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getPrivilege() {
        return privilege;
    }

    public void setPrivilege(int privilege) {
        this.privilege = privilege;
    }

    public boolean getAdded() {
        return isAdded;
    }

    public boolean getDeleted() {
        return isDeleted;
    }

    public void setAdded(boolean b) {
        isAdded = b;
    }

    public void setDeleted(boolean b) {
        isDeleted = b;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
