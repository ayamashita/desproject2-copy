/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.util;

import java.util.ResourceBundle;

/**
 * This class contains global constants for the DES application
 */
public class Constants {
    // Log name
    public final static String GLOBAL_LOG = "no.simula.des";
    public final static String DATE_PATTERN = "dd-MMM-yyyy";
    public final static int FRONTPAGE_ID = 1;

    // Access levels
    public static final int DBA = 2;
    public static final int SA = 1;
    public static final int NONE = 0;

    /**
     * The name of the simula web database
     */
    public static final String SIMULAWEB_DB = ResourceBundle.getBundle("des").getString("SIMULAWEB.DATABASE.NAME");

    public static final String WEB_SERVICE_URL = ResourceBundle.getBundle("des").getString("webServiceUrl");
    public static final String WEB_SERVICE_PEOPLE_METHOD = ResourceBundle.getBundle("des").getString("webServicePeopleMethod");
    public static final String WEB_SERVICE_PUBLICATION_METHOD = ResourceBundle.getBundle("des").getString("webServicePublicationsMethod");

    public static final String WEB_SERVICE_AUTHENTICATION_URL = ResourceBundle.getBundle("des").getString("webServiceAuthUrl");
    public static final String WEB_SERVICE_AUTHENTICATION_METHOD = ResourceBundle.getBundle("des").getString("webServiceAuthenticationMethod");
    
}
