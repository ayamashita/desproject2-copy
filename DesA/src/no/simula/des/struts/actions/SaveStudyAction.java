/*
 * Created on 03.okt.2003
 *
*/
package no.simula.des.struts.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.beans.DateBean;
import no.simula.des.data.StudyDatabase;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This action saves a new / edited study to the database
 */
public class SaveStudyAction extends DesAction {
    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    /* (non-Javadoc)
     * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        StudyForm studyForm = (StudyForm) form;
        StudyBean bean = new StudyBean();

        if (log.isDebugEnabled()) {
            log.debug("from form: " + studyForm.getName());
            log.debug("Operation: " + studyForm.getOperation());
        }

        if (studyForm.getOperation() == StudyForm.RELOAD_STUDY_OPERATION) {
            log.debug("Reoloading study, Do not save");

            return new ActionForward("/studyForm.jsp");
        }

        try {
            populateStudyBean(studyForm, bean);

            //Get editor id from session object
            PeopleBean user = (PeopleBean) request.getSession().getAttribute("user");
            bean.setEditorId(user.getId());
        } catch (Exception e) {
            log.error("Error populating study bean from study form", e);
        }

        if (log.isDebugEnabled()) {
            log.debug("StudyForm: " + studyForm.toString());
        }

        //Check action / id to check wether new or edited study
        StudyDatabase db = new StudyDatabase();
        int studyId = bean.getId();

        try {
            if (studyForm.getAction() == StudyForm.EDIT_STUDY_ACTION) {
                db.updateStudy(bean);
                studyForm.setOperation(StudyForm.RELOAD_STUDY_OPERATION);
                request.setAttribute("msgKey", "no.simula.des.updated");
            } else {
                studyId = db.insertStudy(bean);
                studyForm.setOperation(StudyForm.RELOAD_STUDY_OPERATION);
                log.debug("Id of new study: " + studyId);
                request.setAttribute("msgKey", "no.simula.des.inserted");
            }
        } catch (Exception e) {
            log.error("Error writing study to database", e);

            return mapping.findForward("failure");
        }

        request.setAttribute("study", new Integer(studyId));

        return new ActionForward("/showStudy.do?study=" + studyId);

        //return mapping.findForward("success");
        //return (new ActionForward("/editStudy.do?study="+studyId, true) );
    }

    /**
     * Populates the StudyBean with posted data
     *
     * @param studyForm the form object (source)
     * @param study the data bean (target)
     */
    void populateStudyBean(StudyForm form, StudyBean bean)
        throws Exception {
        //TODO handle null values in optional fields
        //Required fields
        //Id is not set for new studies
        bean.setId(Integer.parseInt(form.getId()));
        bean.setName(form.getName());
        bean.setDescription(form.getDescription());

        //TODO should type be type name or value?
        bean.setType(form.getType());

        //Move date input explicitly. Assume data is already validated
        //End date is required
        int day = Integer.parseInt(form.getEndDay());
        int month = Integer.parseInt(form.getEndMonth());
        int year = Integer.parseInt(form.getEndYear());
        DateBean endDate = new DateBean(year, month, day);

        if (log.isDebugEnabled()) {
            log.debug("endDate = " + endDate.toString());
        }

        bean.setEndDate(endDate.getDate());

        //Start date is optional
        if (!form.getStartDay().equals("") && !form.getStartMonth().equals("") &&
                !form.getStartYear().equals("")) {
            day = Integer.parseInt(form.getStartDay());
            month = Integer.parseInt(form.getStartMonth());
            year = Integer.parseInt(form.getStartYear());

            DateBean startDate = new DateBean(year, month, day);

            if (log.isDebugEnabled()) {
                log.debug("startDate = " + startDate.toString());
            }

            bean.setStartDate(startDate.getDate());
        }

        //Optional fields
        bean.setKeywords(form.getKeywords());
        bean.setNotes(form.getNotes());

        if (!form.getStudents().equals("")) {
            bean.setStudents(Integer.parseInt(form.getStudents()));
        }

        if (!form.getProfessionals().equals("")) {
            bean.setProfessionals(Integer.parseInt(form.getProfessionals()));
        }

        if (!form.getDuration().equals("")) {
            bean.setDuration(Integer.parseInt(form.getDuration()));
        }

        bean.setDurationUnit(Integer.parseInt(form.getDurationUnit()));

        //Output only
        bean.setCreatedDate(form.getCreatedDate());
        bean.setCreatedBy(form.getCreatedBy());
        bean.setEditedDate(form.getEditedDate());
        bean.setEditedBy(form.getEditedBy());

        //TODO Include collections
        bean.setPublications(form.getPublications());
        bean.setResponsibles(form.getResponsibles());

        bean.setStudyMaterial((ArrayList) form.getStudyMaterial());
    }
}
