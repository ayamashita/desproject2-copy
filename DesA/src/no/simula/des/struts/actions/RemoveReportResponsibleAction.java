/*
 * Created on 14.okt.2003
 *
 */
package no.simula.des.struts.actions;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.struts.forms.ReportDefinitionForm;
import no.simula.des.struts.forms.StudyForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



/**
 * This Action removes a person from the list of responsibles for a study.
 * The connection between the two is NOT removed at the database level until
 * SaveStudyAction is called.
 *
 */
public class RemoveReportResponsibleAction extends DesAction {
    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    /* (non-Javadoc)
     * @see no.simula.des.struts.actions.DesAction#executeAuthenticated(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        ReportDefinitionForm reportForm = (ReportDefinitionForm) form;
        String respToRemove = "";

        respToRemove = request.getParameter("responsible");
        log.debug("The id to remove: " + respToRemove);

        if (respToRemove == null || respToRemove.length() == 0) {
            log.error("\"responsible\" parameter not found in request.");
            return mapping.findForward("failure");
        }

        // Get Collection from form.
        Iterator responsibles = reportForm.getResponsibles().iterator();

        //Find publication. Limited number of elements -> use linear search
        while (responsibles.hasNext()) {
            PeopleBean resp = (PeopleBean) responsibles.next();

            if (respToRemove.equals(resp.getId())) {
                //Mark bean for deletion
                log.debug("Setting responsible no:" + resp.getId() +
                    " to deleted");
                resp.setDeleted(true);

                break;
            }
        }

        return mapping.findForward("success");
    }
}
