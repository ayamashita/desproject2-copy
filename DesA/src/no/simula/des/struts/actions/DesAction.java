/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This abstract class is used as a super class for all Struts actions that
 * may need authentication. Hence all authentication mechanisms is located here.
 */
public abstract class DesAction extends Action {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);
    ResourceBundle prb = ResourceBundle.getBundle("des");

    //Set this vatiable i the subclass to controll access level
    protected int min_access_level = 2;

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        PeopleBean user = (PeopleBean) request.getSession().getAttribute("user");

        String strIsSecurityDisabled = prb.getString("IS_SECURITY_DISABLED");

        log.debug("The user is: " + user);

        boolean isSecurityDisabled = (((strIsSecurityDisabled != null) &&
            strIsSecurityDisabled.trim().equalsIgnoreCase("true")) ? true : false);

        log.debug("Security disabeled: " + isSecurityDisabled);

        if (isSecurityDisabled) {
            return executeAuthenticated(mapping, form, request, response);
        }
        
        if (user != null) {
            if (user.getPrivilege() >= getAccessLevel()) {
                return executeAuthenticated(mapping, form, request, response);
            } else {
                request.setAttribute("AccessLevel",
                    new Integer(min_access_level));

                return (mapping.findForward("notAuthenticated"));
            }
        }

        return (mapping.findForward("logon"));

        //failure: login screen / frontpage?
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        return (mapping.findForward("logon"));
    }

    public abstract int getAccessLevel();
}
