package no.simula.des.struts.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.DurationUnitsDatabase;
import no.simula.des.data.StudyTypesDatabase;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Title:ShowStudyTypesDurationUnitsAction
 * Description: This action class is responsible for showing the studytypes and duration units
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author: Anders Aas Hanssen
 * @version 1.0
 */
public class ShowStudyTypesDurationUnitsAction extends DesAction {
    public ShowStudyTypesDurationUnitsAction() {
    }

    public int getAccessLevel() {
        return 2;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        StudyTypesDatabase studyTypesDb = new StudyTypesDatabase();
        ArrayList studyTypes = studyTypesDb.getStudyTypes();
        request.setAttribute("StudyTypes", studyTypes);

        DurationUnitsDatabase durationUnitDb = new DurationUnitsDatabase();
        ArrayList units = durationUnitDb.getDurationUnits();
        request.setAttribute("DurationUnits", units);

        return (mapping.findForward("success"));
    }
}
