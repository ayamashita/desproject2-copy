/*
 * Created on 14.okt.2003
 *
 */
package no.simula.des.struts.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.beans.ContentMessageBean;
import no.simula.des.data.beans.ContentBean;
import no.simula.des.util.Constants;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This Action loads and displays the front page contents
 *
 */
public class FrontPageAction extends Action {
    /**
     * Logging output for this class.
     */
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        ArrayList text = (ArrayList) ContentMessageBean.getInstance(Constants.FRONTPAGE_ID);
        ContentBean frontPageText = (ContentBean) text.get(0);
        String content = frontPageText.getText();

        //setText(StringUtils.replace(frontPageText.getText(),
         //       "\n", "<br>"));
        request.setAttribute("frontPageText", StringUtils.replace(content,"\n", "<br>" ) );

        return mapping.findForward("success");
    }
}
