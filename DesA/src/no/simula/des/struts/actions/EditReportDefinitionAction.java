package no.simula.des.struts.actions;

import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.beans.StudyColumn;
import no.simula.des.data.ReportDefinitionDatabase;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.ReportDefinition;
import no.simula.des.struts.forms.ReportDefinitionForm;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class EditReportDefinitionAction extends DesAction {

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        if(isCancelled(request)) {
            return(mapping.getInputForward());
        }
          
        ReportDefinitionForm reportForm = (ReportDefinitionForm) form;

        ActionErrors errors = null;

        if (saveChanges(request)) {

            PeopleBean user = (PeopleBean) request.getSession().getAttribute("user");
            errors = validate(reportForm, user.getId());
            if (errors == null) {

                ReportDefinition reportDefinition;
                ReportDefinitionDatabase reportDefinitionDatabase = new ReportDefinitionDatabase();

                // Get existing report definition from cache to allow tables in
                // session to be
                // updated automatically (they point to the same object in
                // memory)
                if (isNewReportDefinition(reportForm)) {
                    reportDefinition = new ReportDefinition();
                } else {
                    reportDefinition = reportDefinitionDatabase.getReportDefinition(reportForm.getId().intValue());
                }

                BeanUtils.copyProperties(reportDefinition, reportForm);

                if (isNewReportDefinition(reportForm)) {
                    reportDefinition.setOwner(user);

                    reportDefinitionDatabase.insertReportDefinition(reportDefinition);
                } else {
                    reportDefinitionDatabase.updateReportDefinition(reportDefinition);
                }

                return (mapping.getInputForward());
            }
        }

        // /////////////////////////////////////////////////////
        // Mutually exclusive actions that directs the user
        // to the main view of this action

        if (isInitialRequest(request)) {

            if (isNew(request)) {
                reportForm.setId(null);
                reportForm.setTitle(null);
                reportForm.setOperator("AND");
                reportForm.setColumns(new ArrayList(0));
                reportForm.setResponsibles(new ArrayList(0));
            } else {
                Integer id = (Integer) reportForm.getId();
                ReportDefinitionDatabase reportDefinitionDatabase = new ReportDefinitionDatabase();
                ReportDefinition reportDefinition = reportDefinitionDatabase
                        .getReportDefinition(id.intValue());
                BeanUtils.copyProperties(reportForm, reportDefinition);
            }

//            reportForm.setPersons(new ArrayList(simula.getPersons()));
//            reportForm.getPersons().removeAll(reportForm.getResponsibles());

            StudyColumn[] columns = {
                    new StudyColumn("createdBy"),
                    new StudyColumn("createdDate"),
                    new StudyColumn("description"),
                    new StudyColumn("duration"),
                    new StudyColumn("durationUnitName"),
                    new StudyColumn("editedBy"),
                    new StudyColumn("editedDate"),
                    new StudyColumn("endDate"),
                    new StudyColumn("id"),
                    new StudyColumn("keywords"),
                    new StudyColumn("name"),
                    new StudyColumn("notes"),
                    new StudyColumn("professionals"),
                    new StudyColumn("publications"),
                    new StudyColumn("responsibles"),
                    new StudyColumn("shortDescription"),
                    new StudyColumn("startDate"),
                    new StudyColumn("students"),
                    new StudyColumn("studyMaterial"),
                    new StudyColumn("type")
                    };
            reportForm
                    .setAvailableColumns(new ArrayList(Arrays.asList(columns)));
            reportForm.getAvailableColumns().removeAll(reportForm.getColumns());
        }

        else if (addColumns(request)) {
            String[] add = reportForm.getAddColumn();
            for (int i = 0; i < add.length; i++) {
                StudyColumn column = new StudyColumn(add[i]);
                if (!reportForm.getColumns().contains(column)) {
                    reportForm.getColumns().add(column);
                }
                reportForm.getAvailableColumns().remove(column);
            }
        }

        else if (removeColumns(request)) {
            String[] remove = reportForm.getRemoveColumn();
            for (int i = 0; i < remove.length; i++) {
                StudyColumn column = new StudyColumn(remove[i]);
                if (!reportForm.getAvailableColumns().contains(column)) {
                    reportForm.getAvailableColumns().add(column);
                }
                reportForm.getColumns().remove(column);
            }
            reportForm.sortAvailableColumns();
        }

        if (errors != null) {
            saveErrors(request, errors);
        }
        return (mapping.findForward("edit"));

    }

    private ActionErrors validate(ReportDefinitionForm form, String userId) throws Exception {
        ActionErrors errors = new ActionErrors();

        if(form.getTitle() == null || form.getTitle().length() == 0) {
          ActionError error = new ActionError("errors.required", "title");
          errors.add("title", error);
        }

        if(form.getOperator() == null || form.getOperator().length() == 0) {
          ActionError error = new ActionError("errors.required", "operator");
          errors.add("operator", error);
        }
        
        if(form.getColumns().size() == 0) {
          ActionError error = new ActionError("errors.minoccur", "1 column");
          errors.add("selectedColumns", error);
        }
        
        ReportDefinitionDatabase reportDefinitionDatabase = new ReportDefinitionDatabase();
        int count = reportDefinitionDatabase.getReportDefinitionsCountByTitle(form.getTitle(), form.getId(), userId);
        if (count > 0) {
            ActionError error = new ActionError("error.des.reportDefinition.create.titleNotUnique");
            errors.add("title", error);
        }

        if(errors.isEmpty()) {
          return(null);
        }
        else {
          return(errors);
        }
      }

    private boolean isInitialRequest(HttpServletRequest request) {
        return (request.getParameter("new") != null || request
                .getParameter("id") != null);
    }

    private boolean isNew(HttpServletRequest request) {
        return (request.getParameter("new") != null);
    }

    private boolean addColumns(HttpServletRequest request) {
        return (request.getParameter("addColumns") != null);
    }

    private boolean removeColumns(HttpServletRequest request) {
        return (request.getParameter("removeColumns") != null);
    }

    private boolean saveChanges(HttpServletRequest request) {
        return (request.getParameter("save") != null);
    }

    private boolean isNewReportDefinition(ReportDefinitionForm form) {
        return (form.getId() == null);
    }

}
