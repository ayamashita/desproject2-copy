package no.simula.des.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.ReportDefinitionDatabase;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

public class DeleteReportDefinitionAction extends DesAction {

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        
        DynaActionForm reportFormId = (DynaActionForm) form;
        
        Integer id = (Integer) reportFormId.get("id");
        
        if (id != null) {
            ReportDefinitionDatabase reportDefinitionDatabase = new ReportDefinitionDatabase();
            reportDefinitionDatabase.delete(id.intValue());
        }
        
        return mapping.getInputForward();
    }

}
