package no.simula.des.struts.forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ReportDefinitionForm extends ActionForm {

    private Integer id;
    private String title;
    private String operator;
    private boolean sort;

    private List availableColumns;
    private List columns;
    private String[] addColumn;
    private String[] removeColumn;

    private List responsibles;

    /**
     * <CODE>addResponsible</CODE> and <CODE>removeResponsible</CODE> is
     * reset because Struts is not able to do so automatically.
     * 
     * @param mapping
     *            current action mapping
     * @param request
     *            current http request
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        addColumn = new String[0];
        removeColumn = new String[0];
        sort = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public boolean isSort() {
        return sort;
    }

    public void setSort(boolean sort) {
        this.sort = sort;
    }

    public List getAvailableColumns() {
        return availableColumns;
    }

    public void setAvailableColumns(List availableColumns) {
        this.availableColumns = availableColumns;
    }

    public List getColumns() {
        return columns;
    }

    public void setColumns(List columns) {
        if (columns == null) {
            this.columns = null;
        } else {
            this.columns = new ArrayList(columns);
        }
    }

    public String[] getAddColumn() {
        return addColumn;
    }

    public void setAddColumn(String[] addColumn) {
        this.addColumn = addColumn;
    }

    public String[] getRemoveColumn() {
        return removeColumn;
    }

    public void setRemoveColumn(String[] removeColumn) {
        this.removeColumn = removeColumn;
    }

    /**
     * Returns a list responsible persons
     * 
     * @return responsible persons
     */
    public List getResponsibles() {
        return (responsibles);
    }

    /**
     * Setter for property responsibles.
     * 
     * @param responsibles
     *            New value of property responsibles.
     * 
     */
    public void setResponsibles(List responsibles) {
        this.responsibles = new ArrayList(responsibles);
    }

    public void sortAvailableColumns() {
        Collections.sort(this.availableColumns);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}