package no.simula.des.beans;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import no.simula.des.data.AggregatedStudiesDatabase;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.DefaultCategoryDataset;

import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;


/**
 * This bean creates a <code>Dataset</code> containing the aggregated
 * study information necessary for the rendering engine to produce the
 * Aggregated Study graphs.
 */
public class AggregatedStudiesBean implements DatasetProducer {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);
    private String[] categories; // =    {"mon", "tue", "wen", "thu", "fri", "sat", "sun"};
    private String[] seriesNames; // =    {"cewolfset.jsp", "tutorial.jsp", "testpage.jsp", "performancetest.jsp"};
    private Integer[][] values;
    private Hashtable data;

    public AggregatedStudiesBean() {
    }

    public Object produceDataset(Map params) throws DatasetProduceException {
        log.debug("producing data.");
        populatingData();

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int series = 0; series < seriesNames.length; series++) {
            for (int i = 0; i < categories.length; i++) {
                Hashtable values = (Hashtable) data.get(new Integer(
                            categories[i]));
                Integer value = (Integer) values.get(seriesNames[series]);
                dataset.addValue(value.doubleValue(), seriesNames[series],
                    categories[i]);
            }
        }

        return dataset;
    }

    /*
    public Object produceDataset(Map params) throws DatasetProduceException {
        log.debug("producing data.");

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

          for (int series = 0; series < seriesNames.length; series ++) {
              int lastY = (int)(Math.random() * 1000 + 1000);
              for (int i = 0; i < categories.length; i++) {
                  final int y = lastY + (int)(Math.random() * 200 - 100);
                  lastY = y;
                  dataset.addValue((double)y, seriesNames[series], categories[i]);
              }

          }

          return dataset;

    }
    */

    /**
     * Informs the rendering engine whether a cached object is still valid
     */
    public boolean hasExpired(Map params, Date since) {
        log.debug(getClass().getName() + "hasExpired()");

        return (System.currentTimeMillis() - since.getTime()) > (1000 * 60 * 12);
    }

    /**
     * Identifies this dataset for the rendering engine
     */
    public String getProducerId() {
        return "AggregatedStudies DatasetProducer";
    }

    //---------- Private helper methods -------------
    private void populatingData() {
        try {
            log.debug("populating data.");

            AggregatedStudiesDatabase asDb = new AggregatedStudiesDatabase();
            asDb.populateAggregatesInformation();
            seriesNames = asDb.getStudyTypes();
            data = asDb.getData();
            categories = asDb.getYears();
            values = new Integer[seriesNames.length][categories.length];
        } catch (Exception e) {
            log.error("Error producing aggregates study data has occured: " +
                e);
            categories = new String[] { "" };
            seriesNames = new String[] { "" };
            values = new Integer[0][0];
        }
    }
}
