package no.simula.des.beans;

import java.io.Serializable;

public class StudyColumn implements Comparable, Serializable {

    private String column;

    public StudyColumn(String column) {
        super();
        this.column = column;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        StudyColumn that = (StudyColumn) obj;
        String thatColumn = that.getColumn();
        return (thatColumn.equals(this.column));
    }

    public int hashCode() {
        return column.hashCode();
    }

    public int compareTo(Object o) {
        return this.column.compareTo(((StudyColumn)o).getColumn());
    }

}
