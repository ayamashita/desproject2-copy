import java.util.Map;


public class Study {
	public int id;
	public String name;
	public Map publications;

	public String toString() {
		return "Study [ name: " + name + ", publications: " + publications + " ]";
	}

}
