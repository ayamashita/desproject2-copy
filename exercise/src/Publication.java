
public class Publication {
	public String id;
	public String title;
	public String type;
	public String abstr;
	
	public String toString() {
		return "Publication [ title: " + title + ", type: " + type + ", abstr: " + abstr + " ]";
	}

}
