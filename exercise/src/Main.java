import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class Main {
	XmlRpcClient client;

	public Main() {
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		client = new XmlRpcClient();
		try {
			config.setServerURL(new URL("http://simula.no:9080/simula/xmlrpcdes"));
		} catch (MalformedURLException e) {
			// We know that url is right.
		}
		client.setConfig(config);
	}

	/**
	 * @param args
	 * @throws XmlRpcException 
	 */
	public static void main(String[] args) throws XmlRpcException {

		Main main = new Main();
		
		// Exercise a
		//main.listPublications("xxx");
		
		// Exercise b
		//main.listNumberOfStudiesPerType();
		
		// Exercise d
		//main.listPeopleByEmail("ahe@simula.no stuart@simula.no");
		
		// Exercise e
		//main.listPeopleSorted();
	}
	
	private void listPeopleByEmail(String string) throws XmlRpcException {
		
		String[] emails = string.split(" ");
		for (int j = 0; j < emails.length; j++) {
			HashMap hash = new HashMap();
			hash.put("email", emails[j]);
			Object[] methodParams = new Object[]{ hash };
			Object test = client.execute("getPeople", methodParams);
			Object[] list = (Object[])test;
			for (int i = 0; i < list.length; i++) {
				HashMap element = (HashMap)list[i];
				System.out.println(element.get("email") + " " + element.get("firstname") + " " + element.get("lastname"));
			}
		}
	}

	private void listPeopleSorted() throws XmlRpcException {
		HashMap hash = new HashMap();
		hash.put("sort_on", "jobtitle");
		Object[] methodParams = new Object[]{ hash };
		Object test = client.execute("getPeople", methodParams);
		Object[] list = (Object[])test;
		for (int i = 0; i < list.length; i++) {
			HashMap element = (HashMap)list[i];
			System.out.println(element.get("firstname") + " " + element.get("lastname") + " " + element.get("jobtitle"));
		}
	}

	private void listNumberOfStudiesPerType() {
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/exercise_a";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			PreparedStatement ps = conn.prepareStatement("SELECT study_type, count(*) as count FROM study group by study_type");

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int studyType = rs.getInt("study_type");
				int count = rs.getInt("count");
				
				System.out.println("Study type: " + studyType + ", count: " + count);
			}
			
			rs.close();
			ps.close();
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) { /* ignore close errors */
				}
			}
		}
	}

	public void listPublications(String userId) throws XmlRpcException {
		Map studies = getStudies(userId);

		for (Iterator iterator = studies.keySet().iterator(); iterator.hasNext();) {
			Integer key = (Integer) iterator.next();
			Study study = (Study) studies.get(key);
			getPublications(study);
		}
		
		
		for (Iterator iterator = studies.keySet().iterator(); iterator.hasNext();) {
			Integer key = (Integer) iterator.next();
			Study study = (Study) studies.get(key);
			System.out.println(study);
		}		
		
	}

	private void getPublications(Study study) throws XmlRpcException {
		
		for (Iterator iterator = study.publications.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();

			HashMap hash = new HashMap();
			hash.put("id", key);
			Object[] methodParams = new Object[]{ hash };
			Object test = client.execute("getPublications", methodParams);
			Object[] list = (Object[])test;
			if(list == null || list.length == 0) {
				continue;
			}
			HashMap element = (HashMap)list[0];
			Publication publication = new Publication();
			publication.id = key;
			publication.title = (String) element.get("Title");
			publication.type = (String) element.get("Type");
			publication.abstr = (String) element.get("abstract");
			study.publications.put(key, publication);
		}
		
	}

	private Map getStudies(String peopleId) {
		Map studies = new HashMap();
		Connection conn = null;
		try {
			String userName = "admin";
			String password = "masterkey";
			String url = "jdbc:mysql://localhost:3306/exercise_a";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			PreparedStatement ps = conn.prepareStatement("SELECT s.study_id, s.study_name, ps.publication_id FROM (responsible_study as rs join study as s on rs.study_id = s.study_id) join publication_study as ps on s.study_id = ps.study_id where rs.responsible_id = ?");
			ps.setString(1, peopleId);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Integer studyId = new Integer(rs.getInt("study_id"));
				Study study = (Study) studies.get(studyId);
				if (study == null) {
					study = new Study();
					study.name = rs.getString("study_name");
					study.id = studyId.intValue();
					study.publications = new HashMap();
					studies.put(studyId, study);
				}
				String publicationId = rs.getString("publication_id");
				Map publications = study.publications;
				publications.put(publicationId, null);
			}
			
			rs.close();
			ps.close();
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) { /* ignore close errors */
				}
			}
		}
		
		return studies;
	}

}
